from django.conf.urls import patterns, include, url
from django.contrib import admin

from django.conf.urls.i18n import i18n_patterns
from django.http import Http404


def no_view(request):
    """
    We don't really want anyone going to the static_root.
    However, since we're raising a 404, this allows flatpages middleware to
    step in and serve a page, if there is one defined for the URL.
    """
    raise Http404


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'saveit_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', include('saveit.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^saveit/', include('saveit.urls')),
    url(r'^coachdata/', include('coachData.urls')),
    url(r'^game/', include('game.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
)

# urlpatterns += i18n_patterns('',
#                              (_(r'^coachdata/'), include('coachData.urls')),
#                              (r'^', include('saveit.urls')),
#                              )
