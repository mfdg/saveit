from django.shortcuts import render


def game(request):
    context_dict = {'title': "Play it",
                    'login': False}
    return render(request, 'game/index.html', context_dict)