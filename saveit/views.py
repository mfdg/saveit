# -*- coding: utf-8 -*-+

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render

from saveit.models import Tabs


def index(request):
    # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!

    context_dict = {'active_page': Tabs.HOME,
                    'title': "Home"}

    # Return a rendered response to send to the client.
    # We make use of the shortcut function to make our lives easier.
    # Note that the first parameter is the template we wish to use.

    return render(request, 'saveit/index.html', context_dict)


def about(request):
    context_dict = {'active_page': Tabs.ABOUT,
                    'title': "The Project"}
    return render(request, 'saveit/about.html', context_dict)


def partners(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'title': "Partners"}
    return render(request, 'saveit/partners.html', context_dict)


def news(request):
    context_dict = {'active_page': Tabs.NEWS,
                    'title': "News"}
    return render(request, 'saveit/news.html', context_dict)


def contact(request):
    context_dict = {'active_page': Tabs.CONTACT,
                    'title': "Contact"}
    return render(request, 'saveit/contact.html', context_dict)


def faqs(request):
    context_dict = {'active_page': Tabs.FAQS,
                    'title': "FAQs"}
    return render(request, 'saveit/faqs.html', context_dict)


def eportal(request):
    context_dict = {'active_page': Tabs.EPORTAL,
                    'title': "ePortal"}
    return render(request, 'saveit/eportal.html', context_dict)


# ------------ #
#   PARTNERS   #
# ------------ #
def fculturalista(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/cultural.png",
                    'title': "Fundación Culturalista - Partners",
                    'partner_name': "Fundación Culturalista",
                    'foundation_date': 1923,
                    'country': "Spain",
                    'location': "León",
                    "emblem": False}
    return render(request, 'saveit/partners/fundacionculturalista.html', context_dict)


def uam(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/uam.png",
                    'title': "UAM - Partners",
                    'partner_name': "Autonomous University of Madrid",
                    'foundation_date': 1968,
                    'country': "Spain",
                    'location': "Madrid",
                    "emblem": False}
    return render(request, 'saveit/partners/uam.html', context_dict)


def farenetwork(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/farenetwork.png",
                    'title': "Fare network - Partners",
                    'partner_name': "Fare network",
                    'foundation_date': 1999,
                    'country': "United Kingdom",
                    'location': "London",
                    "emblem": False}
    return render(request, 'saveit/partners/farenetwork.html', context_dict)


def altum(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/altum.png",
                    'title': "Altum Foundation - Partners",
                    'partner_name': "Altum Foundation",
                    'foundation_date': 1998,
                    'country': "Spain",
                    'location': "Sevilla",
                    "emblem": False}
    return render(request, 'saveit/partners/altumfoundation.html', context_dict)


def eupen(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/eupen.png",
                    'title': "AS Eupen VoG - Partners",
                    'partner_name': "AS Eupen VoG",
                    'foundation_date': 1945,
                    'country': "Belgium",
                    'location': "Eupen",
                    "emblem": True}
    return render(request, 'saveit/partners/aseupen.html', context_dict)


def panthergaia(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/panther.png",
                    'title': "CD Panther Force Gaia - Partners",
                    'partner_name': "Clube Desportivo Panther Force Gaia",
                    'foundation_date': 2006,
                    'country': "Portugal",
                    'location': "Vila Nova de Gaia",
                    "emblem": True}
    return render(request, 'saveit/partners/pantherforcegaia.html', context_dict)


def colorno(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/colorno.png",
                    'title': "Rugby Colorno - Partners",
                    'partner_name': "Rugby Colorno",
                    'foundation_date': 1975,
                    'country': "Italy",
                    'location': "Colorno",
                    "emblem": True}
    return render(request, 'saveit/partners/rugbycolorno.html', context_dict)


def aips(request):
    context_dict = {'active_page': Tabs.PARTNERS,
                    'image': "/static/images/logos/aips.png",
                    'title': "AIPS - Partners",
                    'partner_name': "International Sports Press Association",
                    'foundation_date': 1924,
                    'country': "France",
                    'location': "Paris",
                    "emblem": False}
    return render(request, 'saveit/partners/aips.html', context_dict)
