from django.db import models

class Tabs:
    HOME = 0
    ABOUT = 1
    PARTNERS = 2
    NEWS = 3
    CONTACT = 4
    FAQS = 5
    EPORTAL = 6