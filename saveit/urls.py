from django.conf.urls import patterns, url
from saveit import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^theProject/$', views.about, name='about'),
        url(r'^partners/$', views.partners, name='partners'),
        url(r'^partners/fundacionculturalista/$', views.fculturalista, name='fundacionculturalista'),
        url(r'^partners/uam/$', views.uam, name='uam'),
        url(r'^partners/farenetwork/$', views.farenetwork, name='farenetwork'),
        url(r'^partners/altumfoundation/$', views.altum, name='altumfoundation'),
        url(r'^partners/aseupen/$', views.eupen, name='aseupen'),
        url(r'^partners/pantherforcegaia/$', views.panthergaia, name='pantherforcegaia'),
        url(r'^partners/rugbycolorno/$', views.colorno, name='rugbycolorno'),
        url(r'^partners/aips/$', views.aips, name='aips'),


        url(r'^news/$', views.news, name='news'),
        url(r'^contact/$', views.contact, name='contact'),
        url(r'^faqs/$', views.faqs, name='faqs'),
        url(r'^eportal/$', views.eportal, name='eportal'))

