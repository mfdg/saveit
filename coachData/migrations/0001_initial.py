# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Coach',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('confirmation_code', models.CharField(default=b'', max_length=33)),
                ('email_confirmed', models.BooleanField(default=False)),
                ('flag_personal', models.BooleanField(default=False)),
                ('flag_profile', models.BooleanField(default=False)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PersonalInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gender', models.CharField(max_length=1, choices=[(b'W', b'Female'), (b'M', b'Male')])),
                ('age', models.CharField(max_length=1, choices=[(b'1', b'Under 18'), (b'2', b'18-24 years'), (b'3', b'25-34 years'), (b'4', b'35-45 years'), (b'5', b'Over 45')])),
                ('city', models.CharField(default=b'', max_length=50)),
                ('country', models.CharField(default=b'', max_length=50)),
                ('sport', models.CharField(default=b'', max_length=50)),
                ('education', models.CharField(max_length=50, choices=[(b'1', b'Some high school'), (b'2', b'High school graduate'), (b'3', b'Some college'), (b'4', b'Trade/technical/vocational training'), (b'5', b'College graduate'), (b'6', b'Some postgraduate work'), (b'7', b'Post-graduate degree'), (b'8', b'Doctorate'), (b'9', b'Other')])),
                ('education_spec', models.CharField(default=b'', max_length=50)),
                ('sport_education', models.CharField(max_length=50, choices=[(b'1', b'Level I (Allows the coaching of any team up to juvenile category)'), (b'2', b'Level II (Allows the coaching of any team up to national juvenile category and regional division)'), (b'3', b'Level III (Allows the coaching of any team, both at national and international level)'), (b'4', b'None')])),
                ('time_coaching', models.IntegerField()),
                ('where_coach', models.CharField(max_length=50, choices=[(b'1', b'School'), (b'2', b'High School'), (b'3', b'Sport Club'), (b'4', b'Association'), (b'5', b'All of the above'), (b'6', b'Other')])),
                ('where_coach_spec', models.CharField(default=b'', max_length=50)),
                ('children_age', models.CharField(max_length=50, choices=[(b'1', b'Under 10'), (b'2', b'10-11 years'), (b'3', b'11-12 years'), (b'4', b'13-14 years'), (b'5', b'15-16 years'), (b'6', b'Over 16')])),
                ('coach', models.ForeignKey(to='coachData.Coach')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('surname', models.CharField(max_length=150)),
                ('user', models.CharField(max_length=30)),
                ('password', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PsychologicalAssessment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=59)),
                ('n_values', models.IntegerField(default=-1)),
                ('comments', models.CharField(default=b'', max_length=256)),
                ('coach', models.ForeignKey(to='coachData.Coach')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('code', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Value',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('votes', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlayerEvaluation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('sportsmanship', models.IntegerField(max_length=1)),
                ('respect', models.IntegerField(max_length=1)),
                ('tolerance', models.IntegerField(max_length=1)),
                ('compassion', models.IntegerField(max_length=1)),
                ('game_enjoyment', models.IntegerField(max_length=1)),
                ('player', models.ForeignKey(to='coachData.Player')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='psychologicalassessment',
            name='value1',
            field=models.ForeignKey(related_name=b'value1', to='coachData.Value'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='psychologicalassessment',
            name='value2',
            field=models.ForeignKey(related_name=b'value2', to='coachData.Value'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='psychologicalassessment',
            name='value3',
            field=models.ForeignKey(related_name=b'value3', to='coachData.Value'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='psychologicalassessment',
            name='value4',
            field=models.ForeignKey(related_name=b'value4', to='coachData.Value'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='psychologicalassessment',
            name='value5',
            field=models.ForeignKey(related_name=b'value5', to='coachData.Value'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='psychologicalassessment',
            name='value6',
            field=models.ForeignKey(related_name=b'value6', to='coachData.Value'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='team',
            field=models.ForeignKey(to='coachData.Team'),
            preserve_default=True,
        ),
    ]
