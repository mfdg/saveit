# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coachData', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Evaluation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mark', models.IntegerField(default=0, max_length=2)),
                ('playerEvaluation', models.ForeignKey(to='coachData.PlayerEvaluation')),
                ('value', models.ForeignKey(to='coachData.Value')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GameResults',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('won', models.IntegerField(default=0, max_length=3)),
                ('drawn', models.IntegerField(default=0, max_length=3)),
                ('lost', models.IntegerField(default=0, max_length=3)),
                ('goalsFor', models.IntegerField(default=0, max_length=4)),
                ('goalsAgainst', models.IntegerField(default=0, max_length=4)),
                ('points', models.IntegerField(default=0, max_length=5)),
                ('player', models.ForeignKey(to='coachData.Player')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='player',
            name='password',
        ),
        migrations.RemoveField(
            model_name='playerevaluation',
            name='compassion',
        ),
        migrations.RemoveField(
            model_name='playerevaluation',
            name='game_enjoyment',
        ),
        migrations.RemoveField(
            model_name='playerevaluation',
            name='respect',
        ),
        migrations.RemoveField(
            model_name='playerevaluation',
            name='sportsmanship',
        ),
        migrations.RemoveField(
            model_name='playerevaluation',
            name='tolerance',
        ),
        migrations.AddField(
            model_name='player',
            name='age',
            field=models.IntegerField(default=0, max_length=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='team',
            name='coach',
            field=models.ManyToManyField(to='coachData.Coach'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='team',
            name='values',
            field=models.ManyToManyField(to='coachData.Value'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='player',
            name='name',
            field=models.CharField(max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='player',
            name='surname',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='player',
            name='user',
            field=models.CharField(unique=True, max_length=30),
        ),
        migrations.AlterField(
            model_name='team',
            name='code',
            field=models.CharField(unique=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='team',
            name='name',
            field=models.CharField(unique=True, max_length=100),
        ),
        migrations.AlterUniqueTogether(
            name='player',
            unique_together=set([('team', 'name', 'surname')]),
        ),
    ]
