from django.contrib import admin
from coachData.models import Coach, Value, PersonalInfo, PsychologicalAssessment, Team, Player, PlayerEvaluation, \
    Evaluation, GameResults


# Register your models here.


class CoachAdmin(admin.ModelAdmin):
    list_display = ('user', 'confirmation_code')


class ValueAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'votes')


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'age', 'team')


class EvaluationAdmin(admin.ModelAdmin):
    list_display = ('playerEvaluation', 'value', 'mark')


# admin.site.register(Coach)
admin.site.register(Coach, CoachAdmin)
admin.site.register(Value, ValueAdmin)
admin.site.register(PersonalInfo)
admin.site.register(PsychologicalAssessment)

admin.site.register(Team)
admin.site.register(Player, PlayerAdmin)
admin.site.register(PlayerEvaluation)
admin.site.register(Evaluation, EvaluationAdmin)
admin.site.register(GameResults)
