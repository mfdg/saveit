from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from coachData.models import Coach, PersonalInfo


class UserForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Username')}))
    email = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Email')}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': _('Password')}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': _('Repeat your password')}))

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class CoachForm(forms.ModelForm):
    class Meta:
        model = Coach
        exclude = ['user', 'confirmation_code', 'flag_personal', 'flag_profile']


class PersonalInfoForm(forms.ModelForm):
    userid = forms.IntegerField()
    gender = forms.ChoiceField((('W', 'Woman'), ('M', 'Man')))
    age = forms.IntegerField()
    city = forms.CharField(max_length=50)
    country = forms.CharField(max_length=50)
    sport = forms.CharField(max_length=50)
    education = forms.CharField(max_length=50)
    education_spec = forms.CharField(max_length=50, required=False)
    sport_education = forms.CharField(max_length=50)
    time_coaching = forms.IntegerField()
    where_coach = forms.CharField(max_length=50)
    where_coach_spec = forms.CharField(max_length=50, required=False)
    children_age = forms.IntegerField()

    def save(self, *args, **kwargs):
        pinfo = PersonalInfo()

        user = User.objects.filter(id__exact=self.cleaned_data['userid'])
        coach = Coach.objects.filter(user__iexact=user)[0]
        coach.flag_personal = True
        coach.save()

        pinfo.coach = coach

        pinfo.gender = self.cleaned_data['gender']
        pinfo.age = self.cleaned_data['age']
        pinfo.city = self.cleaned_data['city']
        pinfo.country = self.cleaned_data['country']
        pinfo.sport = self.cleaned_data['sport']
        pinfo.education = self.cleaned_data['education']
        pinfo.education_spec = self.cleaned_data['education_spec']
        pinfo.sport_education = self.cleaned_data['sport_education']
        pinfo.time_coaching = self.cleaned_data['time_coaching']
        pinfo.where_coach = self.cleaned_data['where_coach']
        pinfo.where_coach_spec = self.cleaned_data['where_coach_spec']
        pinfo.children_age = self.cleaned_data['children_age']
        return pinfo.save()

    class Meta:
        model = PersonalInfo
        exclude = ['coach']
        fields = ('gender', 'age', 'city', 'country', 'sport', 'education', 'sport_education',
                  'time_coaching', 'where_coach', 'children_age',)
