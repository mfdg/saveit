from django.conf.urls import patterns, url
from coachData import views

urlpatterns = patterns('',
       url(r'^$', views.ini_login, name='login'),
       url(r'^coachlogin', views.coach_login, name='coachlogin'),
       url(r'^logout$', views.coach_logout, name='logout'),
       url(r'^coachregister', views.coach_register, name='coachregister'),
       url(r'^coachindex', views.coach_index, name='coachindex'),
       url(r'^tocoachindex', views.to_coach_index, name='tocoachindex'),

       url(r'^personalformdata', views.personal_form_data, name='personal_form_data'),
       url(r'^psychoformdata', views.psycho_form_data, name='psycho_form_data'),
       url(r'^psychoform', views.psycho_form, name='psycho_form'),

       url(r'^addteam', views.add_team, name='addteam'),
       url(r'^addnewteam', views.add_new_team, name='addnewteam'),
       url(r'^deleteteam', views.delete_team, name='deleteteam'),

       url(r'^jointeam', views.join_team, name='jointeam'),
       url(r'^joinnewteam', views.join_new_team, name='joinnewteam'),

       url(r'^evaluate', views.evaluate, name='evaluate'),
       url(r'^evaluations', views.evaluations, name='evaluations'),
       url(r'^saveteamevaluation', views.save_team_evaluation, name='saveteamevaluation'),

       url(r'^gamelogin', views.game_login, name='gamelogin'),
       url(r'^gamesaveresult', views.game_save_result, name='gamesaveresult'),
       url(r'^gamegetranking', views.game_get_ranking, name='gamegetranking'))
