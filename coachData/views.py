from time import time
from datetime import datetime

from django.core import mail
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.utils.translation import ugettext as _
from django.utils.crypto import get_random_string
from django.http import JsonResponse

from django.db import IntegrityError
from coachData.models import Tabs, ValuesNames, User, Coach, Value, PersonalInfo, PsychologicalAssessment, Team, Player, \
    PlayerEvaluation, GameResults, Evaluation
from coachData.forms import UserForm, CoachForm, PersonalInfoForm


def ini_login(request):
    # Si ya logeado: llevarte a pagina personal
    # Si no: login/register
    form = UserForm()
    context_dict = {'title': _("Login"),
                    'login': True,
                    'form': form}
    return render(request, 'coachData/login.html', context_dict)


def coach_logout(request):
    logout(request)
    form = UserForm()
    context_dict = {'title': _("Login"),
                    'login': True,
                    'form': form}
    return render(request, 'coachData/login.html', context_dict)


def coach_login(request):
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        # We use request.POST.get('<variable>') as opposed to request.POST['<variable>'],
        # because the request.POST.get('<variable>') returns None, if the value does not exist,
        # while the request.POST['<variable>'] will raise key error exception

        username = request.POST.get('username')
        password = request.POST.get('password')
        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username.lower(), password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                coach = Coach.objects.filter(user__exact=user)[0]
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return redirect('tocoachindex')
            else:  # An inactive account was used - no logging in!
                # form = UserForm()
                form = UserForm(request.POST)
                context_dict = {'title': _("Login"),
                                'form': form,
                                'login': True,
                                'error': _("Error: this account is disabled")}
                return render(request, 'coachData/login.html', context_dict)
        else:
            # Bad login details were provided. So we can't log the user in.
            # print "Invalid login details: {0}, {1}".format(username, password)
            # form = UserForm()
            form = UserForm(request.POST)
            context_dict = {'title': _("Login"),
                            'form': form,
                            'login': True,
                            'error': _("Error: invalid login details supplied")}
            return render(request, 'coachData/login.html', context_dict)

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        context_dict = {'title': _("Login"),
                        'login': True,
                        'error': _("Error: something was wrong")}
        return render(request, 'coachData/login.html', context_dict)


def to_coach_index(request):
    context_dict = {'active_page': Tabs.HOME,
                    'title': _("Coach index")}
    return render(request, 'coachData/coachindex.html', context_dict)


def coach_register(request):
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        coach_form = CoachForm(data=request.POST)

        # If the form is valid...
        if all((user_form.is_valid(), coach_form.is_valid())):

            if error_register(request):
                # Bad login details were provided. So we can't log the user in.
                context_dict = {'title': _("Register"),
                                'form': user_form,
                                'login': False,
                                'reg_error': _("Error: passwords don't match")}
                return render(request, 'coachData/login.html', context_dict)

            if user_form.cleaned_data['password'] != user_form.cleaned_data['password2']:
                # Bad login details were provided. So we can't log the user in.
                context_dict = {'title': _("Register"),
                                'form': user_form,
                                'login': False,
                                'reg_error': _("Error: passwords don't match")}
                return render(request, 'coachData/login.html', context_dict)

            # Save the user's form data to the database.
            user = user_form.save()

            user.username = user.username.lower()
            # Hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.is_active = False
            user.save()

            # Save the coach's form data to the database.
            coach = coach_form.save(commit=False)
            coach.user = user
            confirmation_code = get_random_string(length=33)
            coach.confirmation_code = confirmation_code
            coach.save()

            send_registration_confirmation(coach)

            # TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # --- LOGIN --- #
            # username = request.POST.get('username')
            # password = request.POST.get('password')
            #
            # user = authenticate(username=username, password=password)
            #
            # login(request, user)
            #
            # context_dict = {'active_page': Tabs.HOME,
            #                 'title': "Coach index"}
            # return render(request, 'coachData/coachindex.html', context_dict)

            context_dict = {'title': _("Login"),
                            'form': user_form,
                            'login': True,
                            'registered': True}
            return render(request, 'coachData/login.html', context_dict)


            # else:
            #     # Bad login details were provided. So we can't log the user in.
            #     # print "Invalid login details: {0}, {1}".format(username, password)
            #     context_dict = {'title': _("Register"),
            #                     'form': user_form,
            #                     'login': False}
            #     return render(request, 'coachData/login.html', context_dict)

    else:
        # Bad register details were provided. So we can't log the user in.
        user_form = UserForm()

    context_dict = {'title': _("Register"),
                    'form': user_form,
                    'login': False}
    return render(request, 'coachData/login.html', context_dict)


def coach_index(request):
    form = PersonalInfoForm()  # request.POST

    # if form.is_valid():
    #     # Save the user's form data to the database.
    #     form.save()
    # else:
    #     print form.errors

    context_dict = {'active_page': Tabs.HOME,
                    'title': _("Coach index"),
                    'form': form}
    return render(request, 'coachData/coachindex.html',
                  context_dict)


def add_team(request):
    values = Value.objects.all()
    context_dict = {'active_page': Tabs.ADD_TEAM, 'values': values,
                    'title': _("Add team")}
    return render(request, 'coachData/addTeam.html', context_dict)


def add_new_team(request):
    user_id = request.GET.get('id')
    code = str(int(round(time() * 10)))
    letter1 = chr(int(code[0]) + 65)
    letter2 = chr(int(code[1]) + 65)
    letter3 = chr(int(code[2]) + 65)

    code = code[3:] + letter1 + letter2 + letter3

    team_name = request.GET.get('team')

    try:
        if Team.objects.get(name=team_name):
            data = {
                'response': "ERR",
                'reason': _("This team already exists")
            }

            return JsonResponse(data)

    except Team.DoesNotExist:
        # Team can be created
        pass

    team = Team(name=team_name, code=code)
    team.save()

    values = request.GET.getlist('values[]')
    for value in values:
        value_aux = Value.objects.filter(id=int(value))[0]
        team.values.add(value_aux)
        team.save()

    coach = Coach.objects.filter(user=user_id)[0]

    team.coach.add(coach)
    team.save()

    names = request.GET.getlist('names[]')
    surnames = request.GET.getlist('surnames[]')
    ages = request.GET.getlist('ages[]')

    i = 0
    # Create all the players
    for name in names:
        user = team_name + str(i + 1)
        try:
            player = Player(team=team, name=name, surname=surnames[i], user=user, age=ages[i])
            print ages[i]
            player.save()
            i += 1
            print player

            result = GameResults(player=player, won=0, drawn=0, lost=0, goalsFor=0, goalsAgainst=0, points=0)
            result.save()

        except IntegrityError:
            print "IntegrityError"
            data = {
                'response': "ERR",
                'reason': _("There are several players with the same identifications or empty fields")
            }
            Team.objects.filter(id=team.id).delete()

            return JsonResponse(data)

    data = {
        'response': "OK"
    }

    return JsonResponse(data)


def delete_team(request):
    team_name = request.GET.get('team')
    team = Team.objects.filter(name=team_name)

    if team:
        team.delete()

    data = {
        'response': "OK"
    }
    return JsonResponse(data)


def join_team(request):
    user_id = request.POST.get('userID', None)
    coach = Coach.objects.filter(user=user_id)
    teams = Team.objects.all().exclude(coach=coach)

    context_dict = {'active_page': Tabs.JOIN_TEAM,
                    'teams': teams,
                    'title': _("Join team")}
    return render(request, 'coachData/joinTeam.html', context_dict)


def join_new_team(request):
    user_id = request.GET.get('id')
    team_name = request.GET.get('team_name')
    team_code = request.GET.get('team_code')

    try:
        team = Team.objects.get(name=team_name)
        print team
        if team:
            if team.code == team_code:
                coach = Coach.objects.get(user=user_id)
                team.coach.add(coach)
                team.save()

                data = {
                    'response': "OK"
                }
                return JsonResponse(data)

            else:
                data = {
                    'response': "ERR",
                    'reason': _("Code incorrect")
                }
                return JsonResponse(data)

    except Team.DoesNotExist:
        data = {
            'response': "ERR",
            'reason': _("This team doesn't exist")
        }
        return JsonResponse(data)


def evaluate(request):
    user_id = request.POST.get('userID', None)
    coach = Coach.objects.filter(user=user_id)
    teams = Team.objects.filter(coach=coach)

    players = {}
    for team in teams:
        players[team] = Player.objects.filter(team=team)

    context_dict = {'active_page': Tabs.EVALUATE,
                    'teams': teams,
                    'players': players,
                    'title': _("Evaluate")}
    return render(request, 'coachData/evaluateTeam.html', context_dict)


def evaluations(request):
    user_id = request.POST.get('userID', None)
    coach = Coach.objects.filter(user=user_id)
    teams = Team.objects.filter(coach=coach)

    # Players of the coach's teams
    players_aux = Player.objects.filter(team__in=teams)
    # Values of those players
    player_evaluations = PlayerEvaluation.objects.filter(player__in=players_aux)

    evals = Evaluation.objects.filter(playerEvaluation__in=player_evaluations)

    # Coach's evaluated teams
    teams_evaluated = []
    for ev in evals:
        print ev
        if ev.playerEvaluation.player.team not in teams_evaluated:
            teams_evaluated.append(ev.playerEvaluation.player.team)

    # Player of the evaluated teams
    players = {}
    for team in teams_evaluated:
        players[team] = Player.objects.filter(team=team)

    context_dict = {'active_page': Tabs.EVALUATIONS,
                    'teams': teams_evaluated,
                    'players': players,
                    'evaluations': evals,
                    # 'values_names': ValuesNames,
                    'title': _("Evaluations")}
    return render(request, 'coachData/evaluations.html', context_dict)


def save_team_evaluation(request):
    team = request.GET.get('team')

    print team

    try:
        team = Team.objects.get(name=team)
        if team:
            players = request.GET.getlist('players[]')
            values = request.GET.getlist('values[]')

            team_values = team.values.all()
            n_team_values = team_values.__len__()

            i = 0
            # Save the evaluation of all the players
            for user in players:
                player = Player.objects.filter(user=user)[0]

                if player:
                    # timestamp = time()  # .strftime('%Y-%m-%d %H:%M:%S')
                    # print timestamp

                    player_evaluation = PlayerEvaluation(player=player, date=datetime.now())
                    player_evaluation.save()

                    n_value = 0
                    for value in team_values:
                        value_aux = Value.objects.filter(name=value)[0]
                        evaluation = Evaluation(value=value_aux, mark=values[i * n_team_values + n_value],
                                                playerEvaluation=player_evaluation)
                        evaluation.save()
                        n_value += 1

                else:
                    print "This player doesn't exist"

                i += 1

            data = {
                'response': "OK"
            }

            return JsonResponse(data)

    except Team.DoesNotExist:
        data = {
            'response': "ERR",
            'reason': _("This team doesn't exist")
        }

        return JsonResponse(data)


# --------------------- #
# -      Forms        - #
# --------------------- #
def psycho_form(request):
    results = request.GET.get('results', None)
    nval_results = request.GET.get('nvalues', None)
    values_results = request.GET.getlist('values[]')
    comments_results = request.GET.get('comments', None)
    if not comments_results:
        comments_results = ""
    user_id = request.GET.get('user_id', None)
    user = User.objects.filter(id__exact=user_id)
    coach = Coach.objects.filter(user__iexact=user)[0]

    if nval_results and values_results:
        value1 = Value.objects.filter(id__iexact=values_results[0])[0]
        value2 = Value.objects.filter(id__iexact=values_results[1])[0]
        value3 = Value.objects.filter(id__iexact=values_results[2])[0]
        value4 = Value.objects.filter(id__iexact=values_results[3])[0]
        value5 = Value.objects.filter(id__iexact=values_results[4])[0]
        value6 = Value.objects.filter(id__iexact=values_results[5])[0]

        psycho_assess = PsychologicalAssessment(coach=coach, answer=results, n_values=nval_results,
                                                value1=value1, value2=value2, value3=value3,
                                                value4=value4, value5=value5, value6=value6,
                                                comments=comments_results)
        psycho_assess.save()

        value1.votes += 1
        value1.save()
        value2.votes += 1
        value2.save()
        value3.votes += 1
        value3.save()
        value4.votes += 1
        value4.save()
        value5.votes += 1
        value5.save()
        value6.votes += 1
        value6.save()

        coach.flag_profile = True
        coach.save()

        data = {
            'response': "OK",
            'user': user_id  # user.username
        }
        # data = {}
        # data['response'] = 'OK'
        # data['user'] = user_id

    else:

        data = {
            'response': "ERR",
            'user': user_id  # user.username
        }
        # data = {}
        # data['response'] = 'ERROR'
        # data['user'] = user_id

    return JsonResponse(data)


def personal_form_data(request):
    username = request.GET.get('username', None)
    user = User.objects.filter(username__iexact=username)[0]
    if user:
        coach = Coach.objects.filter(user__exact=user)[0]
        if coach:
            pinfo = PersonalInfo.objects.filter(coach__exact=coach)[0]
            if pinfo:
                data = {
                    'response': "OK",
                    'gender': pinfo.gender,
                    'age': pinfo.age,
                    'city': pinfo.city,
                    'country': pinfo.country,
                    'sport': pinfo.sport,
                    'education': pinfo.education,
                    'education_spec': pinfo.education_spec,
                    'sport_education': pinfo.sport_education,
                    'time_coaching': pinfo.time_coaching,
                    'where_coach': pinfo.where_coach,
                    'where_coach_spec': pinfo.where_coach_spec,
                    'children_age': pinfo.children_age
                }
            else:
                data = {'response': "ERR", 'error': "No PersonalInfo match."}
        else:
            data = {'response': "ERR", 'error': "No Coach match."}
    else:
        data = {'response': "ERR", 'error': "No User match."}
    return JsonResponse(data)


def psycho_form_data(request):
    username = request.GET.get('username', None)
    user = User.objects.filter(username__iexact=username)[0]
    if user:
        coach = Coach.objects.filter(user__exact=user)[0]
        if coach:
            passess = PsychologicalAssessment.objects.filter(coach__exact=coach)[0]
            if passess:
                data = {
                    'response': "OK",
                    'answer': passess.answer,
                    'nvalues': passess.n_values,
                    'value1': passess.value1.id,
                    'value2': passess.value2.id,
                    'value3': passess.value3.id,
                    'value4': passess.value4.id,
                    'value5': passess.value5.id,
                    'value6': passess.value6.id,
                    'comments': passess.comments
                }
            else:
                data = {'response': "ERR", 'error': "No PersonalInfo match."}
        else:
            data = {'response': "ERR", 'error': "No Coach match."}
    else:
        data = {'response': "ERR", 'error': "No User match."}
    return JsonResponse(data)


# -------------------- #
# -    Auxiliar      - #
# -------------------- #
def error_register(request):
    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']

    if username.replace(" ", "") == "" or password.replace(" ", "") == "":
        return True
    if len(username) > 15 or len(password) > 50:
        return True
    if not "@" in email:
        return True
    try:
        if User.objects.get(username=username):
            return True
    except User.DoesNotExist:
        pass


# TODO
def send_registration_confirmation(coach):
    confirmation_mail = 'pinar.sanz@estudiante.uam.es'
    title = "Save it account confirmation"
    content = "http://www.saveitproject.eu/confirm/" + str(coach.confirmation_code) + "/" + coach.user.username
    # send_mail(title, content, 'no-reply@saveit.com', [confirmation_mail], fail_silently=False)

    # try:
    #     send_mail("", "", 'no-reply@saveit.com', [confirmation_mail])
    # except BadHeaderError:
    #     return HttpResponse('Invalid header found.')
    # return HttpResponseRedirect('/contact/thanks/')

    html_content = "<strong>Comment tu vas?</strong>"
    email = mail.EmailMessage("my subject", html_content, "no-reply@saveit.com", ["pinar.sanz@estudiante.uam.es"])
    email.content_subtype = "html"
    res = email.send()
    return HttpResponse('%s' % res)


def confirm(request, confirmation_code, username):
    try:
        user = User.objects.get(username=username)
        coach = Coach.objects.get(user=user)

        # confirmation_code esta en coach sacarlo de ahi y comparar
        # profile = user.get_profile()
        if coach.confirmation_code == confirmation_code:
            user.is_active = True
            user.save()
            # user.backend = 'django.contrib.auth.backends.ModelBackend'
            # auth_login(request, user)
        context_dict = {'title': _("Login"),
                        'login': True,
                        'error': _("Registration confirmed")}
        return render(request, 'coachData/login.html', context_dict)
    except:
        context_dict = {'title': _("Login"),
                        'login': True,
                        'error': _("Error: registration unable to be confirmed")}
        return render(request, 'coachData/login.html', context_dict)


#############################
#
#   JUEGO
#
#############################

# Called from the game to verify a login and get the evaluation of the player
def game_login(request):
    user = request.GET.get('user', None)
    player = Player.objects.filter(user__iexact=user)[0]
    
    if player:

        playerEvaluationsList = PlayerEvaluation.objects.filter(player=player)  # .order_by('-date')

        if playerEvaluationsList:
            playerEvaluations = playerEvaluationsList[0]

            evaluations = Evaluation.objects.filter(playerEvaluation=playerEvaluations)
            evaluations_marks = {}

            if evaluations:
                i = 0
                for evaluation in evaluations:
                    evaluations_marks[i] = evaluation.mark
                    i += 1
                data = {'response': "OK", 'evaluations': evaluations_marks, 'evaluated': True, 'max_mark': 10}
            else:
                data = {'response': "OK", 'evaluations': evaluations_marks, 'evaluated': False, 'max_mark': 10}
        else:
            data = {'response': "OK", 'evaluated': False, 'max_mark': 10}

    else:
        data = {'response': "ERR", 'error': _("Incorrect user")}
    return JsonResponse(data)


# Called from the game to save a match's result
def game_save_result(request):
    user = request.GET.get('user', None)
    player = Player.objects.filter(user__iexact=user)[0]

    if player:
        result_list = GameResults.objects.filter(player=player)

        if result_list:
            result = result_list[0]
            result.goalsFor += int(request.GET.get('goalsFor', 0))
            result.goalsAgainst += int(request.GET.get('goalsAgainst', 0))
            result.save()
        else:
            result = GameResults(player=player, goalsFor=int(request.GET.get('goalsFor', 0)),
                                 goalsAgainst=int(request.GET.get('goalsAgainst', 0)))
            result.save()

        final_result = int(request.GET.get('result', 0))

        if final_result == 1:
            result.won += 1
        elif final_result == 0:
            result.drawn += 1
        else:
            result.lost += 1

        result.points = 3 * result.won + result.drawn
        result.save()

        data = {'response': "OK"}
    else:
        data = {'response': "ERR", 'error': _("Incorrect user")}
    return JsonResponse(data)


# Called from the game to get the global results
def game_get_ranking(request):
    user = request.GET.get('user', None)
    player = Player.objects.filter(user__iexact=user)[0]
    players_ranking = GameResults.objects.filter(player__team=player.team).order_by('-points')

    users = {}
    won = {}
    drawn = {}
    lost = {}
    goalsFor = {}
    goalsAgainst = {}
    points = {}
    i = 0
    for player_result in players_ranking:
        users[i] = player_result.player.user
        won[i] = player_result.won
        drawn[i] = player_result.drawn
        lost[i] = player_result.lost
        goalsFor[i] = player_result.goalsFor
        goalsAgainst[i] = player_result.goalsAgainst
        points[i] = player_result.points
        i += 1

    data = {'response': "OK", 'user': users, 'won': won, 'drawn': drawn, 'lost': lost, 'goalsFor': goalsFor,
            'goalsAgainst': goalsAgainst, 'points': points}

    return JsonResponse(data)
