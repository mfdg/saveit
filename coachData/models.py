from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Tabs:
    HOME = 0
    ADD_TEAM = 1
    JOIN_TEAM = 2
    EVALUATE = 3
    EVALUATIONS = 4


ValuesNames = ["Sportsmanship", "Respect", "Tolerance", "Compassion", "Game enjoyment"]


class Coach(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    confirmation_code = models.CharField(max_length=33, default="")
    email_confirmed = models.BooleanField(default=False)
    flag_personal = models.BooleanField(default=False)
    flag_profile = models.BooleanField(default=False)

    def __unicode__(self):  # __unicode__ on Python 2
        return self.user.username


class Value(models.Model):
    name = models.CharField(max_length=50)
    votes = models.IntegerField(default=0)

    def __unicode__(self):  # __unicode__ on Python 2
        return self.name


class PersonalInfo(models.Model):
    GENDER = (
        ('W', 'Female'),
        ('M', 'Male')
    )
    AGE = (
        ('1', 'Under 18'),
        ('2', '18-24 years'),
        ('3', '25-34 years'),
        ('4', '35-45 years'),
        ('5', 'Over 45')
    )
    EDUCATION = (
        ('1', 'Some high school'),
        ('2', 'High school graduate'),
        ('3', 'Some college'),
        ('4', 'Trade/technical/vocational training'),
        ('5', 'College graduate'),
        ('6', 'Some postgraduate work'),
        ('7', 'Post-graduate degree'),
        ('8', 'Doctorate'),
        ('9', 'Other')
    )
    SPORT_EDUCATION = (
        ('1', 'Level I (Allows the coaching of any team up to juvenile category)'),
        ('2', 'Level II (Allows the coaching of any team up to national juvenile category and regional division)'),
        ('3', 'Level III (Allows the coaching of any team, both at national and international level)'),
        ('4', 'None')
    )
    WHERE_COACH = (
        ('1', 'School'),
        ('2', 'High School'),
        ('3', 'Sport Club'),
        ('4', 'Association'),
        ('5', 'All of the above'),
        ('6', 'Other')
    )
    CHILDREN_AGE = (
        ('1', 'Under 10'),
        ('2', '10-11 years'),
        ('3', '11-12 years'),
        ('4', '13-14 years'),
        ('5', '15-16 years'),
        ('6', 'Over 16')
    )
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    gender = models.CharField(max_length=1, choices=GENDER)
    age = models.CharField(max_length=1, choices=AGE)
    city = models.CharField(max_length=50, default="")
    country = models.CharField(max_length=50, default="")
    sport = models.CharField(max_length=50, default="")
    education = models.CharField(max_length=50, choices=EDUCATION)
    education_spec = models.CharField(max_length=50, default="")
    sport_education = models.CharField(max_length=50, choices=SPORT_EDUCATION)
    time_coaching = models.IntegerField()
    where_coach = models.CharField(max_length=50, choices=WHERE_COACH)
    where_coach_spec = models.CharField(max_length=50, default="")
    children_age = models.CharField(max_length=50, choices=CHILDREN_AGE)

    def __unicode__(self):
        return self.coach + " personal info"


class PsychologicalAssessment(models.Model):
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    answer = models.CharField(max_length=59)
    n_values = models.IntegerField(default=-1)
    comments = models.CharField(max_length=256, default="")
    value1 = models.ForeignKey(Value, related_name='value1', on_delete=models.CASCADE)
    value2 = models.ForeignKey(Value, related_name='value2', on_delete=models.CASCADE)
    value3 = models.ForeignKey(Value, related_name='value3', on_delete=models.CASCADE)
    value4 = models.ForeignKey(Value, related_name='value4', on_delete=models.CASCADE)
    value5 = models.ForeignKey(Value, related_name='value5', on_delete=models.CASCADE)
    value6 = models.ForeignKey(Value, related_name='value6', on_delete=models.CASCADE)

    def __unicode__(self):
        return self.coach + ": " + self.value1 + self.value2 + self.value3 + self.value4 + self.value5


# ------------------------------------------- #
#
#          TEAMS AND EVALUATIONS
#
# ------------------------------------------- #

class Team(models.Model):
    name = models.CharField(max_length=100, unique=True)
    code = models.CharField(max_length=50, unique=True)
    coach = models.ManyToManyField(Coach)
    values = models.ManyToManyField(Value)

    def __unicode__(self):
        return self.name + " (" + self.code + ")"


class Player(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=True)
    surname = models.CharField(max_length=150, null=True)
    user = models.CharField(max_length=30, unique=True)
    age = models.IntegerField(max_length=2, default=0)

    # password = models.CharField(max_length=30)

    class Meta:
        unique_together = ('team', 'name', 'surname',)

    def __unicode__(self):
        return self.name + " " + self.surname  # + ": " + self.team


class PlayerEvaluation(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    date = models.DateField()

    def __unicode__(self):
        return self.player.__unicode__()


class Evaluation(models.Model):
    value = models.ForeignKey(Value, on_delete=models.CASCADE)
    # Values in [1,5]
    mark = models.IntegerField(max_length=2, default=0)
    playerEvaluation = models.ForeignKey(PlayerEvaluation, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.value.__unicode__() + ": " + str(self.mark) + " (" + self.playerEvaluation.__unicode__() + ")"


class GameResults(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    won = models.IntegerField(max_length=3, default=0)
    drawn = models.IntegerField(max_length=3, default=0)
    lost = models.IntegerField(max_length=3, default=0)
    goalsFor = models.IntegerField(max_length=4, default=0)
    goalsAgainst = models.IntegerField(max_length=4, default=0)
    points = models.IntegerField(max_length=5, default=0)

    def __unicode__(self):
        return self.player.__unicode__() + ": W" + str(self.won) + " - D" + str(self.drawn) + " - L" + str(self.lost) + \
               " (F " + str(self.goalsFor) + ", A " + str(self.goalsAgainst) + ") : " + str(self.points)
