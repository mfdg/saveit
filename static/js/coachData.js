//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
var scale_in, scale_out;
var is_personal_info = false;

// PSYCHOLOGY ASSESSMENT
QUESTIONS = [29, 5, 25];

$('#show_register').click(function () {
    if (animating) return false;
    animating = true;

    // Hide the login form and show the register form with style
    $('#login_form').animate({opacity: 0}, {
        step: function (now, mx) {
            // Scale from 80% to 100%
            scale_in = 0.8 + (1 - now) * 0.2;
            // Scale down to 80%
            scale_out = 1 - (1 - now) * 0.2;
            // Increase the opacity
            opacity = 1 - now;

            $('#register_form').css({'transform': 'scale(' + scale_in + ')', 'opacity': opacity});
            $('#login_form').css({'transform': 'scale(' + scale_out + ')'});
        },
        duration: 800,
        complete: function () {
            $('#login_form').hide();
            animating = false;
            $('#register_form').show();
        },
        // This comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$('#show_login').click(function () {
    if (animating) return false;
    animating = true;

    // Show the login form and hide the register form with style
    $('#register_form').animate({opacity: 0}, {
        step: function (now, mx) {
            // Scale from 80% to 100%
            scale_in = 0.8 + (1 - now) * 0.2;
            // Scale down to 80%
            scale_out = 1 - (1 - now) * 0.2;
            // Increase the opacity
            opacity = 1 - now;

            $('#login_form').css({'transform': 'scale(' + scale_in + ')', 'opacity': opacity});
            $('#register_form').css({'transform': 'scale(' + scale_out + ')'});
        },
        duration: 800,
        complete: function () {
            $('#register_form').hide();
            animating = false;
            $('#login_form').show();
        },
        // This comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$('#show_personal_form').click(function () {
    is_personal_info = true;
    $('#psycho_info_content').hide();
    $('#coaching_profile_errors').hide();
    $('#psycho_square').css({'border-color': '#9d9d9d'});
    $('#personal_square').css({'border-color': '#d82835'});
    $("#personal_info_content :input").prop('disabled', false);
    $("#personal_info_content :input[type='submit']").show();
    $('#personal_info_content').show();
});

$('#show_personal_form_disabled').click(function () {
    is_personal_info = true;
    $('#psycho_info_content').hide();
    $('#psycho_square').css({'border-color': '#9d9d9d'});
    $('#personal_square').css({'border-color': '#d82835'});

    var username = $('.container > h1').text().toLowerCase();

    $.ajax({
        url: '/coachdata/personalformdata/',
        data: {
            'username': username
        },
        dataType: 'json',
        success: function (data) {
            $('input:radio[name="gender"]').filter('[value="' + data.gender + '"]').prop('checked', true);
            $('input:radio[name="age"]').filter('[value="' + data.age + '"]').prop('checked', true);
            $('#city').val(data.city);
            $('#country').val(data.country);
            $('#sport').val(data.sport);
            $('input:radio[name="education"]').filter('[value="' + data.education + '"]').prop('checked', true);
            $('input:text[name="education_spec"]').val(data.education_spec);
            $('input:radio[name="sport_education"]').filter('[value="' + data.sport_education + '"]').prop('checked', true);

            $('#time_coaching').val(data.time_coaching);

            $('input:radio[name="where_coach"]').filter('[value="' + data.where_coach + '"]').prop('checked', true);
            $('input:text[name="where_coach_spec"]').val(data.where_coach_spec);
            $('input:radio[name="children_age"]').filter('[value="' + data.children_age + '"]').prop('checked', true);
        }
    });

    $("#personal_info_content :input").prop('disabled', true);
    $("#personal_info_content :input[type='button']").prop('disabled', false);
    $("#personal_form_submit").hide();
    $('#personal_info_content').show();
});

$('#show_psycho_form').click(function () {
    is_personal_info = false;
    $('#personal_info_content').hide();
    $('#personal_profile_errors').hide();
    $('#personal_profile_sv_errors').hide();
    $('#personal_square').css({'border-color': '#9d9d9d'});
    $('#psycho_square').css({'border-color': '#d82835'});
    $("#psycho_info_content :input").prop('disabled', false);
    $("#psycho_info_content :input[type='submit']").show();
    $('#psycho_info_content').show();
});

$('#show_psycho_form_disabled').click(function () {
    is_personal_info = false;
    $('#personal_info_content').hide();
    $('#personal_square').css({'border-color': '#9d9d9d'});
    $('#psycho_square').css({'border-color': '#d82835'});

    var username = $('.container > h1').text().toLowerCase();

    $.ajax({
        url: '/coachdata/psychoformdata/',
        data: {
            'username': username
        },
        dataType: 'json',
        success: function (data) {

            var counter = 0;
            for (var t = 1; t <= QUESTIONS.length; t++) {
                for (var q = 1; q <= QUESTIONS[t - 1]; q++, counter++) {
                    $('input[name=t' + t + 'q' + q + ']', '#psycho_info_form').each(function () {
                        $(this).prop('disabled', true);
                        if (data.answer[counter] == $(this).val()) {
                            $(this).prop('checked', true);
                        }
                    });
                }
            }

            $('#nvalues').val(data.nvalues);
            console.log(data.value1 + " - " + data.value2);
            $('#value_checkboxes > div :checkbox').each(function () {
                if ($(this).val() == data.value1 || $(this).val() == data.value2 || $(this).val() == data.value3
                    || $(this).val() == data.value4 || $(this).val() == data.value5 || $(this).val() == data.value6) {
                    $(this).prop('checked', true);
                }
            });
            $('#comments').val(data.comments);
        }
    });

    $("#psycho_info_content :input").prop('disabled', true);
    $("#psycho_info_content :input[type='button']").prop('disabled', false);
    $("#psycho_form_submit").hide();
    $('#psycho_info_content').show();
});

$('#value_checkboxes :checkbox').change(function () {
    var $cs = $('#value_checkboxes').find(':checkbox:checked');
    if ($cs.length > 6) {
        this.checked = false;
    }
});

$(".reg_next").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'transform': 'scale(' + scale + ')'});
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
            next_fs.show();
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });

    //show the next fieldset
    //next_fs.show();
});

$(".reg_previous").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
            previous_fs.show();
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$('.reg_finish').click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().prev().prev();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //1. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //2. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            //current_fs.css({'transform': 'scale('+scale+')'});
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
            next_fs.show();


            if (is_personal_info) {
                $('#personal_info_content').hide();
                $('#show_personal_form').hide();

                $('#personal_square').css({'border-color': '#9d9d9d'});
            } else {
                $('#psycho_info_content').hide();
                $('#show_psycho_form').hide();

                $('#psycho_square').css({'border-color': '#9d9d9d'});
            }
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".reg_submit").click(function () {
    return false;
});

// Change input value on label click
// $('.range-labels li').on('click', function () {
//     var index = $(this).index();
//     $rangeInput = $('.range input');
//     $rangeInput.val(index).trigger('input');
// });

/****************************************
 *
 *             DATATABLES
 *
 ****************************************/
var emptyTable = "There are no teams";
var emptyTablePlayers = "There are no players";
var lengthMenu = ""; //_MENU_ /page
var zeroRecords = "No results";
var info = "Page _PAGE_ of _PAGES_";
var search = "Search:";
var next = ">";
var previous = "<";

$.extend(true, $.fn.dataTable.defaults, {
    "searching": false,
    "ordering": false,
    "paging": false,
    "info": false,
    "responsive": true,
    "autoWidth": false,
    "language": {
        "emptyTable": emptyTable,
        "lengthMenu": lengthMenu,
        "zeroRecords": zeroRecords,
        "info": info,
        "search": search,
        "paginate": {
            "next": next,
            "previous": previous
        }
    }
});

$(document).ready(function () {
    $('#test1').DataTable({
        "ajax": '../../static/ajax/' + lang + '/test1.txt'
    });
    $('#test2').DataTable({
        "ajax": '../../static/ajax/' + lang + '/test2.txt'
    });
    $('#test3').DataTable({
        "ajax": '../../static/ajax/' + lang + '/test3.txt'
    });
    $('#teams').DataTable();

    $('#join_teams').DataTable({
        "searching": true,
        "ordering": true,
        "paging": true
    });
});

/*
 * Control the second up scroll in the coaching form scroll
 */
$(function () {
    $(".test1_wrapper1").scroll(function () {
        $("#test1_wrapper")
            .scrollLeft($(".test1_wrapper1").scrollLeft());
    });
    $(".test1_wrapper").scroll(function () {
        $(".test1_wrapper1")
            .scrollLeft($(".test1_wrapper").scrollLeft());
    });
});

var rowCounter = 1;

$(document).ready(function () {
    var t = $('#addTeam').DataTable();

    $('#addRow').on('click', function () {
        t.row.add([
            '<input id="name' + rowCounter + '" name="name' + rowCounter + '" value="" type="text">',
            '<input id="surname' + rowCounter + '" name="surname' + rowCounter + '" value="" type="text">',
            '<input id="age' + rowCounter + '" name="age' + rowCounter + '" value="" type="number" min=0>',
            '<img class="table_icon" src="../../static/images/coachdata/delete-icon.png" alt="">'
        ]).draw(false);

        rowCounter++;
    });

    // Automatically add a first row of data
    $('#addRow').click();
});

var table = $('#addTeam').DataTable({
    "language": {
        "emptyTable": emptyTablePlayers
    }
});

$('#addTeam tbody').on('click', 'img', function () {
    table
        .row($(this).parents('tr'))
        .remove().draw();
});

var showed_team = null;

$(document).ready(function () {
    var table = $('#teams').DataTable();

    $('#teams tbody').on('click', 'tr', function () {

        var data = table.row(this).data();

        if (table.page.info().recordsTotal == 0)
            return;

        //Show the table with the players of this team
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $('#' + data[1]).hide();
            $('#ev_save_button').hide();
            $('#ev_error_empty').hide();
            $('#ev_error_ajax').hide();
            $("#ev_saved").hide();
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            if (showed_team != null) {
                showed_team.hide();
                $('#ev_error_empty').hide();
                $('#ev_error_ajax').hide();
                $("#ev_saved").hide();
            }
            // showed_team.css({'display': 'none'});
            showed_team = $('#' + data[1]);
            showed_team.show();
            $('#ev_save_button').show();
        }
    });

    var join_teams_table = $('#join_teams').DataTable();

    $('#join_teams tbody').on('click', 'tr', function () {

        var data = join_teams_table.row(this).data();

        if (join_teams_table.page.info().recordsTotal == 0)
            return;

        //Show the table with the players of this team
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $("#team_chose").text("");
        }
        else {
            join_teams_table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            $("#team_chosen").text(data[0]);
        }
    });
});


/****************************************
 *
 *         FUNCTION ON CLICK
 *
 ****************************************/
$('#personal_form_submit').on('click', function () {
    var errors = false;
    var input = document.forms["personal_info_form"]["gender"].value;

    if (input == '' || input == null) {
        errors = true;
        $('#error_genre').show();
    }
    input = document.forms["personal_info_form"]["age"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_age').show();
    }
    input = document.forms["personal_info_form"]["city"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_city').show();
    }
    input = document.forms["personal_info_form"]["country"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_country').show();
    }
    input = document.forms["personal_info_form"]["sport"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_sport').show();
    }
    input = document.forms["personal_info_form"]["education"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_education').show();
    }
    input = document.forms["personal_info_form"]["sport_education"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_sport_education').show();
    }
    input = document.forms["personal_info_form"]["where_coach"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_where').show();
    }
    input = document.forms["personal_info_form"]["children_age"].value;
    if (input == '' || input == null) {
        errors = true;
        $('#error_children_age').show();
    }

    // If any field is empty: return error
    if (errors) {
        $('#personal_profile_errors').show();
        return;
    }

    document.getElementById("personal_info_form").submit();

    finish_effect_form("#personal_form_submit");
    $('#personal_profile_errors').hide();
});

$('#psycho_form_submit').on('click', function () {
    var psychoForm = [];

    for (var t = 1; t <= QUESTIONS.length; t++) {
        for (var q = 1; q <= QUESTIONS[t - 1]; q++) {
            if (typeof($('input[name=t' + t + 'q' + q + ']:checked', '#psycho_info_form').val()) === 'undefined') {
                // Empty field error
                $('#coaching_profile_errors').show();
                return;
                // psychoForm.push('0');
            } else
                psychoForm.push($('input[name=t' + t + 'q' + q + ']:checked', '#psycho_info_form').val());
        }
    }

    var string = psychoForm.join("");
    var user_id = $("#user_id").text();

    var nvalues = $("#nvalues").val();
    var checked = $('#value_checkboxes > div').find(':checkbox:checked');
    var values = [];
    for (var i = 0; i < checked.length; i++) {
        values[i] = checked[i].value;
    }
    var comments = $("#comments").val();

    if (!nvalues || !values) {
        $('#coaching_profile_errors').show();
        return;
    }
    if (values.length != 6) {
        $('#coaching_profile_errors').show();
        return;
    }

    $.ajax({
        url: '/coachdata/psychoform/',
        data: {
            'results': string,
            'user_id': user_id,
            'nvalues': nvalues,
            'values': values,
            'comments': comments
        },
        dataType: 'json',
        success: function (data) {
            if (data['response'] == "OK") {
                $('#psycho_incompleted').hide();
                $('#psycho_completed').show();
            } else {
                $('#psycho_incompleted').hide();
                $('#psycho_error').show();
            }
            $('#coaching_profile_errors').hide();
        },
        error: function (data) {
            $('#psycho_incompleted').hide();
            $('#psycho_error').show();
            $('#coaching_profile_errors').hide();
        }
    });

    finish_effect_form("#psycho_form_submit");
});

$('#login_fieldset_submit').on('click', function () {
    var passfield = document.getElementById('dummy_password');
    var pass = passfield.value.toString().hashCode();
    document.getElementById('log_password').value = pass.toString();

    document.getElementById("login_form").submit();
});

$('#register_fieldset_submit').on('click', function () {
    var passfield = document.getElementById('dummy_reg_password');
    var pass = passfield.value.toString().hashCode();
    document.getElementById("reg_password").value = pass.toString();

    var passfield2 = document.getElementById('dummy_reg_password2');
    var pass2 = passfield2.value.toString().hashCode();
    document.getElementById("reg_password2").value = pass2.toString();

    document.getElementById("register_form").submit();
});

// Save a new team
$('#save_team').on('click', function () {
    var id = $('#user_id').text();
    var team = $('#team').val();
    var names = [];
    var surnames = [];
    var ages = [];

    var inputs = $('table input:text');
    var numbers = $(':input[type="number"]');

    for (var i = 0; i < inputs.length / 2; i++) {

        names[i] = inputs[2 * i].value;
        surnames[i] = inputs[2 * i + 1].value;
        ages[i] = numbers[i].value;

        if (names[i] == '' || surnames[i] == '' || team == '' || ages[i] == '') {
            $("#team_recorded").hide();
            $("#addTeam_error_title").show();
            $("#addTeam_error").hide();
            $("#addTeam_empty_error").show();
            return;
        }
    }

    var checkboxes = $('input:checkbox');
    var values = [];
    var noChecked = true;
    console.log(checkboxes.length );
    for (i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            console.log(checkboxes[i].value);
            values.push(checkboxes[i].value);
            noChecked = false;
        }
    }

    if (noChecked) {
        $("#team_recorded").hide();
        $("#addTeam_error_title").show();
        $("#addTeam_error").hide();
        $("#addTeam_empty_error").show();
        return;
    }

    $.ajax({
        url: '/coachdata/addnewteam/',
        data: {
            'id': id,
            'team': team,
            'names': names,
            'surnames': surnames,
            'ages': ages,
            'values': values
        },
        dataType: 'json',
        success: function (data) {
            if (data['response'] == "OK") {
                $("#addTeam_error_title").hide();
                $("#addTeam_error").hide();
                $("#addTeam_empty_error").hide();

                $("#team_recorded").show();
                $("#addTeam").DataTable().clear().draw();
                rowCounter = 1;
                $('#addRow').click();
                $("#team").val('');

            } else {
                console.log("ERROR");
                console.log(data['reason']);

                $("#team_recorded").hide();
                $("#addTeam_error_title").show();
                $("#addTeam_empty_error").hide();
                $("#addTeam_error").text(data['reason']);
                $("#addTeam_error").show();

            }
        },
        error: function (data) {
            console.log("AJAX ERROR");
            console.log(data);

            // Delete the team if there was an error and it was created
            $.ajax({
                url: '/coachdata/deleteteam/',
                data: {
                    'team': team
                },
                dataType: 'json',
                success: function (data) {
                    console.log("Team no creado");
                    console.log(data)
                },
                error: function (data) {
                    console.log("Error al eliminar equipo no valido");
                    console.log(data)
                }
            });

        }
    });
});

// Save the evaluation of the players of a team
$('.save_evaluation').on('click', function () {
    var team = $('.ev_team_name:visible').text();
    var players = [];
    var values = [];

    var n_values = $('.ev_team:visible .n_values').text();

    if ($('.ev_player:visible').length * n_values != $('input[type=radio]:checked').length) {
        console.log("error");
        $('#ev_error_ajax').hide();
        $('#ev_error_empty').show();
        return;
    }

    var value;
    $('input[type=radio]:checked').each(function (i, obj) {
        value = $(this).attr('id').substr(-1);
        if (value == 0)
            values[i] = 10;
        else
            values[i] = value;
        console.log($(this).attr('id').substr(-1));
    });

    $('.ev_player:visible').each(function (i, obj) {
        players[i] = $(this).attr('id');
        console.log($(this).attr('id'));
    });

    $.ajax({
        url: '/coachdata/saveteamevaluation/',
        data: {
            'team': team,
            'players': players,
            'values': values
        },
        dataType: 'json',
        success: function (data) {
            if (data['response'] == "OK") {
                console.log(data);

                $('#ev_error_empty').hide();
                $('#ev_error_ajax').hide();
                $("#evTeam_error").hide();
                $("#ev_save_button").hide();
                $('.ev_team:visible').hide();
                $("#ev_saved").show();

            } else {
                console.log("ERROR");
                $('#ev_error_empty').hide();
                $('#ev_error_ajax').show();
                $("#evTeam_error").text(data['reason']);
                $("#evTeam_error").show();

            }
        },
        error: function (data) {
            console.log("AJAX ERROR");
            $('#ev_error_empty').hide();
            $('#ev_error_ajax').show();
            console.log(data)
        }
    });
});

// Assign a team to the current coach
$('#join_team_button').on('click', function () {
    var id = $('#user_id').text();
    var team = $('#team_chosen').text();
    var code = $('#code').val();

    if (code == "" || team == "") {
        $('#join_ok').hide();
        $('#join_error_empty').show();
        $('#join_error_ajax').hide();
        $('#join_error').hide();
        return;
    }

    $.ajax({
        url: '/coachdata/joinnewteam/',
        data: {
            'id': id,
            'team_name': team,
            'team_code': code
        },
        dataType: 'json',
        success: function (data) {
            if (data['response'] == "OK") {
                $('#join_ok').show();
                $('#join_error_empty').hide();
                $('#join_error_ajax').hide();
                $('#join_error').hide();

            } else {
                console.log("ERROR");
                console.log(data['reason']);
                $('#join_ok').hide();
                $('#join_error_empty').hide();
                $('#join_error_ajax').show();
                $('#join_error').text(data['reason']);
                $('#join_error').show();

            }
        },
        error: function (data) {
            console.log("AJAX ERROR");
            $('#join_ok').hide();
            $('#join_error_empty').hide();
            $('#join_error_ajax').show();
            $('#join_error').hide();
        }
    });
});

/****************************************
 *
 *             FUNCTION
 *
 ****************************************/
function finish_effect_form(form_id) {
    if (animating) return false;
    animating = true;

    current_fs = $(form_id).parent();
    next_fs = $(form_id).parent().prev().prev();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //1. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //2. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            //current_fs.css({'transform': 'scale('+scale+')'});
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
            next_fs.show();


            if (is_personal_info) {
                $('#personal_info_content').hide();
                // $('#show_personal_form').hide();
                $('#personal_square').css({'border-color': '#9d9d9d'});
            } else {
                $('#psycho_info_content').hide();
                $('#psycho_square').css({'border-color': '#9d9d9d'});
            }
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
}

String.prototype.hashCode = function () {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
