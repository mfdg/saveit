var rankingState = {

    // UI.js option specs
    menuOptionWidth: 400,
    menuOptionScale: 0.5,

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    compareMeanResults: function (a, b) {
        if (a["pointsMean"] < b["pointsMean"]) {
            return 1;
        } else if (a["pointsMean"] > b["pointsMean"]) {
            return -1;
        } else {
            if ((a["goalsForMean"] - a["goalsAgainstMean"]) < (b["goalsForMean"] - b["goalsAgainstMean"])) {
                return 1;
            } else if ((a["goalsForMean"] - a["goalsAgainstMean"]) > (b["goalsForMean"] - b["goalsAgainstMean"])) {
                return -1;
            } else {
                if (a["goalsForMean"] < b["goalsForMean"]) {
                    return 1;
                } else if (a["goalsForMean"] > b["goalsForMean"]) {
                    return -1;
                }
            }
        }
        return 0;
    },

    /* *****************************************
     * addControl
     *
     * Factory for the easy creation of control
     * symbols.
     * *****************************************/
    drawTables: function (data) {
        var displaceX = 70;

        var dataLen = Object.keys(data['user']).length;
        var topLimit = 5;
        if (dataLen < 5)
            topLimit = dataLen;

        // Team table titles
        var leagueTitle = game.add.text(displaceX, 100, "Top Points",
            {
                font: "24pt RoundsBlack", fill: game.global.colors.RED,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 3
            });
        var gamesPlayedTitle = game.add.text(displaceX + 254, 150, "Pl",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        gamesPlayedTitle.anchor.setTo(0.5);
        gamesPlayedTitle.anchor.x = Math.round(gamesPlayedTitle.width * 0.5) / gamesPlayedTitle.width;
        var gamesWonTitle = game.add.text(displaceX + 283, 150, "W",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        gamesWonTitle.anchor.setTo(0.5);
        gamesWonTitle.anchor.x = Math.round(gamesWonTitle.width * 0.5) / gamesWonTitle.width;
        var gamesDrawnTitle = game.add.text(displaceX + 312, 150, "D",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        gamesDrawnTitle.anchor.setTo(0.5);
        gamesDrawnTitle.anchor.x = Math.round(gamesDrawnTitle.width * 0.5) / gamesDrawnTitle.width;
        var gamesLostTitle = game.add.text(displaceX + 341, 150, "L",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        gamesLostTitle.anchor.setTo(0.5);
        gamesLostTitle.anchor.x = Math.round(gamesLostTitle.width * 0.5) / gamesLostTitle.width;
        var goalsScoredTitle = game.add.text(displaceX + 370, 150, "G.F.",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        goalsScoredTitle.anchor.setTo(0.5);
        goalsScoredTitle.anchor.x = Math.round(goalsScoredTitle.width * 0.5) / goalsScoredTitle.width;
        var goalsAgainstTitle = game.add.text(displaceX + 399, 150, "G.A.",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        goalsAgainstTitle.anchor.setTo(0.5);
        goalsAgainstTitle.anchor.x = Math.round(goalsAgainstTitle.width * 0.5) / goalsAgainstTitle.width;
        var pointsTitle = game.add.text(displaceX + 428, 150, 'Pts.',
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        pointsTitle.anchor.setTo(0.5);
        pointsTitle.anchor.x = Math.round(pointsTitle.width * 0.5) / pointsTitle.width;

        // Render each team
        for (var i = 0; i < topLimit; i++) {
            var currentRow;

            // Alternate color for a neat table
            if (i % 2) {
                currentRow = "darkRow";
            } else {
                currentRow = "lightRow";
            }
            var bg = game.add.image(displaceX, (i * 25) + 157, currentRow);
            var pos = game.add.text(displaceX - 20, (i * 25) + 159, i + 1,
                {
                    font: '16pt PopWarner', fill: game.global.colors.ORANGE,
                    stroke: game.global.colors.DARKGRAY, strokeThickness: 2
                });
            var name = game.add.text(displaceX + 10, (i * 25) + 161, data['user'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            var gamesPlayed = game.add.text(displaceX + 254, (i * 25) + 173, data['won'][i] + data['drawn'][i] + data['lost'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesPlayed.anchor.setTo(0.5);
            gamesPlayed.anchor.x = Math.round(gamesPlayed.width * 0.5) / gamesPlayed.width;
            var gamesWon = game.add.text(displaceX + 283, (i * 25) + 173, data['won'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesWon.anchor.setTo(0.5);
            gamesWon.anchor.x = Math.round(gamesWon.width * 0.5) / gamesWon.width;
            var gamesDrawn = game.add.text(displaceX + 312, (i * 25) + 173, data['drawn'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesDrawn.anchor.setTo(0.5);
            gamesDrawn.anchor.x = Math.round(gamesDrawn.width * 0.5) / gamesDrawn.width;
            var gamesLost = game.add.text(displaceX + 341, (i * 25) + 173, data['lost'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesLost.anchor.setTo(0.5);
            gamesLost.anchor.x = Math.round(gamesLost.width * 0.5) / gamesLost.width;
            var goalsScored = game.add.text(displaceX + 370, (i * 25) + 173, data['goalsFor'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            goalsScored.anchor.setTo(0.5);
            goalsScored.anchor.x = Math.round(goalsScored.width * 0.5) / goalsScored.width;
            var goalsAgainst = game.add.text(displaceX + 399, (i * 25) + 173, data['goalsAgainst'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            goalsAgainst.anchor.setTo(0.5);
            goalsAgainst.anchor.x = Math.round(goalsAgainst.width * 0.5) / goalsAgainst.width;
            var points = game.add.text(displaceX + 428, (i * 25) + 173, data['points'][i],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            points.anchor.setTo(0.5);
            points.anchor.x = Math.round(points.width * 0.5) / points.width;
        }

        // Mean table titles
        var meanTitle = game.add.text(displaceX, 300, "Top per Game",
            {
                font: "24pt RoundsBlack", fill: game.global.colors.RED,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 3
            });
        var meanGamesPlayedTitle = game.add.text(displaceX + 254, 350, "Pl",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        meanGamesPlayedTitle.anchor.setTo(0.5);
        meanGamesPlayedTitle.anchor.x = Math.round(meanGamesPlayedTitle.width * 0.5) / meanGamesPlayedTitle.width;
        var meanGamesWonTitle = game.add.text(displaceX + 283, 350, "W",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        meanGamesWonTitle.anchor.setTo(0.5);
        meanGamesWonTitle.anchor.x = Math.round(meanGamesWonTitle.width * 0.5) / meanGamesWonTitle.width;
        var meanGamesDrawnTitle = game.add.text(displaceX + 312, 350, "D",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        meanGamesDrawnTitle.anchor.setTo(0.5);
        meanGamesDrawnTitle.anchor.x = Math.round(meanGamesDrawnTitle.width * 0.5) / meanGamesDrawnTitle.width;
        var meanGamesLostTitle = game.add.text(displaceX + 341, 350, "L",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        meanGamesLostTitle.anchor.setTo(0.5);
        meanGamesLostTitle.anchor.x = Math.round(meanGamesLostTitle.width * 0.5) / meanGamesLostTitle.width;
        var meanGoalsScoredTitle = game.add.text(displaceX + 370, 350, "G.F.",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        meanGoalsScoredTitle.anchor.setTo(0.5);
        meanGoalsScoredTitle.anchor.x = Math.round(meanGoalsScoredTitle.width * 0.5) / meanGoalsScoredTitle.width;
        var meanGoalsAgainstTitle = game.add.text(displaceX + 399, 350, "G.A.",
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        meanGoalsAgainstTitle.anchor.setTo(0.5);
        meanGoalsAgainstTitle.anchor.x = Math.round(meanGoalsAgainstTitle.width * 0.5) / meanGoalsAgainstTitle.width;
        var meanPointsTitle = game.add.text(displaceX + 428, 350, 'Pts.',
            {
                font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1
            });
        meanPointsTitle.anchor.setTo(0.5);
        meanPointsTitle.anchor.x = Math.round(meanPointsTitle.width * 0.5) / meanPointsTitle.width;

        var meanResAux = [];
        var played = 1;
        for (i = 0; i < dataLen; i++) {
            played = data['won'][i] + data['lost'][i] + data['drawn'][i];
            console.log(played);
            if (played != 0) {
                meanResAux[i] = {
                    "user": data['user'][i],
                    "won": data['won'][i],
                    "lost": data['lost'][i],
                    "drawn": data['drawn'][i],
                    "goalsForMean": data['goalsFor'][i] / played,
                    "goalsAgainstMean": data['goalsAgainst'][i] / played,
                    "pointsMean": data['points'][i] / played
                };
            } else {
                meanResAux[i] = {
                    "user": data['user'][i],
                    "won": 0,
                    "lost": 0,
                    "drawn": 0,
                    "goalsForMean": 0,
                    "goalsAgainstMean": 0,
                    "pointsMean": 0
                };
            }
        }

        meanResAux.sort(this.compareMeanResults);

        for (i = 0; i < topLimit; i++) {
            var meanCurrentRow;

            // Alternate color for a neat table
            if (i % 2) {
                meanCurrentRow = "darkRow";
            } else {
                meanCurrentRow = "lightRow";
            }
            var meanbg = game.add.image(displaceX, (i * 25) + 357, meanCurrentRow);
            var meanpos = game.add.text(displaceX - 20, (i * 25) + 359, i + 1,
                {
                    font: '16pt PopWarner', fill: game.global.colors.ORANGE,
                    stroke: game.global.colors.DARKGRAY, strokeThickness: 2
                });
            var meanName = game.add.text(displaceX + 10, (i * 25) + 361, meanResAux[i]["user"],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            var meanGamesPlayed = game.add.text(displaceX + 254, (i * 25) + 373, meanResAux[i]['won'] + meanResAux[i]['drawn'] + meanResAux[i]['lost'],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            meanGamesPlayed.anchor.setTo(0.5);
            meanGamesPlayed.anchor.x = Math.round(meanGamesPlayed.width * 0.5) / meanGamesPlayed.width;
            var meanGamesWon = game.add.text(displaceX + 283, (i * 25) + 373, meanResAux[i]['won'],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            meanGamesWon.anchor.setTo(0.5);
            meanGamesWon.anchor.x = Math.round(meanGamesWon.width * 0.5) / meanGamesWon.width;
            var meanGamesDrawn = game.add.text(displaceX + 312, (i * 25) + 373, meanResAux[i]['drawn'],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            meanGamesDrawn.anchor.setTo(0.5);
            meanGamesDrawn.anchor.x = Math.round(meanGamesDrawn.width * 0.5) / meanGamesDrawn.width;
            var meanGamesLost = game.add.text(displaceX + 341, (i * 25) + 373, meanResAux[i]['lost'],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            meanGamesLost.anchor.setTo(0.5);
            meanGamesLost.anchor.x = Math.round(meanGamesLost.width * 0.5) / meanGamesLost.width;
            var meanGoalsScored = game.add.text(displaceX + 370, (i * 25) + 373, meanResAux[i]['goalsForMean'],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            meanGoalsScored.anchor.setTo(0.5);
            meanGoalsScored.anchor.x = Math.round(meanGoalsScored.width * 0.5) / meanGoalsScored.width;
            var meanGoalsAgainst = game.add.text(displaceX + 399, (i * 25) + 373, meanResAux[i]['goalsAgainstMean'],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            meanGoalsAgainst.anchor.setTo(0.5);
            meanGoalsAgainst.anchor.x = Math.round(meanGoalsAgainst.width * 0.5) / meanGoalsAgainst.width;
            var meanPoints = game.add.text(displaceX + 428, (i * 25) + 373, meanResAux[i]['pointsMean'],
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            meanPoints.anchor.setTo(0.5);
            meanPoints.anchor.x = Math.round(meanPoints.width * 0.5) / meanPoints.width;
        }

        // Write the legend
        var gamesPlayedLegend = game.add.text(displaceX + 540, 150, "Pl - " + game.global.tableLegend.PLAYED,
            {font: '12pt Arial', fill: "black", strokeThickness: 1});
        gamesPlayedLegend.anchor.setTo(0, 0.5);
        var gamesWonLegend = game.add.text(displaceX + 540, 168, "W - " + game.global.tableLegend.WON,
            {font: '12pt Arial', fill: "black", strokeThickness: 1});
        gamesWonLegend.anchor.setTo(0, 0.5);
        var gamesDrawnLegend = game.add.text(displaceX + 540, 186, "D - " + game.global.tableLegend.DRAWN,
            {font: '12pt Arial', fill: "black", strokeThickness: 1});
        gamesDrawnLegend.anchor.setTo(0, 0.5);
        var gamesLostLegend = game.add.text(displaceX + 540, 204, "L - " + game.global.tableLegend.LOST,
            {font: '12pt Arial', fill: "black", strokeThickness: 1});
        gamesLostLegend.anchor.setTo(0, 0.5);
        var goalsScoredLegend = game.add.text(displaceX + 540, 222, "G.F. - " + game.global.tableLegend.FOR,
            {font: '12pt Arial', fill: "black", strokeThickness: 1});
        goalsScoredLegend.anchor.setTo(0, 0.5);
        var goalsAgainstLegend = game.add.text(displaceX + 540, 240, "G.A. - " + game.global.tableLegend.AGAINST,
            {font: '12pt Arial', fill: "black", strokeThickness: 1});
        goalsAgainstLegend.anchor.setTo(0, 0.5);
        var pointsLegend = game.add.text(displaceX + 540, 258, "Pts. - " + game.global.tableLegend.POINTS,
            {font: '12pt Arial', fill: "black", strokeThickness: 1});
        pointsLegend.anchor.setTo(0, 0.5);

        // Name label
        var text = game.add.text(game.world.centerX - 140, 200, game.global.selectorLabel.HOME,
            {
                font: '32pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 4
            });
        text.anchor.setTo(0.5, 0.5);
        text.anchor.x = Math.round(text.width * 0.5) / text.width;
    },

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set optionCount to 1.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75, 1.75);

        var shadow = game.add.image(290, game.world.centerY + 25, 'shadowBG');
        shadow.anchor.setTo(0.5, 0.5);
        shadow.scale.setTo(0.9, 1.4);

        // Create header using UI.js
        createHeader();

        // Button to exit the game
        var exitButton = game.add.image(game.canvas.width - 60, 9, 'exit');
        exitButton.scale.setTo(0.38);
        exitButton.inputEnabled = true;
        exitButton.input.useHandCursor = true;
        exitButton.events.onInputUp.add(function (target) {
            game.global.lastState = game.global.states.MENU; // Visit menu
            game.state.start('menu');
        }, this);
        exitButton.events.onInputOver.add(function (target) {
            target.scale.setTo(0.42);
        });
        exitButton.events.onInputOut.add(function (target) {
            target.scale.setTo(0.38);
        });

        // Button to exit the game
        var optionsButton = game.add.image(game.canvas.width - 120, 9, 'cog');
        optionsButton.scale.setTo(0.38);
        optionsButton.inputEnabled = true;
        optionsButton.input.useHandCursor = true;
        optionsButton.events.onInputUp.add(function (target) {
            game.state.start('options');
        }, this);
        optionsButton.events.onInputOver.add(function (target) {
            target.scale.setTo(0.42);
        });
        optionsButton.events.onInputOut.add(function (target) {
            target.scale.setTo(0.38);
        });

        // Button to exit the game
        var rankingButton = game.add.image(game.canvas.width - 180, 9, 'ballIcon');
        rankingButton.scale.setTo(0.38);
        rankingButton.inputEnabled = true;
        rankingButton.input.useHandCursor = true;
        rankingButton.events.onInputUp.add(function (target) {
            game.global.lastState = game.global.states.LEAGUE_MENU; // Visit leagueMenu
            game.state.start('leagueMenu');
        }, this);
        rankingButton.events.onInputOver.add(function (target) {
            target.scale.setTo(0.42);
        });
        rankingButton.events.onInputOut.add(function (target) {
            target.scale.setTo(0.38);
        });

        console.log(game.global.user);
        getMatchResults(game.global.user);
    }
};