var menuState = {

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    loginResponse: function (success) {
        if (success) {

            game.teamName = this.nameInput.text; // + ' TEAM';
            game.user = this.nameInput.text;

            game.teamName = game.teamName.toLowerCase();
            game.teamName = game.teamName.replace(/\b\w/g, function ($1) {
                return $1.toUpperCase()
            });

            game.global.lastState = game.global.states.LEAGUE_MENU; // Visit leagueMenu
            game.state.start('leagueMenu');
        } else {
            this.error.alpha = 1;
            tween = game.add.tween(this.error).to({angle: -10}, 80)
                .to({angle: 10}, 160).to({angle: -10}, 160)
                .to({angle: 10}, 160).to({angle: 0}, 80).start();
            // alert("That user does not exist.");
        }
    },

    /* *****************************************
     * addMenuOption
     *
     * Factory for the easy creation of menu
     * options.
     * *****************************************/
    addMenuOption: function (text, callback) {
        var optionStyle = {
            font: '26pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 5
        };
        var txt = game.add.text(100, (this.optionCount * 60) + 195, text, optionStyle);

        txt.x = txt.x + txt.width / 2;
        txt.y = txt.y + txt.height / 2;
        txt.anchor.setTo(0.5);

        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        if (callback != "log") {
            txt.events.onInputUp.add(callback);
        } else {
            txt.events.onInputUp.add(function (target) {
                this.logAction();
            }, this)
        }
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.optionCount++;
        return txt;
    },


    /* *****************************************
     * addKeyboardKey
     *
     * Factory for the easy creation of keys
     * to write the team name.
     * *****************************************/
    addKeyboardKey: function (text) {
        var optionStyle = {
            font: '18pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 4
        };

        var txt;
        if (text == 'space') {
            txt = game.add.text((this.countX * 30) + game.world.centerX - 135, (this.countY * 30) + 330, text, {
                font: '14pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
                stroke: game.global.colors.ORANGE, strokeThickness: 4
            });
            txt.anchor.setTo(0.2, 0.5);
        } else {
            txt = game.add.text((this.countX * 30) + game.world.centerX - 135, (this.countY * 30) + 330, text, optionStyle);
            txt.anchor.setTo(0.5, 0.5);
            txt.anchor.x = Math.round(txt.width * 0.5) / txt.width;
        }

        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(function (target) {
            if (this.nameInput.text.length <= game.global.maxTeamLength) {
                if (text == 'space')
                    this.nameInput.text = this.nameInput.text + ' ';
                else
                    this.nameInput.text = this.nameInput.text + target.text;
            }
        }, this);
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.countX++;
        if (this.countX > 6) {
            this.countX = 0;
            this.countY++;
        }

        this.keys[this.countX + this.countY * 7 - 1] = txt;
    },

    /* *****************************************
     * addNumberKey
     *
     * Factory for the easy creation of keys
     * to write the team name.
     * *****************************************/
    addNumberKey: function (text) {
        var optionStyle = {
            font: '18pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 4
        };

        var txt = game.add.text((this.countNumX * 30) + game.world.centerX + 85, (this.countNumY * 30) + 330, text, optionStyle);
        txt.anchor.setTo(0.5, 0.5);
        txt.anchor.x = Math.round(txt.width * 0.5) / txt.width;
        if (text == '0') {
            txt.x += 30;
        }

        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(function (target) {
            if (this.nameInput.text.length <= game.global.maxTeamLength) {
                if (text == 'space')
                    this.nameInput.text = this.nameInput.text + ' ';
                else
                    this.nameInput.text = this.nameInput.text + target.text;
            }
        }, this);
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.countNumX++;
        if (this.countNumX > 2) {
            this.countNumX = 0;
            this.countNumY++;
        }

        this.numbers[this.countNumX + this.countNumY * 3 - 1] = txt;
    },


    /* *****************************************
     * pauseAction
     *
     * Pauses the game and displays match info
     *
     * Params:
     *    endgame: true if the game ended
     * *****************************************/
    logAction: function () {
        this.logPopUp = true;

        for (var l = 0; l < this.menuButtons.length; l++) {
            this.menuButtons[l].input.enabled = false;
        }

        this.logBG = game.add.image(game.world.centerX + 4, game.world.centerY + 55 + 4, 'logShadowBG');
        this.logBG.anchor.setTo(0.5, 0.5);

        // Name label
        this.nameText = game.add.text(game.world.centerX, 230, game.global.selectorLabel.USER,
            {
                font: '32pt PopWarner', fill: game.global.colors.RED,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 5
            });
        this.nameText.anchor.setTo(0.5, 0.5);
        this.nameText.anchor.x = Math.round(this.nameText.width * 0.5) / this.nameText.width;

        // Input background for team name
        this.inputBG = game.add.image(game.world.centerX - 20, 280, 'inputBG');
        this.inputBG.anchor.setTo(0.5, 0.5);

        // Dummy text where team name will be written
        this.nameInput = game.add.text(game.world.centerX - 20, 283, '',
            {font: '18pt PopWarner', fill: game.global.colors.DARKGRAY});
        this.nameInput.anchor.setTo(0.5, 0.5);

        // Create the keyboard to type the team name
        for (var i = 65; i < 91; i++) {
            this.addKeyboardKey(String.fromCharCode(i));
        }
        this.addKeyboardKey('space');

        for (i = 1; i < 10; i++) {
            this.addNumberKey(i.toString());
        }
        this.addNumberKey(0);

        // Erase button
        this.erase = game.add.image(game.world.centerX + 75,
            280, 'erase');
        this.erase.anchor.setTo(0.5, 0.5);
        this.erase.inputEnabled = true;
        this.erase.input.useHandCursor = true;
        this.erase.events.onInputUp.add(function (target) {
            this.nameInput.text = this.nameInput.text.substring(0, this.nameInput.text.length - 1);
        }, this);
        this.erase.events.onInputOver.add(function (target) {
            target.loadTexture('erase_orange');
        });
        this.erase.events.onInputOut.add(function (target) {
            target.loadTexture('erase');
        });

        // Error marker
        this.error = game.add.image(game.world.centerX + 115,
            280, 'error');
        this.error.anchor.setTo(0.5, 0.5);
        this.error.alpha = 0;

        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        var optionStyle = {
            font: '22pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 5
        };

        // Button to start the game
        this.playButton = game.add.text(game.world.centerX, 470, game.global.optionsLabel.OK, optionStyle);
        this.playButton.anchor.setTo(0.5, 0.5);
        this.playButton.anchor.x = Math.round(this.playButton.width * 0.5) / this.playButton.width;
        this.playButton.inputEnabled = true;
        this.playButton.input.useHandCursor = true;
        this.playButton.events.onInputUp.add(function (target) {
            loginCoachdata(this.nameInput.text);
        }, this);
        this.playButton.events.onInputOver.add(onOver);
        this.playButton.events.onInputOut.add(onOut);
    },

    /* *****************************************
     * pauseExit
     *
     * Resumes the match
     * *****************************************/
    logExit: function () {
        if (this.logPopUp) {
            this.logBG.destroy();
            this.nameText.destroy();
            this.inputBG.destroy();
            this.nameInput.destroy();
            this.erase.destroy();
            this.error.destroy();
            this.playButton.destroy();

            for (var i = 0; i < this.keys.length; i++) {
                this.keys[i].destroy();
                this.countX = 0;
                this.countY = 0;
            }
            for (var j = 0; j < this.numbers.length; j++) {
                this.numbers[j].destroy();
                this.countNumX = 0;
                this.countNumY = 0;
            }

            for (var l = 0; l < this.menuButtons.length; l++) {
                this.menuButtons[l].input.enabled = true;
            }

            this.logPopUp = false;
        }
    },


    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set optionCount to 1.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;
        this.keys = [];
        this.numbers = [];

        this.countX = 0;
        this.countY = 0;
        this.countNumX = 0;
        this.countNumY = 0;

        this.logPopUp = false;
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75, 1.75);
        // Add a background image
        var bgshadow = game.add.image(0, 0, 'bg_shadow');

        this.bgRectangle = new Phaser.Rectangle(game.world.centerX - 170, game.world.centerY + 55 - 160, 341, 320);

        if (game.firstMenu) {
            game.firstMenu = false;
            bgshadow.scale.setTo(2.6);
            tween = game.add.tween(bgshadow.scale).to({x: 1.75, y: 1.75}, 1500).start();
        } else {
            bgshadow.scale.setTo(1.75);
        }

        // Display the name of the game
        var titleImg = game.add.image(3 * game.world.centerX / 5 + 10, game.world.centerY / 2, 'title');
        titleImg.scale.setTo(0.65);
        titleImg.anchor.setTo(0.5, 0.5);

        this.menuButtons = [];
        var menuIndex = 0;


        // Use addMenuOption to create the clickable elements
        this.menuButtons[menuIndex] = this.addMenuOption(game.global.menuLabel.LOG, "log");
        game.input.onDown.add(function (pointer) {
            if (!this.bgRectangle.contains(pointer.x, pointer.y)) {
                this.logExit();
            }
        }, this);
        menuIndex++;

        this.menuButtons[menuIndex] = this.addMenuOption(game.global.menuLabel.INSTRUCTIONS, function (target) {
            game.state.start('instructions');
        });
        menuIndex++;

        this.menuButtons[menuIndex] = this.addMenuOption(game.global.menuLabel.OPTIONS, function (target) {
            game.state.start('options');
        });
        menuIndex++;

        this.menuButtons[menuIndex] = this.addMenuOption(game.global.menuLabel.CREDITS, function (target) {
            game.state.start('credits');
        });
    }

};
