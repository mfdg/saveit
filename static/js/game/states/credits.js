// We create our play state
var creditsState = {
    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * *****************************************/
    preload: function () {
        //this.optionCount = 1;
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function() {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75,1.75);

        // Add a background image
        var bgshadow = game.add.image(0, 0, 'bg_shadow');
        bgshadow.scale.setTo(1.75,1.75);

        // Display the name of the game
        var titleImg = game.add.image(3 * game.world.centerX / 5 + 10, game.world.centerY / 2, 'title');
        titleImg.scale.setTo(0.65);
        titleImg.anchor.setTo(0.5, 0.5);

        // Display the texts where our names and a title will be shown
        var title = game.add.text(game.world.width, 280, game.global.credits.BY,
            { font: '30px Courier', fill: '#ffffff', stroke: game.global.colors.DARKGRAY, strokeThickness: 4 });
        title.anchor.setTo(0, 0.5);
        game.add.tween(title).to({x: 20}, 2000).easing(Phaser.Easing.Bounce.Out).start();

        var mateoLabel = game.add.text(game.world.width + 100, 330, 'Mateo Fdez de Gamboa Santiuste',
            { font: '24px Courier', fill: '#ffffff', stroke: game.global.colors.DARKGRAY, strokeThickness: 4 });
        mateoLabel.anchor.setTo(0.5, 0.5);
        mateoLabel.anchor.x = Math.round(mateoLabel.width * 0.5) / mateoLabel.width;
        game.add.tween(mateoLabel).to({x: game.world.centerX - 150}, 2200).easing(Phaser.Easing.Bounce.Out).start();

        var pinarLabel = game.add.text(game.world.width + 200, 360, 'Pinar Sanz Sacristán',
            { font: '24px Courier', fill: '#ffffff', stroke: game.global.colors.DARKGRAY, strokeThickness: 4 });
        pinarLabel.anchor.setTo(0.5, 0.5);
        pinarLabel.anchor.x = Math.round(pinarLabel.width * 0.5) / pinarLabel.width;
        game.add.tween(pinarLabel).to({x: game.world.centerX - 150}, 2250).easing(Phaser.Easing.Bounce.Out).start();

        // Display the ball
        var ball = game.add.image(game.world.centerX - 170, -100, 'ball_credits');
        ball.anchor.setTo(0.5, 0.5);
        ball.scale.setTo(0.33);
        game.add.tween(ball).to({y: 465}, 2200).easing(Phaser.Easing.Bounce.Out).start();

        // Return to main menu
        var optionStyle = { font: '26pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 4};
        var txt = game.add.text(100, 450, game.global.optionsLabel.BACK, optionStyle);
        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(function (target) {
            game.state.start('menu');
        });
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
    }
};