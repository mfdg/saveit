var formationCount = 0;

var leagueMenuState = {

    menuOptionWidth: 400,
    menuOptionScale: 0.5,
    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * addControl
     *
     * Factory for the easy creation of control
     * symbols.
     * *****************************************/
    addControl: function (text) {
        var img;

        // Default control symbol is selected
        if (text == 'simulate')
            img = text + '_hover';
        else
            img = text;

        var icon = game.add.image((this.controlCount * 60) + 160, game.world.centerY - this.displaceY + 310, img);
        icon.anchor.setTo(0.5, 0.5);
        icon.scale.setTo(0.3, 0.3);
        icon.inputEnabled = true;
        icon.input.useHandCursor = true;

        // Save control when clicking the symbol
        icon.events.onInputUp.add(function (target) {
            for (var i = 0; i < this.controlIcons.length; i++) {
                this.controlIcons[i].loadTexture(game.global.controls[i]);
            }
            target.loadTexture(text + "_hover");
            this.selectedControl = text;
        }, this);
        this.controlIcons.push(icon);

        this.controlCount++;
    },

    /* *****************************************
     * showControls
     *
     * Function that adds the control selector
     * *****************************************/
    showControls: function () {
        this.addControl('simulate');
        this.addControl('controller');
    },

    /* *****************************************
     * addColor
     *
     * Factory for the easy creation of color
     * options.
     * *****************************************/
    addColor: function (text) {
        var icon = game.add.image(130 + (this.colorCount * 52), game.world.centerY - this.displaceY + 250, text + "ball");
        icon.anchor.setTo(0.5, 0.5);

        var onOver = function (target) {
            target.loadTexture(text + "ball_hover");
        };
        var onOut = function (target) {
            target.loadTexture(text + "ball");
        };
        icon.inputEnabled = true;
        icon.input.useHandCursor = true;
        icon.events.onInputUp.add(function (target) {
            this.selectedColor = text;
            this.colorMarker.x = target.x;
            this.formation.loadTexture(game.global.formations[formationCount] + '_' + this.selectedColor);
        }, this);
        icon.events.onInputOver.add(onOver);
        icon.events.onInputOut.add(onOut);

        this.colorBalls.push(icon);
        this.colorCount++;
    },

    /* *****************************************
     * showColors
     *
     * Function that adds the color selector
     * *****************************************/
    showColors: function () {
        for (var i = 0; i < game.global.shirtColors.length; i++) {
            this.addColor(game.global.shirtColors[i]);
        }
    },

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set counters for UI building, storage
     * arrays and default choices.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;
        this.countX = 0;
        this.countY = 0;
        this.colorCount = 0;
        this.controlCount = 0;

        this.colorBalls = new Array();
        this.controlIcons = new Array();
        this.selectedColor = 'red';
        this.selectedAI = 'fox';
        this.selectedControl = 'simulate';

        game.global.playerSpeed = 1;
        game.global.playerResistance = 1;
        game.global.playerStrength = 1;
        game.global.playerDefense = 1;
        game.global.playerTechnique = 1;
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75, 1.75);

        var bg = game.add.image(game.world.centerX, game.world.centerY + 42, 'shadowBG');
        bg.anchor.setTo(0.5, 0.5);
        bg.scale.setTo(1.33, 1.46);

        // Create header using UI.js
        createHeader();

        // Button to exit the game
        var exitButton = game.add.image(game.canvas.width - 60, 9, 'exit');
        exitButton.scale.setTo(0.38);
        exitButton.inputEnabled = true;
        exitButton.input.useHandCursor = true;
        exitButton.events.onInputUp.add(function (target) {
            game.global.lastState = game.global.states.MENU; // Visit menu
            game.state.start('menu');
        }, this);
        exitButton.events.onInputOver.add(function (target) {
            target.scale.setTo(0.42);
        });
        exitButton.events.onInputOut.add(function (target) {
            target.scale.setTo(0.38);
        });

        // Button to exit the game
        var optionsButton = game.add.image(game.canvas.width - 120, 9, 'cog');
        optionsButton.scale.setTo(0.38);
        optionsButton.inputEnabled = true;
        optionsButton.input.useHandCursor = true;
        optionsButton.events.onInputUp.add(function (target) {
            game.state.start('options');
        }, this);
        optionsButton.events.onInputOver.add(function (target) {
            target.scale.setTo(0.42);
        });
        optionsButton.events.onInputOut.add(function (target) {
            target.scale.setTo(0.38);
        });

        // Button to exit the game
        var rankingButton = game.add.image(game.canvas.width - 180, 9, 'trophy');
        rankingButton.scale.setTo(0.38);
        rankingButton.inputEnabled = true;
        rankingButton.input.useHandCursor = true;
        rankingButton.events.onInputUp.add(function (target) {
            game.global.lastState = game.global.states.RANKING; // Visit ranking
            game.state.start('ranking');
        }, this);
        rankingButton.events.onInputOver.add(function (target) {
            target.scale.setTo(0.42);
        });
        rankingButton.events.onInputOut.add(function (target) {
            target.scale.setTo(0.38);
        });

        var markStyle = {
            font: '18pt PopWarner', fill: game.global.colors.WHITE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        };

        // Placement constants
        this.displaceY = 110;
        this.intervalY = 48;

        // Points to distribute
        var behaviourCoef = 0.5;
        if (game.global.evaluated) {
            behaviourCoef = game.global.evaluation;
        }
        this.maxPoints = Math.round(20 * behaviourCoef);

        // Points title
        var pointsText = game.add.text(30, game.world.centerY - this.displaceY - 50, "Points:", {
            font: '22pt PopWarner', fill: game.global.colors.RED,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        });
        pointsText.anchor.setTo(0, 0.5);
        this.pointsAttrib = game.add.text(210, game.world.centerY - this.displaceY - 50, this.maxPoints, {
            font: '18pt PopWarner', fill: game.global.colors.WHITE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        });
        this.pointsAttrib.anchor.setTo(1, 0.5);
        this.remainingPoints = this.maxPoints;
        for (var i = 0; i < 5; i++) {

            // Creation of slide bar and markers
            var bar = game.add.image(game.world.centerX - 80, game.world.centerY - this.displaceY + (this.intervalY * i), 'slide_bar');
            bar.anchor.setTo(0.5, 0.5);
            var selector = game.add.image(game.world.centerX - 80 - bar.width / 2 + 12, game.world.centerY - this.displaceY + (this.intervalY * i), 'slide_select');
            selector.anchor.setTo(0.5, 0.5);
            switch (i) {
                case 0:
                    this.speedSelector = selector;
                    this.speedMarker = 1;
                    this.speedMarkerText = game.add.text(210, game.world.centerY - this.displaceY + (this.intervalY * i), this.speedMarker, markStyle);
                    this.speedMarkerText.anchor.setTo(1, 0.5);
                    this.speedText = game.add.text(30, game.world.centerY - this.displaceY + (this.intervalY * i), game.global.attributes[i], markStyle);
                    this.speedText.anchor.setTo(0, 0.5);
                    break;
                case 1:
                    this.resistanceSelector = selector;
                    this.resistanceMarker = 1;
                    this.resistanceMarkerText = game.add.text(210, game.world.centerY - this.displaceY + (this.intervalY * i), this.resistanceMarker, markStyle);
                    this.resistanceMarkerText.anchor.setTo(1, 0.5);
                    this.resistanceText = game.add.text(30, game.world.centerY - this.displaceY + (this.intervalY * i), game.global.attributes[i], markStyle);
                    this.resistanceText.anchor.setTo(0, 0.5);
                    break;
                case 2:
                    this.strengthSelector = selector;
                    this.strengthMarker = 1;
                    this.strengthMarkerText = game.add.text(210, game.world.centerY - this.displaceY + (this.intervalY * i), this.strengthMarker, markStyle);
                    this.strengthMarkerText.anchor.setTo(1, 0.5);
                    this.strengthText = game.add.text(30, game.world.centerY - this.displaceY + (this.intervalY * i), game.global.attributes[i], markStyle);
                    this.strengthText.anchor.setTo(0, 0.5);
                    break;
                case 3:
                    this.defenseSelector = selector;
                    this.defenseMarker = 1;
                    this.defenseMarkerText = game.add.text(210, game.world.centerY - this.displaceY + (this.intervalY * i), this.defenseMarker, markStyle);
                    this.defenseMarkerText.anchor.setTo(1, 0.5);
                    this.defenseText = game.add.text(30, game.world.centerY - this.displaceY + (this.intervalY * i), game.global.attributes[i], markStyle);
                    this.defenseText.anchor.setTo(0, 0.5);
                    break;
                case 4:
                    this.techniqueSelector = selector;
                    this.techniqueMarker = 1;
                    this.techniqueMarkerText = game.add.text(210, game.world.centerY - this.displaceY + (this.intervalY * i), this.techniqueMarker, markStyle);
                    this.techniqueMarkerText.anchor.setTo(1, 0.5);
                    this.techniqueText = game.add.text(30, game.world.centerY - this.displaceY + (this.intervalY * i), game.global.attributes[i], markStyle);
                    this.techniqueText.anchor.setTo(0, 0.5);
                    break;
                default:
                    break;
            }

            // Creation of +- buttons
            var plus = game.add.image(game.world.centerX + bar.width / 2 - 60, game.world.centerY - this.displaceY - 10 + (this.intervalY * i), 'plus');
            plus.anchor.setTo(0.5, 0.5);
            plus.inputEnabled = true;
            plus.input.useHandCursor = true;
            var minus = game.add.image(game.world.centerX + bar.width / 2 - 60, game.world.centerY - this.displaceY + 10 + (this.intervalY * i), 'minus');
            minus.anchor.setTo(0.5, 0.5);
            minus.inputEnabled = true;
            minus.input.useHandCursor = true;

            // Button input assignment
            switch (i) {
                case 0:
                    plus.events.onInputUp.add(function (target) {
                        if (this.speedMarker < 10 && this.remainingPoints > 0) {
                            game.global.playerSpeed++;
                            this.speedMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.speedSelector.x = this.speedSelector.x + 20;
                            this.speedMarkerText.text = this.speedMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.speedMarker > 1 && this.remainingPoints < this.maxPoints) {
                            game.global.playerSpeed--;
                            this.speedMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.speedSelector.x = this.speedSelector.x - 20;
                            this.speedMarkerText.text = this.speedMarker;
                        }
                    }, this);
                    break;
                case 1:
                    plus.events.onInputUp.add(function (target) {
                        if (this.resistanceMarker < 10 && this.remainingPoints > 0) {
                            game.global.playerResistance++;
                            this.resistanceMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.resistanceSelector.x = this.resistanceSelector.x + 20;
                            this.resistanceMarkerText.text = this.resistanceMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.resistanceMarker > 1 && this.remainingPoints < this.maxPoints) {
                            game.global.playerResistance--;
                            this.resistanceMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.resistanceSelector.x = this.resistanceSelector.x - 20;
                            this.resistanceMarkerText.text = this.resistanceMarker;
                        }
                    }, this);
                    break;
                case 2:
                    plus.events.onInputUp.add(function (target) {
                        if (this.strengthMarker < 10 && this.remainingPoints > 0) {
                            game.global.playerStrength++;
                            this.strengthMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.strengthSelector.x = this.strengthSelector.x + 20;
                            this.strengthMarkerText.text = this.strengthMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.strengthMarker > 1 && this.remainingPoints < this.maxPoints) {
                            game.global.playerStrength--;
                            this.strengthMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.strengthSelector.x = this.strengthSelector.x - 20;
                            this.strengthMarkerText.text = this.strengthMarker;
                        }
                    }, this);
                    break;
                case 3:
                    plus.events.onInputUp.add(function (target) {
                        if (this.defenseMarker < 10 && this.remainingPoints > 0) {
                            game.global.playerDefense++;
                            this.defenseMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.defenseSelector.x = this.defenseSelector.x + 20;
                            this.defenseMarkerText.text = this.defenseMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.defenseMarker > 1 && this.remainingPoints < this.maxPoints) {
                            game.global.playerDefense--;
                            this.defenseMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.defenseSelector.x = this.defenseSelector.x - 20;
                            this.defenseMarkerText.text = this.defenseMarker;
                        }
                    }, this);
                    break;
                case 4:
                    plus.events.onInputUp.add(function (target) {
                        if (this.techniqueMarker < 10 && this.remainingPoints > 0) {
                            game.global.playerTechnique++;
                            this.techniqueMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.techniqueSelector.x = this.techniqueSelector.x + 20;
                            this.techniqueMarkerText.text = this.techniqueMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.techniqueMarker > 1 && this.remainingPoints < this.maxPoints) {
                            game.global.playerTechnique--;
                            this.techniqueMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.techniqueSelector.x = this.techniqueSelector.x - 20;
                            this.techniqueMarkerText.text = this.techniqueMarker;
                        }
                    }, this);
                    break;
                default:
                    break;
            }
        }

        // Field background for the formations
        var field = game.add.image(game.world.centerX + 220, game.world.centerY - this.displaceY + 150, 'field');
        field.anchor.setTo(0.5, 0.5);
        field.angle = 90;
        field.scale.setTo(0.7);

        // Formation overlay selection
        this.formation = game.add.image(game.world.centerX + 220, game.world.centerY - this.displaceY + 150, game.global.formations[formationCount] + '_' + this.selectedColor);
        this.formation.anchor.setTo(0.5, 0.5);
        this.formation.scale.setTo(0.7);

        // Font style for formation name and navigators
        var styleFormation = {
            font: '28pt PopWarner', fill: game.global.colors.ORANGE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        };

        // Formation name label
        this.fieldText = game.add.text(game.world.centerX + 220, game.world.centerY - this.displaceY - 50, game.global.formations[formationCount], styleFormation);
        this.fieldText.anchor.setTo(0.5, 0.5);
        this.fieldText.anchor.x = Math.round(this.fieldText.width * 0.5) / this.fieldText.width;

        // Formation change inputs
        // - Left arrow
        var leftArrow = game.add.text(game.world.centerX + 100, game.world.centerY - this.displaceY - 50, '<', styleFormation);
        leftArrow.anchor.setTo(0, 0.5);
        leftArrow.inputEnabled = true;
        leftArrow.input.useHandCursor = true;
        leftArrow.events.onInputUp.add(function (target) {
            formationCount--;
            if (formationCount < 0) {
                formationCount = game.global.formations.length - 1;
            }
            this.formation.loadTexture(game.global.formations[formationCount] + '_' + this.selectedColor);
            this.fieldText.text = game.global.formations[formationCount];
        }, this);
        // - Right arrow
        var rightArrow = game.add.text(game.world.centerX + 340, game.world.centerY - this.displaceY - 50, '>', styleFormation);
        rightArrow.anchor.setTo(1, 0.5);
        rightArrow.inputEnabled = true;
        rightArrow.input.useHandCursor = true;
        rightArrow.events.onInputUp.add(function (target) {
            formationCount++;
            if (formationCount >= game.global.formations.length) {
                formationCount = 0;
            }
            this.formation.loadTexture(game.global.formations[formationCount] + '_' + this.selectedColor);
            this.fieldText.text = game.global.formations[formationCount];
        }, this);


        // Color label
        var colorText = game.add.text(30, game.world.centerY - this.displaceY + 250, game.global.selectorLabel.COLOR,
            {
                font: '18pt PopWarner', fill: game.global.colors.WHITE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 4
            });
        colorText.anchor.setTo(0, 0.5);

        // Create the marker for current selected color
        this.colorMarker = game.add.image(130, game.world.centerY - this.displaceY + 250, 'selector');
        this.colorMarker.anchor.setTo(0.5, 0.5);

        // Creation of color balls
        this.showColors();

        // Control label
        var playerText = game.add.text(30, game.world.centerY - this.displaceY + 310, game.global.selectorLabel.PLAYER,
            {
                font: '18pt PopWarner', fill: game.global.colors.WHITE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 4
            });
        playerText.anchor.setTo(0, 0.5);
        this.showControls();

        // Button to start the game
        var img = game.global.image.PLAY;
        var playButton = game.add.image(game.world.centerX, game.world.centerY + 230, img);
        playButton.anchor.setTo(0.5, 0.5);
        playButton.scale.setTo(0.4);
        playButton.inputEnabled = true;
        playButton.input.useHandCursor = true;
        playButton.events.onInputUp.add(function (target) {

            // Initialization
            game.global.selectedFormation = formationCount;
            game.teamColor = this.selectedColor;
            game.playerTeam = new Team(null, null, game.teamColor, game.teamName, game.global.selectedFormation,
                null, null);

            game.teamAI = this.selectedAI;
            game.rivalAI = 'fox';
            game.control = this.selectedControl;
            game.state.start('play');
        }, this);
        playButton.events.onInputOver.add(function (target) {
            target.loadTexture(img + "_hover");
        });
        playButton.events.onInputOut.add(function (target) {
            target.loadTexture(img);
        });
    }
};