var bootState = {

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************

    /* *****************************************
     * loadScripts
     *
     * Load the game files needed to run the game.
     * *****************************************/
    loadScripts: function () {
        game.load.script('menu', '../../static/js/game/states/menu.js');
        game.load.script('options', '../../static/js/game/states/options.js');
        game.load.script('instructions', '../../static/js/game/states/instructions.js');
        game.load.script('credits', '../../static/js/game/states/credits.js');
        game.load.script('play', '../../static/js/game/states/play.js');
        game.load.script('playOnline', '../../static/js/game/states/playOnline.js');
        game.load.script('leagueMenu', '../../static/js/game/states/leagueMenu.js');
        game.load.script('ranking', '../../static/js/game/states/ranking.js');

        game.load.script('Player','../../static/js/game/classes/Player.js');
        game.load.script('Footballer','../../static/js/game/classes/Footballer.js');
        game.load.script('Goalkeeper','../../static/js/game/classes/Goalkeeper.js');
        game.load.script('League', '../../static/js/game/classes/League.js');
        game.load.script('Team', '../../static/js/game/classes/Team.js');
        game.load.script('Ball','../../static/js/game/classes/Ball.js');
        game.load.script('Goal', '../../static/js/game/classes/Goal.js');
        game.load.script('Action', '../../static/js/game/classes/Action.js');
        game.load.script('FoxEvaluator', '../../static/js/game/classes/FoxEvaluator.js');
        game.load.script('DonkeyEvaluator', '../../static/js/game/classes/DonkeyEvaluator.js');
        game.load.script('Emoji', '../../static/js/game/classes/Emoji.js');
        game.load.script('AI', '../../static/js/game/auxiliary/AI.js');
        game.load.script('connection', '../../static/js/game/auxiliary/connection.js');
        game.load.script('UI', '../../static/js/game/auxiliary/UI.js');
        game.load.script('nameGen', '../../static/js/game/auxiliary/nameGen.js');
        game.load.script('saveLoad', '../../static/js/game/auxiliary/saveLoad.js');
        game.load.script('gaussianDist', '../../static/js/game/auxiliary/auxiliary.js');

        game.load.script('Client', '../../static/js/game/client.js');
    },

    /* *****************************************
     * loadMusic
     *
     * Load music.
     * *****************************************/
    loadMusic: function () {
        game.load.audio('whistle', ['../../static/sounds/whistle.mp3']);
        game.load.audio('endGameWhistle', ['../../static/sounds/endGameWhistle.mp3']);
        game.load.audio('crowd1', ['../../static/sounds/crowd1.mp3']);
        game.load.audio('rewind', ['../../static/sounds/rewind.mp3']);
        game.load.audio('error', ['../../static/sounds/error.mp3']);

        game.load.audio('menu', ['../../static/music/menu.ogg']);
        game.load.audio('azeroth', ['../../static/music/azeroth.ogg']);
        game.load.audio('starWars', ['../../static/music/starWars.ogg']);
        game.load.audio('nirn', ['../../static/music/nirn.ogg']);
        game.load.audio('harryPotter', ['../../static/music/harryPotter.ogg']);
    },

    /* *****************************************
     * loadImages
     *
     * Load the images necessary for the sprites
     * and backgrounds used.
     * *****************************************/
    loadImages: function () {
        // Load the players
        // - Cocoa
        game.load.spritesheet('cocoa_green', '../../static/images/game/cocoa/cocoa_green.png', 21, 39, 32);
        game.load.spritesheet('cocoa_red', '../../static/images/game/cocoa/cocoa_red.png', 21, 39, 32);
        game.load.spritesheet('cocoa_white', '../../static/images/game/cocoa/cocoa_white.png', 21, 39, 32);
        game.load.spritesheet('cocoa_yellow', '../../static/images/game/cocoa/cocoa_yellow.png', 21, 39, 32);

        // - Coffee
        game.load.spritesheet('coffee_green', '../../static/images/game/coffee/coffee_green.png', 21, 39, 32);
        game.load.spritesheet('coffee_red', '../../static/images/game/coffee/coffee_red.png', 21, 39, 32);
        game.load.spritesheet('coffee_white', '../../static/images/game/coffee/coffee_white.png', 21, 39, 32);
        game.load.spritesheet('coffee_yellow', '../../static/images/game/coffee/coffee_yellow.png', 21, 39, 32);

        // - Milk
        game.load.spritesheet('milk_green', '../../static/images/game/milk/milk_green.png', 21, 39, 32);
        game.load.spritesheet('milk_red', '../../static/images/game/milk/milk_red.png', 21, 39, 32);
        game.load.spritesheet('milk_white', '../../static/images/game/milk/milk_white.png', 21, 39, 32);
        game.load.spritesheet('milk_yellow', '../../static/images/game/milk/milk_yellow.png', 21, 39, 32);

        // - Vanilla
        game.load.spritesheet('vanilla_green', '../../static/images/game/vanilla/vanilla_green.png', 21, 39, 32);
        game.load.spritesheet('vanilla_red', '../../static/images/game/vanilla/vanilla_red.png', 21, 39, 32);
        game.load.spritesheet('vanilla_white', '../../static/images/game/vanilla/vanilla_white.png', 21, 39, 32);
        game.load.spritesheet('vanilla_yellow', '../../static/images/game/vanilla/vanilla_yellow.png', 21, 39, 32);

        // Emojis
        game.load.image('emo_greetings', '../../static/images/game/emojis/greetings.png');
        game.load.image('emo_wellPlayed', '../../static/images/game/emojis/wellPlayed.png');
        game.load.image('emo_wow', '../../static/images/game/emojis/wow.png');
        game.load.image('emo_oops', '../../static/images/game/emojis/oops.png');
        game.load.image('emo_angry', '../../static/images/game/emojis/angry.png');

        // Load the title
        game.load.image('title', '../../static/images/game/title1.png');

        // Load the ui static
        game.load.image('matchBar', '../../static/images/game/matchBar.png');
        game.load.image('menuBar', '../../static/images/game/menuBar.png');

        game.load.image('redball', '../../static/images/game/matchicons/red.png');
        game.load.image('whiteball', '../../static/images/game/matchicons/white.png');
        game.load.image('yellowball', '../../static/images/game/matchicons/yellow.png');
        game.load.image('greenball', '../../static/images/game/matchicons/green.png');

        game.load.image('redball_hover', '../../static/images/game/matchicons/red_hover.png');
        game.load.image('whiteball_hover', '../../static/images/game/matchicons/white_hover.png');
        game.load.image('yellowball_hover', '../../static/images/game/matchicons/yellow_hover.png');
        game.load.image('greenball_hover', '../../static/images/game/matchicons/green_hover.png');

        game.load.image('marker', '../../static/images/game/matchicons/marker.png');
        game.load.image('selector', '../../static/images/game/menubuttons/selector.png');

        game.load.image('arrowkeys', '../../static/images/game/instructions/arrowkeys.png');
        game.load.image('zKey', '../../static/images/game/instructions/Z.png');
        game.load.image('xKey', '../../static/images/game/instructions/X.png');
        game.load.image('cKey', '../../static/images/game/instructions/C.png');
        game.load.image('arrowkeys_gamepad', '../../static/images/game/instructions/arrowkeys_gamepad.png');
        game.load.image('zKey_gamepad', '../../static/images/game/instructions/Z_gamepad.png');
        game.load.image('xKey_gamepad', '../../static/images/game/instructions/X_gamepad.png');
        game.load.image('cKey_gamepad', '../../static/images/game/instructions/C_gamepad.png');

        game.load.image('inputBG', '../../static/images/game/inputBG.png');
        game.load.image('shadowBG', '../../static/images/game/menuCreateBG.png');
        game.load.image('logShadowBG', '../../static/images/game/loginInputBG.png');
        game.load.image('error', '../../static/images/game/error.png');
        game.load.image('erase', '../../static/images/game/erase.png');
        game.load.image('erase_orange', '../../static/images/game/erase_orange.png');


        game.load.image('slide_bar', '../../static/images/game/pointdistribution/bar.png');
        game.load.image('slide_select', '../../static/images/game/pointdistribution/marker.png');
        game.load.image('plus', '../../static/images/game/pointdistribution/plus.png');
        game.load.image('minus', '../../static/images/game/pointdistribution/minus.png');

        game.load.image('lightRow', '../../static/images/game/table/lightRow.png');
        game.load.image('darkRow', '../../static/images/game/table/darkRow.png');

        // Load the ball
        game.load.image('ball', '../../static/images/game/football.png');
        game.load.image('ball_credits', '../../static/images/game/football_credits.png');

        // Load the field
        game.load.image('field', '../../static/images/game/field.jpg');

        // Load stadium background
        game.load.image('stadium', '../../static/images/game/stadium.png');

        // Load walls and goal
        game.load.image('wallV', '../../static/images/game/wallVertical.png');
        game.load.image('wallH', '../../static/images/game/wallHorizontal.png');
        game.load.image('goalV', '../../static/images/game/goalV.png');
        game.load.image('goalHLeft', '../../static/images/game/goalHLeft.png');
        game.load.image('goalHRight', '../../static/images/game/goalHRight.png');

        // Load a new asset that we will use in the menu state
        game.load.image('azeroth', '../../static/images/game/azeroth.jpg');
        game.load.image('starWars', '../../static/images/game/starwars.jpg');
        game.load.image('nirn', '../../static/images/game/nirn.jpg');
        game.load.image('harryPotter', '../../static/images/game/harryPotter.jpg');

        // Load menu options
        game.load.image('menuHome', '../../static/images/game/menubuttons/menuHome.png');
        game.load.image('menuHome_hover', '../../static/images/game/menubuttons/menuHome_hover.png');
        game.load.image('menuLeague', '../../static/images/game/menubuttons/menuLeague.png');
        game.load.image('menuLeague_hover', '../../static/images/game/menubuttons/menuLeague_hover.png');
        game.load.image('menuFriendly', '../../static/images/game/menubuttons/menuFriendly.png');
        game.load.image('menuFriendly_hover', '../../static/images/game/menubuttons/menuFriendly_hover.png');
        game.load.image('menuSave', '../../static/images/game/menubuttons/menuSave.png');
        game.load.image('menuSave_hover', '../../static/images/game/menubuttons/menuSave_hover.png');
        game.load.image('menuExit', '../../static/images/game/menubuttons/menuExit.png');
        game.load.image('menuExit_hover', '../../static/images/game/menubuttons/menuExit_hover.png');
        game.load.image('playButton', '../../static/images/game/menubuttons/playButton.png');
        game.load.image('playButton_hover', '../../static/images/game/menubuttons/playButton_hover.png');
        // Spanish menu options
        game.load.image('menuHome_es', '../../static/images/game/menubuttons/menuHome_es.png');
        game.load.image('menuHome_es_hover', '../../static/images/game/menubuttons/menuHome_es_hover.png');
        game.load.image('menuLeague_es', '../../static/images/game/menubuttons/menuLeague_es.png');
        game.load.image('menuLeague_es_hover', '../../static/images/game/menubuttons/menuLeague_es_hover.png');
        game.load.image('menuFriendly_es', '../../static/images/game/menubuttons/menuFriendly_es.png');
        game.load.image('menuFriendly_es_hover', '../../static/images/game/menubuttons/menuFriendly_es_hover.png');
        game.load.image('menuSave_es', '../../static/images/game/menubuttons/menuSave_es.png');
        game.load.image('menuSave_es_hover', '../../static/images/game/menubuttons/menuSave_es_hover.png');
        game.load.image('menuExit_es', '../../static/images/game/menubuttons/menuExit_es.png');
        game.load.image('menuExit_es_hover', '../../static/images/game/menubuttons/menuExit_es_hover.png');
        game.load.image('playButton_es', '../../static/images/game/menubuttons/playButton_es.png');
        game.load.image('playButton_es_hover', '../../static/images/game/menubuttons/playButton_es_hover.png');

        // Load formation images
        // green
        game.load.image('1-3-2_green', '../../static/images/game/formations/green/1-3-2.png');
        game.load.image('1-4-1_green', '../../static/images/game/formations/green/1-4-1.png');
        game.load.image('2-1-3_green', '../../static/images/game/formations/green/2-1-3.png');
        game.load.image('2-2-2_green', '../../static/images/game/formations/green/2-2-2.png');
        game.load.image('2-3-1_green', '../../static/images/game/formations/green/2-3-1.png');
        game.load.image('3-1-2_green', '../../static/images/game/formations/green/3-1-2.png');
        game.load.image('3-2-1_green', '../../static/images/game/formations/green/3-2-1.png');
        game.load.image('3-3_green', '../../static/images/game/formations/green/3-3.png');
        game.load.image('4-1-1_green', '../../static/images/game/formations/green/4-1-1.png');
        // red
        game.load.image('1-3-2_red', '../../static/images/game/formations/red/1-3-2.png');
        game.load.image('1-4-1_red', '../../static/images/game/formations/red/1-4-1.png');
        game.load.image('2-1-3_red', '../../static/images/game/formations/red/2-1-3.png');
        game.load.image('2-2-2_red', '../../static/images/game/formations/red/2-2-2.png');
        game.load.image('2-3-1_red', '../../static/images/game/formations/red/2-3-1.png');
        game.load.image('3-1-2_red', '../../static/images/game/formations/red/3-1-2.png');
        game.load.image('3-2-1_red', '../../static/images/game/formations/red/3-2-1.png');
        game.load.image('3-3_red', '../../static/images/game/formations/red/3-3.png');
        game.load.image('4-1-1_red', '../../static/images/game/formations/red/4-1-1.png');
        // white
        game.load.image('1-3-2_white', '../../static/images/game/formations/white/1-3-2.png');
        game.load.image('1-4-1_white', '../../static/images/game/formations/white/1-4-1.png');
        game.load.image('2-1-3_white', '../../static/images/game/formations/white/2-1-3.png');
        game.load.image('2-2-2_white', '../../static/images/game/formations/white/2-2-2.png');
        game.load.image('2-3-1_white', '../../static/images/game/formations/white/2-3-1.png');
        game.load.image('3-1-2_white', '../../static/images/game/formations/white/3-1-2.png');
        game.load.image('3-2-1_white', '../../static/images/game/formations/white/3-2-1.png');
        game.load.image('3-3_white', '../../static/images/game/formations/white/3-3.png');
        game.load.image('4-1-1_white', '../../static/images/game/formations/white/4-1-1.png');
        // yellow
        game.load.image('1-3-2_yellow', '../../static/images/game/formations/yellow/1-3-2.png');
        game.load.image('1-4-1_yellow', '../../static/images/game/formations/yellow/1-4-1.png');
        game.load.image('2-1-3_yellow', '../../static/images/game/formations/yellow/2-1-3.png');
        game.load.image('2-2-2_yellow', '../../static/images/game/formations/yellow/2-2-2.png');
        game.load.image('2-3-1_yellow', '../../static/images/game/formations/yellow/2-3-1.png');
        game.load.image('3-1-2_yellow', '../../static/images/game/formations/yellow/3-1-2.png');
        game.load.image('3-2-1_yellow', '../../static/images/game/formations/yellow/3-2-1.png');
        game.load.image('3-3_yellow', '../../static/images/game/formations/yellow/3-3.png');
        game.load.image('4-1-1_yellow', '../../static/images/game/formations/yellow/4-1-1.png');

        // Load play button
        game.load.image('play', '../../static/images/game/menubuttons/play.png');
        game.load.image('pause', '../../static/images/game/menubuttons/pause.png');

        // Header buttons
        game.load.image('exit', '../../static/images/game/icons/exit.png');
        game.load.image('cog', '../../static/images/game/icons/cog.png');
        game.load.image('trophy', '../../static/images/game/icons/trophy.png');
        game.load.image('ballIcon', '../../static/images/game/icons/ball.png');

        // Load AI animal icons
        game.load.image('fox', '../../static/images/game/ai/fox.png');
        game.load.image('donkey', '../../static/images/game/ai/donkey.png');
        game.load.image('fox_hover', '../../static/images/game/ai/fox_hover.png');
        game.load.image('donkey_hover', '../../static/images/game/ai/donkey_hover.png');

        // Load Control icons
        game.load.image('controller', '../../static/images/game/control/controller.png');
        game.load.image('simulate', '../../static/images/game/control/simulate.png');
        game.load.image('controller_hover', '../../static/images/game/control/controller_hover.png');
        game.load.image('simulate_hover', '../../static/images/game/control/simulate_hover.png');
    },

    /* *****************************************
     * addStates
     *
     * Add game states.
     * *****************************************/
    addStates: function () {
        // Add all the remaining states
        game.state.add('menu', menuState);
        game.state.add('options', optionsState);
        game.state.add('instructions', instructionsState);
        game.state.add('credits', creditsState);
        game.state.add('play', playState);
        game.state.add('playOnline', playOnlineState);
        game.state.add('leagueMenu', leagueMenuState);
        game.state.add('ranking', rankingState);
    },


    /* *****************************************
     * addMusic
     *
     * Add music and sounds to the game.
     * *****************************************/
    addMusic: function () {
        // Sounds
        game.global.ringSound = game.add.audio('whistle');
        game.global.endGameSound = game.add.audio('endGameWhistle');
        game.global.crowdSound = game.add.audio('crowd1');
        game.global.saveSound = game.add.audio('rewind');
        game.global.failLoadSound = game.add.audio('error');

        // Music
        game.global.music = {
            'menu' : game.add.audio('menu'),
            'azeroth' : game.add.audio('azeroth'),
            'starWars' : game.add.audio('starWars'),
            'nirn' : game.add.audio('nirn'),
            'harryPotter' : game.add.audio('harryPotter')
        };
        game.global.music['menu'].loop = true;
        game.global.music['azeroth'].loop = true;
        game.global.music['starWars'].loop = true;
        game.global.music['nirn'].loop = true;
        game.global.music['harryPotter'].loop = true;

        // Start menu music
        game.global.currentMusic = game.global.music['menu'];
        game.global.currentMusic.play();
    },

    /* *****************************************
     * setGamepad
     *
     * Check if there is a gamepad connected and
     * set listeners for gamepad support.
     * *****************************************/
    setGamepad: function () {
        game.gamepad = navigator.getGamepads()[0];

        if(game.gamepad)
            game.global.gamepadConnected = true;
        else
            game.global.gamepadConnected = false;

        var onGamepadConnect = function(event) {
            game.gamepad = navigator.getGamepads()[0];
            game.global.gamepadConnected = true;
        };

        var onGamepadDisconnect = function (event) {
            game.gamepad = null;
            game.global.gamepadConnected= false;
        };

        window.addEventListener('gamepadconnected', onGamepadConnect, false);
        window.addEventListener('gamepaddisconnected', onGamepadDisconnect, false);
    },


    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * init
     *
     * Set loading label and progress bar.
     * *****************************************/
    init: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75);
        // Add a background image
        var bgshadow = game.add.image(0, 0, 'bg_shadow');
        bgshadow.scale.setTo(2.6);

        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.world.centerX, 150, 'Loading...',{ font: '30px PopWarner',
            fill: game.global.colors.WHITE });
        loadingLabel.anchor.setTo(0.5, 0.5);
        loadingLabel.anchor.x = Math.round(loadingLabel.width * 0.5) / loadingLabel.width;

        // Display the progress bar
        this.progressBar = game.add.sprite(game.world.centerX, 200, 'progressBar');
        this.progressBar.anchor.setTo(0.5, 0.5);
    },

    /* *****************************************
     * preload
     *
     * Set background color, physics, and call
     * auxiliary functions.
     * *****************************************/
    preload: function () {
        game.stage.disableVisibilityChange = true;
        game.load.setPreloadSprite(this.progressBar);

        // Set some game settings
        game.stage.backgroundColor = game.global.colors.BGBLUE;
        game.physics.startSystem(Phaser.Physics.P2JS);
        game.physics.p2.setImpactEvents(true);
        game.physics.p2.restitution = 0.8;

        this.loadScripts();
        this.loadMusic();
        this.loadImages();
    },

    /* *****************************************
     * create
     *
     * Add states, music and start menu state.
     * *****************************************/
    create: function () {
        this.addStates();
        this.addMusic();

        this.setGamepad();

        game.firstMenu = true;

        // Start the load state
        game.state.start('menu');
    }
};