var optionsState = {

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * addMenuOption
     *
     * Factory for the easy creation of menu
     * options.
     * *****************************************/
    addMenuOption: function (text, callback) {
        var optionStyle = {
            font: '22pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 3
        };
        var txt = game.add.text(100, (this.optionCount * 65) + 200, text, optionStyle);
        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(callback);
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.optionCount++;
    },

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set optionCount to 1.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75, 1.75);
        // Add a background image
        var bgshadow = game.add.image(0, 0, 'bg_shadow');
        bgshadow.scale.setTo(1.75, 1.75);
        // Display the name of the game
        var titleImg = game.add.image(3 * game.world.centerX / 5 + 10, game.world.centerY / 2, 'title');
        titleImg.scale.setTo(0.65);
        titleImg.anchor.setTo(0.5, 0.5);

        // Change language to english
        this.addMenuOption(game.global.optionsLabel.ENGLISH, function (target) {
            // Load variables in new language
            game.global.menuLabel.START = "New game";
            game.global.menuLabel.LOAD = "Load game";
            game.global.menuLabel.LOG = "Log in";
            game.global.menuLabel.INSTRUCTIONS = "Instructions";
            game.global.menuLabel.OPTIONS = "Options";
            game.global.menuLabel.CREDITS = "Credits";

            game.global.optionsLabel.MUTE = "Mute sounds";
            game.global.optionsLabel.NOTMUTE = "Play sounds";
            game.global.optionsLabel.BACK = "Back";
            game.global.optionsLabel.OK = "Accept";

            game.global.instructions.INTRO = "If you choose to manage your players directly,\nthis are the keys to use:";
            game.global.instructions.ARROWKEYS = "During the match you\nwill manage the player\nclosest" +
                " to the ball,\nwhich is marked by a\nblack pin over his head.\n" +
                "Use the arrow keys to\nmove the selected player.";
            game.global.instructions.ZKEY = "You can pass the ball to another\nplayer with the Z key.";
            game.global.instructions.XKEY = "Use the X key to kick the ball\nto goal.";
            game.global.instructions.CKEY = "Press the C key to try to steal\nthe ball from an opponent.";

            game.global.instructions.INTRO_GP = "If you use a gamepad, these are the specific\ncontrols:";
            game.global.instructions.ARROWKEYS_GP = "During the match you\nwill manage the player\nclosest" +
                " to the ball,\nwhich is marked by a\nblack pin over his head.\n" +
                "Use the arrow keys or\nthe joystic to move the\nselected player.";
            game.global.instructions.ZKEY_GP = "You can pass the ball to another\nplayer with the blue botton.";
            game.global.instructions.XKEY_GP = "Use the red button to kick the\nball to goal.";
            game.global.instructions.CKEY_GP = "Press the pink button to try to\nsteal the ball from an opponent.";

            game.global.home.HOME = "Home";

            game.global.selectorLabel.YOURS = "Your AI";
            game.global.selectorLabel.RIVALS = "Rival's AI";
            game.global.selectorLabel.NAME = "Name";
            game.global.selectorLabel.COLOR = "Color";

            game.global.matchLabel.POSSESSION = "Possession";
            game.global.matchLabel.CLICKTORESUME = "Click to return to your game";
            game.global.matchLabel.CLICKTOFINISH = "Click to return to the menu";
            game.global.matchLabel.PAUSE = "Pause";
            game.global.matchLabel.END = "End";
            game.global.matchLabel.HALF = "2nd Half";

            game.global.friendlyMenuText.YOURTEAM = "Your team";
            game.global.friendlyMenuText.RIVALTEAM = "Rival team";

            game.global.endLeagueText.BADEND = "Too bad...";
            game.global.endLeagueText.NEUTRALEND = "Not bad,";
            game.global.endLeagueText.GOODEND = "Congratulations!";
            game.global.endLeagueText.FINISH = "You ended ";
            game.global.endLeagueText.NEWLEAGUE = "Choose a new league to continue,\nor back to return.";

            game.global.leagueMenuText.NEXTMATCH = "Next match:";

            game.global.tableLegend.PLAYED = "Games played";
            game.global.tableLegend.WON = "Games won";
            game.global.tableLegend.DRAWN = "Games drawn";
            game.global.tableLegend.LOST = "Games lost";
            game.global.tableLegend.FOR = "Goals for";
            game.global.tableLegend.AGAINST = "Goals against";
            game.global.tableLegend.POINTS = "Points";

            game.global.credits.BY = "Developed and designed by:";

            game.global.image.PAUSE = "pause";
            game.global.image.END = "end";
            game.global.image.HOME = "menuHome";
            game.global.image.LEAGUE = "menuLeague";
            game.global.image.FRIENDLY = "menuFriendly";
            game.global.image.SAVE = "menuSave";
            game.global.image.EXIT = "menuExit";

            game.global.attributes[0] = "Speed";
            game.global.attributes[1] = "Resistance";
            game.global.attributes[2] = "Strength";
            game.global.attributes[3] = "Defense";
            game.global.attributes[4] = "Technique";

            game.state.start('menu');
        });
        // Change language to spanish
        this.addMenuOption(game.global.optionsLabel.SPANISH, function (target) {
            // Load variables in new language
            game.global.menuLabel.START = "Nueva partida";
            game.global.menuLabel.LOAD = "Cargar partida";
            game.global.menuLabel.LOG = "Iniciar sesión";
            game.global.menuLabel.INSTRUCTIONS = "Instrucciones";
            game.global.menuLabel.OPTIONS = "Opciones";
            game.global.menuLabel.CREDITS = "Créditos";

            game.global.optionsLabel.MUTE = "Silenciar";
            game.global.optionsLabel.NOTMUTE = "Activar sonidos";
            game.global.optionsLabel.BACK = "Atrás";
            game.global.optionsLabel.OK = "Aceptar";

            game.global.instructions.INTRO = "Si eliges manejar directamente a tus jugadores,\nestas son las teclas a usar:";
            game.global.instructions.ARROWKEYS = "Durante el partido mane-\njarás el jugador más\ncercano a la" +
                " pelota, el\ncual estará señalado por\nun marcador negro sobre\nsu cabeza. " +
                "Usa las fle-\nchas para mover al ju-\ngador seleccionado.";
            game.global.instructions.ZKEY = "Puedes pasar el balón a otro\njugador con la tecla Z.";
            game.global.instructions.XKEY = "Usa la tecla X para chutar el\nbalón a portería.";
            game.global.instructions.CKEY = "Presiona la tecla C para intentar\nrobarle el balón al oponente.";

            game.global.instructions.INTRO_GP = "Si utilizas un controlador, estos son los controles\nespecíficos:";
            game.global.instructions.ARROWKEYS_GP = "Durante el partido mane-\njarás el jugador más\ncercano a la" +
                " pelota, el\ncual estará señalado por\nun marcador negro sobre\nsu cabeza. " +
                "Usa las fle-\nchas o el joystick pa-\nra mover al jugador\nseleccionado.";
            game.global.instructions.ZKEY_GP = "Puedes pasar el balón a otro\njugador con el botón azul.";
            game.global.instructions.XKEY_GP = "Usa el botón rojo para chutar\nel balón a portería.";
            game.global.instructions.CKEY_GP = "Presiona el botón rosa para inten-\ntar robarle el balón al oponente.";

            game.global.home.HOME = "Inicio";

            game.global.selectorLabel.YOURS = "IA";
            game.global.selectorLabel.RIVALS = "IA";
            game.global.selectorLabel.NAME = "Nombre";
            game.global.selectorLabel.COLOR = "Color";

            game.global.matchLabel.POSSESSION = "Posesión";
            game.global.matchLabel.CLICKTORESUME = "Click para volver al juego";
            game.global.matchLabel.CLICKTOFINISH = "Click para volver al menú";
            game.global.matchLabel.PAUSE = "Pausa";
            game.global.matchLabel.END = "Fin";
            game.global.matchLabel.HALF = "2a Parte";

            game.global.friendlyMenuText.YOURTEAM = "Tu equipo";
            game.global.friendlyMenuText.RIVALTEAM = "Equipo rival";

            game.global.endLeagueText.BADEND = "Qué pena...";
            game.global.endLeagueText.NEUTRALEND = "No está mal,";
            game.global.endLeagueText.GOODEND = "¡Enhorabuena!";
            game.global.endLeagueText.FINISH = "Has terminado ";
            game.global.endLeagueText.NEWLEAGUE = "Elige una liga nueva para continuar,\no atrás para volver.";

            game.global.leagueMenuText.NEXTMATCH = "Próximo\npartido:";

            game.global.tableLegend.PLAYED = "Partidos jugados";
            game.global.tableLegend.WON = "Partidos ganados";
            game.global.tableLegend.DRAWN = "Partidos empatados";
            game.global.tableLegend.LOST = "Partidos perdidos";
            game.global.tableLegend.FOR = "Goles a favor";
            game.global.tableLegend.AGAINST = "Goles en contra";
            game.global.tableLegend.POINTS = "Puntos";

            game.global.credits.BY = "Desarrollado y diseñado por:";

            game.global.image.PAUSE = "es_pause";
            game.global.image.END = "es_end";
            game.global.image.HOME = "menuHome_es";
            game.global.image.LEAGUE = "menuLeague_es";
            game.global.image.FRIENDLY = "menuFriendly_es";
            game.global.image.SAVE = "menuSave_es";
            game.global.image.EXIT = "menuExit_es";

            game.global.attributes[0] = "Velocidad";
            game.global.attributes[1] = "Resistencia";
            game.global.attributes[2] = "Fuerza";
            game.global.attributes[3] = "Defensa";
            game.global.attributes[4] = "Técnica";

            game.state.start('menu');
        });
        // Toggle sounds
        this.addMenuOption(game.global.ringSound.volume ? game.global.optionsLabel.MUTE : game.global.optionsLabel.NOTMUTE,
            function (target) {

                game.global.currentMusic.volume = !game.global.currentMusic.volume;

                game.global.ringSound.volume = !game.global.ringSound.volume;
                game.global.endGameSound.volume = !game.global.endGameSound.volume;
                game.global.crowdSound.volume = !game.global.crowdSound.volume;
                game.global.saveSound.volume = !game.global.saveSound.volume;
                game.global.failLoadSound.volume = !game.global.failLoadSound.volume;

                target.text = game.global.ringSound.volume ? game.global.optionsLabel.MUTE : game.global.optionsLabel.NOTMUTE;
            });
        this.addMenuOption(game.global.optionsLabel.BACK, function (target) {
            if (game.global.lastState == game.global.states.MENU)
                game.state.start('menu');
            else if (game.global.lastState == game.global.states.RANKING)
                game.state.start('ranking');
            else
                game.state.start('leagueMenu');
        });
    }
};