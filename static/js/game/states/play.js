// We create our play state
var playState = {

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set some parameters and add sound
     * *****************************************/
    preload: function () {
        this.iniTime = game.time.time;
        this.time = 0;
        this.seconds = 0;
        this.minutes = 0;
        this.possHome = 0;
        this.possVisitor = 0;

        this.counter = 0;

        this.endGame = false;

        this.secondPart = false;
        this.hasRinged = false;
        this.hasStarted = false;

        this.teamAttributes = [game.global.playerSpeed, game.global.playerResistance, game.global.playerStrength, game.global.playerDefense, game.global.playerTechnique];
        var rivalSpeed = Math.random() + 0.7;
        this.rivalAttributes = [randomBetween(5, 10), randomBetween(5, 10), randomBetween(5, 10), randomBetween(5, 10), randomBetween(5, 10)];

        var v = 1 / 6 * game.global.playerSpeed + 1 / 3;
        //this.rivalAttributes = [1, 1, 1, 1, 1];

        // Add sounds
        this.ringSound = game.global.ringSound;
        this.endGameSound = game.global.endGameSound;
        this.crowdSound = game.global.crowdSound;
    },

    /* *****************************************
     * create
     *
     * Create the necessary elements for the
     * game.
     * *****************************************/
    create: function () {
        this.stadium = game.add.sprite(game.world.centerX, 55, 'stadium');
        this.stadium.anchor.setTo(0.5, 0);

        this.background = game.add.sprite(game.world.centerX, game.world.centerY + 25, 'field');
        this.background.anchor.setTo(0.5, 0.5);
        this.background.scale.setTo(1.5, 1.5);

        // UI assets
        var matchBar = game.add.sprite(0, 0, 'matchBar');
        matchBar.scale.setTo(1, 0.33);

        // Button to pause the game
        var pauseButton = game.add.image(game.canvas.width - 57, 7, 'pause');
        pauseButton.scale.setTo(0.5);
        pauseButton.inputEnabled = true;
        pauseButton.input.useHandCursor = true;
        pauseButton.events.onInputUp.add(function (target) {
            this.pauseAction(false);
        }, this);
        game.input.onDown.add(function (event) {
            this.pauseExit();
        }, this);

        // Create announcement for the start of the second half
        this.announcement = game.add.text(game.world.centerX, game.world.height - 80, game.global.matchLabel.HALF,
            {font: '60px PopWarner', fill: "white", stroke: game.global.colors.DARKGRAY, strokeThickness: 4});
        this.announcement.anchor.setTo(0.5, 0);
        this.announcement.anchor.x = Math.round(this.announcement.width * 0.5) / this.announcement.width;
        this.announcement.alpha = 0;

        // Collision groups - movable and unmovable
        game.movableObj = game.physics.p2.createCollisionGroup();
        game.unmovObj = game.physics.p2.createCollisionGroup();

        // Queue to render in order
        this.renderQueue = game.add.group();
        this.renderQueue.enableBody = true;
        this.renderQueue.physicsBodyType = Phaser.Physics.P2JS;

        // Create environment
        this.createWorld();

        // Set the keys to play with
        game.cursor = game.input.keyboard.addKeys({
            'up': Phaser.KeyCode.UP, 'down': Phaser.KeyCode.DOWN,
            'left': Phaser.KeyCode.LEFT, 'right': Phaser.KeyCode.RIGHT,
            'Z': Phaser.KeyCode.Z, 'X': Phaser.KeyCode.X, 'C': Phaser.KeyCode.C
        });

        // Ball
        var ball = new Ball(game, game.world.centerX, game.world.centerY + 20);
        this.ball = this.renderQueue.add(ball);
        this.ball.body.setCollisionGroup = game.movableObj;
        this.ball.body.collides = [game.movableObj, game.unmovObj];

        // Players and the ActionEvaluator that will be used by them
        this.evaluator = new FoxEvaluator(this);
        this.dumbevaluator = new DonkeyEvaluator(this);

        // Teams - home and visitor
        game.playerTeam.setFormation(game.global.formations[game.global.selectedFormation]);
        this.homeTeam = game.playerTeam;
        this.homeTeam.setGoals(this.goalL, this.goalR);
        // this.homeTeam = new Team(this.goalR, this.goalL, game.teamColor, game.teamName,
        //     game.global.formations[game.global.selectedFormation], null, null);
        this.homeTeam.score = 0;

        if (game.teamColor != 'red') {
            this.visitorTeam = new Team(this.goalR, this.goalL, "red", createName(),
                game.global.formations[game.global.rivalSelectedFormation], null, null);
        } else {
            this.visitorTeam = new Team(this.goalR, this.goalL, "yellow", createName(),
                game.global.formations[game.global.rivalSelectedFormation], null, null);
        }
        this.visitorTeam.score = 0;


        // Labels
        // - score
        this.scoreLabel = game.add.text(game.world.centerX, 30, '0 - 0',
            {font: '26px PopWarner', fill: game.global.colors.ORANGE});
        this.scoreLabel.anchor.setTo(0.5, 0.5);
        this.scoreLabel.anchor.x = Math.round(this.scoreLabel.width * 0.5) / this.scoreLabel.width;


        if (this.homeTeam.name.length < game.global.scoreStringLength) {
            this.homeTeamScore = game.add.text(game.world.centerX - (this.scoreLabel.width / 2) - 10, 30, this.homeTeam.name,
                {font: '26px PopWarner', fill: game.global.colors.ORANGE});
        } else if (this.homeTeam.name.length < game.global.scoreStringLengthMid) {
            this.homeTeamScore = game.add.text(game.world.centerX - (this.scoreLabel.width / 2) - 10, 30, this.homeTeam.name,
                {font: '22px PopWarner', fill: game.global.colors.ORANGE});
        } else {
            this.homeTeamScore = game.add.text(game.world.centerX - (this.scoreLabel.width / 2) - 10, 31, this.homeTeam.name,
                {font: '18px PopWarner', fill: game.global.colors.ORANGE});
        }
        this.homeTeamScore.anchor.setTo(1, 0.5);
        var homeBall = game.add.sprite(game.world.centerX - (this.scoreLabel.width / 2) - this.homeTeamScore.width - 30,
            28, this.homeTeam.color + 'ball');
        homeBall.anchor.setTo(0.5, 0.5);

        if (this.visitorTeam.name.length < game.global.scoreStringLength) {
            this.visitorTeamScore = game.add.text(game.world.centerX + (this.scoreLabel.width / 2) + 10, 30, this.visitorTeam.name,
                {font: '26px PopWarner', fill: game.global.colors.ORANGE});
        } else if (this.visitorTeam.name.length < game.global.scoreStringLengthMid) {
            this.visitorTeamScore = game.add.text(game.world.centerX + (this.scoreLabel.width / 2) + 10, 30, this.visitorTeam.name,
                {font: '20px PopWarner', fill: game.global.colors.ORANGE});
        } else {
            this.visitorTeamScore = game.add.text(game.world.centerX + (this.scoreLabel.width / 2) + 10, 31, this.visitorTeam.name,
                {font: '18px PopWarner', fill: game.global.colors.ORANGE});
        }
        this.visitorTeamScore.anchor.setTo(0, 0.5);
        var visitorBall = game.add.sprite(game.world.centerX + (this.scoreLabel.width / 2) + this.visitorTeamScore.width + 30,
            28, this.visitorTeam.color + 'ball');
        visitorBall.anchor.setTo(0.5, 0.5);

        // - clock
        this.timer = game.add.text(50, 30, '00:00', {font: '26px PopWarner', fill: game.global.colors.ORANGE});
        this.timer.anchor.setTo(0.5, 0.5);
        this.timer.anchor.x = Math.round(this.timer.width * 0.5) / this.timer.width;

        // Players
        // - team 1
        switch (game.teamAI) {
            case 'fox':
                this.homeTeam.buildTeam(this.evaluator, this.ball, this.teamAttributes, this.renderQueue);
                break;
            case 'donkey':
                this.homeTeam.buildTeam(this.dumbevaluator, this.ball, this.teamAttributes, this.renderQueue);
                break;
            default:
                this.homeTeam.buildTeam(this.evaluator, this.ball, this.teamAttributes, this.renderQueue);
                break;
        }
        // - team 2
        switch (game.rivalAI) {
            case 'fox':
                this.visitorTeam.buildTeam(this.evaluator, this.ball, this.rivalAttributes, this.renderQueue);
                break;
            case 'donkey':
                this.visitorTeam.buildTeam(this.dumbevaluator, this.ball, this.rivalAttributes, this.renderQueue);
                break;
            default:
                this.visitorTeam.buildTeam(this.evaluator, this.ball, this.rivalAttributes, this.renderQueue);
                break;
        }


        // Add the marker to see the player with the ball
        this.marker = game.add.sprite(0, 0, 'marker');
        this.marker.anchor.setTo(0.5, 0.5);
        this.marker.visible = false;

        // If handling the team directly
        if (game.control == 'controller') {
            // Set player
            this.player = game.playerTeam.getPlayerClosestToBall(this);
            this.player.isPlayer = true;
            this.marker.visible = true;
        }

        // Sort the queue used for rendering
        this.renderQueue.sort();
    },

    /* *****************************************
     * update
     *
     * Checks and functions to be called periodi-
     * cally while the game is running.
     * *****************************************/
    update: function () {

        if (!this.endGame) {
            // Organize the rendering order
            this.renderQueue.sort('y', Phaser.Group.SORT_ASCENDING);

            var playedTime = Math.floor(game.time.time - this.iniTime - game.global.delayStart);

            // Check for gamepad pause
            if (game.global.gamepadConnected &&
                (game.gamepad.buttons[0].pressed || game.gamepad.buttons[9].pressed)) {
                if (!this.pauseMenuActive) {
                    this.pauseAction(false);
                } else {
                    this.pauseExit();
                }
            }

            // Start the play
            if (!this.hasStarted) {
                this.startMatch();
                this.hasStarted = true;
            }

            // Finish the first part of the match
            // Player stop playing and exit the field
            if (playedTime > (game.global.duration / 2) &&
                playedTime <= (game.global.duration / 2) + game.global.delay) {
                if (this.hasRinged) {
                    this.updateEndMatch();
                } else {
                    this.updateTimer();
                    this.endMatch();
                    this.hasRinged = true;
                }
            }
            // Start the second part of the match
            else if (playedTime > (game.global.duration / 2) + game.global.delay) {
                if (this.secondPart) {
                    // Match ends
                    this.hasRinged = false;
                    this.hasStarted = false;
                    this.secondPart = false;
                    sendMatchResults(this.homeTeam.score, this.visitorTeam.score);
                    this.pauseAction(true);
                } else {
                    this.halfTime();
                    this.secondPart = true;
                    this.hasRinged = false;
                    this.hasStarted = false;
                }
            } else if (playedTime > 0) {
                this.match();
            }
        } else {
            this.updateEndMatch();
        }
    },

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * match
     *
     * Handle the actions of a match
     * *****************************************/
    match: function () {
        // Update timer
        if (!game.paused) {
            this.updateTimer();
        }

        // Destroy announcement
        if (this.announcement.alpha > 0) {
            this.announcement.alpha -= 0.05;
        }

        /***************************************************************************************************************
         *
         * INPUT PLAYING
         *
         ***************************************************************************************************************/

        // If handling the team directly
        if (game.control == 'controller') {
            // Pass
            if (game.cursor.Z.isDown ||
                (game.global.gamepadConnected && game.gamepad.buttons[2].pressed)) {
                if (this.ball.owner == this.player) {
                    this.ball.kick(200 * this.player.strength, this.player.direction.x, this.player.direction.y);
                }
            }

            // Swipe
            if (game.cursor.C.isDown ||
                (game.global.gamepadConnected && game.gamepad.buttons[3].pressed)) {
                if (this.ball.owner != this.player
                    && Phaser.Math.distance(this.player.body.x, this.player.body.y, this.ball.x, this.ball.y) < 20) {
                    this.ball.push(this.player.direction.x, this.player.direction.y);
                }
            }

            // Shoot
            if (game.cursor.X.isDown ||
                (game.global.gamepadConnected && game.gamepad.buttons[1].pressed)) {
                if (this.ball.owner == this.player) {
                    this.ball.kick(300 * this.player.strength, this.player.direction.x, this.player.direction.y);
                }
            }

            if (this.counter > 15) {
                this.counter = 0;
                // Unset current player
                this.player.isPlayer = false;
                // Set player
                this.player = game.playerTeam.getPlayerClosestToBall(this);
                this.player.isPlayer = true;
            } else {
                this.counter++;
            }

            // Place marker on top of controlled player
            this.marker.x = this.player.x;
            this.marker.y = this.player.y - 50;

            if (this.marker.visible == false) {
                this.marker.visible = true;
            }
        }
        /***************************************************************************************************************/

        // Set ball owner
        this.ball.updateOwner(this.homeTeam.players.concat(this.visitorTeam.players));

        // Update player position
        this.homeTeam.updatePlayers();
        this.visitorTeam.updatePlayers();

        // Check goal
        if (this.homeTeam.goal.isGoal(this.ball)) {
            this.ball.body.velocity.x = 0;
            this.ball.body.velocity.y = 0;
            this.goalScored(this.visitorTeam);
        } else if (this.visitorTeam.goal.isGoal(this.ball)) {
            this.ball.body.velocity.x = 0;
            this.ball.body.velocity.y = 0;
            this.goalScored(this.homeTeam);
        }
    },

    /* *****************************************
     * startMatch
     *
     * Plays the start whistle.
     * *****************************************/
    startMatch: function () {
        this.ringSound.play();
    },

    /* *****************************************
     * halfTime
     *
     * Change the sides of each team and returns
     * the players to their initial positions.
     * *****************************************/
    halfTime: function () {
        this.ball.body.velocity.x = 0;
        this.ball.body.velocity.y = 0;
        this.homeTeam.switchSides();
        this.visitorTeam.switchSides();
        this.goalScored(null);
        this.iniTime = Math.floor(game.time.time);
        this.minutes = Math.floor((game.time.time - this.iniTime) / 60000) % 60;
        this.seconds = Math.floor((game.time.time - this.iniTime) / 1000) % 60;
        this.announcement.alpha = 1;

        this.homeTeam.affectResistanceToPlayers();
        this.visitorTeam.affectResistanceToPlayers();
    },

    /* *****************************************
     * endMatch
     *
     * Plays the finish whistle.
     * *****************************************/
    endMatch: function () {
        this.marker.visible = false;
        this.endGameSound.play();
    },

    /* *****************************************
     * updateEndMatch
     *
     * Makes the players move to exit the field.
     * *****************************************/
    updateEndMatch: function () {
        // Update player position
        this.homeTeam.updatePlayersExit();
        this.visitorTeam.updatePlayersExit();
    },

    /* *****************************************
     * createWorld
     *
     * Create the walls and goals.
     * *****************************************/
    createWorld: function () {
        // Create our wall group with P2 physics
        this.walls = game.add.group();
        this.walls.enableBody = true;
        this.walls.physicsBodyType = Phaser.Physics.P2JS;

        // Create the 4 walls
        var wall1 = game.add.sprite(game.world.centerX - 380, game.world.centerY + 20, null, 0, this.walls); // Left
        wall1.body.setRectangle(20, 520, 0, 0);
        var wall2 = game.add.sprite(game.world.centerX + 380, game.world.centerY + 20, null, 0, this.walls); // Right
        wall2.body.setRectangle(20, 520, 0, 0);
        var wall3 = game.add.sprite(game.world.centerX, game.world.centerY - 235, null, 0, this.walls); // Top
        wall3.body.setRectangle(780, 20, 0, 0);
        var wall4 = game.add.sprite(game.world.centerX, game.world.centerY + 285, null, 0, this.walls); // Bottom
        wall4.body.setRectangle(780, 20, 0, 0);

        // Set all the walls and goals to be immovable
        this.walls.setAll('body.kinematic', true);
        this.walls.setAll('body.setCollisionGroup', game.unmovObj);
        this.walls.setAll('body.collides', [game.movableObj, game.unmovObj]);

        // Create our goal group with P2 physics
        this.goals = game.add.group();
        this.goals.enableBody = true;
        this.goals.physicsBodyType = Phaser.Physics.P2JS;

        // Create the goals
        game.add.sprite(game.world.centerX - 366, game.world.centerY - 53, 'goalV');
        game.add.sprite(game.world.centerX - 370, game.world.centerY - 51, 'goalHLeft', 0, this.goals);
        game.add.sprite(game.world.centerX - 370, game.world.centerY + 102, 'goalHLeft', 0, this.goals);

        game.add.sprite(game.world.centerX + 362, game.world.centerY - 53, 'goalV');
        game.add.sprite(game.world.centerX + 370, game.world.centerY - 51, 'goalHRight', 0, this.goals);
        game.add.sprite(game.world.centerX + 370, game.world.centerY + 102, 'goalHRight', 0, this.goals);

        // Set all the goals to be immovable
        this.goals.setAll('body.kinematic', true);
        this.goals.setAll('body.setCollisionGroup', game.unmovObj);
        this.goals.setAll('body.collides', [game.movableObj, game.unmovObj]);

        // Create the goalzones
        this.goalL = new Goal(game, game.global.side.LEFT);
        this.goalR = new Goal(game, game.global.side.RIGHT);
    },

    /* *****************************************
     * goalScored
     *
     * Tasks to do after a goal is scored.
     *
     * Params:
     *    teamScored: the team that has scored
     * *****************************************/
    goalScored: function (teamScored) {
        this.homeTeam.returnPlayers(0);
        this.visitorTeam.returnPlayers(0);
        this.ball.returnToIniPosition(0);
        if (teamScored == this.homeTeam) {
            this.crowdSound.play();
            this.homeTeam.score++;
        } else if (teamScored == this.visitorTeam) {
            this.crowdSound.play();
            this.visitorTeam.score++;
        }
        this.scoreLabel.text = this.homeTeam.score + ' - ' + this.visitorTeam.score;
    },

    /* *****************************************
     * updateTimer
     *
     * Update the clock
     * *****************************************/
    updateTimer: function () {
        this.time = game.time.time - this.iniTime - game.global.delayStart;
        this.minutes = Math.floor(this.time / 60000) % 60;
        this.seconds = Math.floor(this.time / 1000) % 60;
        //If any of the digits becomes a single digit number, pad it with a zero
        if (this.seconds < 10)
            this.seconds = '0' + this.seconds;
        if (this.minutes < 10)
            this.minutes = '0' + this.minutes;
        this.timer.setText(this.minutes + ':' + this.seconds);

        if (this.homeTeam.hasBall) {
            this.possHome++;
        } else if (this.visitorTeam.hasBall) {
            this.possVisitor++;
        }
    },

    /* *****************************************
     * pauseAction
     *
     * Pauses the game and displays match info
     *
     * Params:
     *    endgame: true if the game ended
     * *****************************************/
    pauseAction: function (endgame) {
        this.pauseTime = game.time.time;

        this.pauseMenuActive = true;

        var title = game.global.matchLabel.PAUSE, pausePrompt = game.global.matchLabel.CLICKTORESUME;
        if (endgame) {
            this.endGame = true;
            title = game.global.matchLabel.END;
            pausePrompt = game.global.matchLabel.CLICKTOFINISH;
        } else {
            game.paused = true;
        }

        this.pauseMenu = game.add.sprite(game.world.centerX, game.world.centerY, 'shadowBG');
        this.pauseMenu.scale.setTo(0.72, 1.25);
        this.pauseMenu.anchor.setTo(0.5);

        this.titlePause = game.add.text(game.world.centerX, game.world.centerY - 150, title,
            {font: '70px PopWarner', fill: game.global.colors.ORANGE});
        this.titlePause.anchor.setTo(0.5, 0.5);
        this.titlePause.anchor.x = Math.round(this.titlePause.width * 0.5) / this.titlePause.width;
        this.titlePause.anchor.y = Math.round(this.titlePause.height * 0.5) / this.titlePause.height;

        this.scorePause = game.add.text(game.world.centerX, game.world.centerY - 80, this.homeTeam.score + ' - ' +
            this.visitorTeam.score,
            {font: '32px PopWarner', fill: game.global.colors.LIGHTBLUE});
        this.scorePause.anchor.setTo(0.5, 0.5);
        this.scorePause.anchor.x = Math.round(this.scorePause.width * 0.5) / this.scorePause.width;

        this.homePause = game.add.sprite(game.world.centerX - (this.scorePause.width / 2) - 20, game.world.centerY - 83,
            this.homeTeam.color + 'ball');
        this.homePause.anchor.setTo(0.5, 0.5);
        this.visitorPause = game.add.sprite(game.world.centerX + (this.scorePause.width / 2) + 20, game.world.centerY - 83,
            this.visitorTeam.color + 'ball');
        this.visitorPause.anchor.setTo(0.5, 0.5);

        var homePossession = 0;
        var visitorPossession = 0;
        if (this.possHome != 0 || this.possVisitor != 0) {
            homePossession = Math.round((this.possHome / (this.possHome + this.possVisitor)) * 100);
            visitorPossession = Math.round((this.possVisitor / (this.possHome + this.possVisitor)) * 100);
        }
        this.possPause = game.add.text(game.world.centerX, game.world.centerY - 10, game.global.matchLabel.POSSESSION,
            {font: '28px PopWarner', fill: game.global.colors.DARKBLUE});
        this.possPause.anchor.setTo(0.5, 0.5);
        this.possPause.anchor.x = Math.round(this.possPause.width * 0.5) / this.possPause.width;

        var style = {font: '22px PopWarner', fill: game.global.colors.LIGHTBLUE};
        var splitString, count, lines;

        // Check whether the local team's name fits in one line
        var localNameString = this.homeTeam.name;
        if (localNameString.length > game.global.pauseStringLength) {
            splitString = localNameString.split(" ");
            count = splitString[0].length;
            lines = 0;
            localNameString = splitString[0];
            for (var i = 1; i < splitString.length; i++) {
                if (splitString[i].length + count < game.global.pauseStringLength) {
                    localNameString += " " + splitString[i];
                    count += 1 + splitString[i].length;
                } else {
                    localNameString += "\n" + splitString[i];
                    count = splitString[i].length;
                    lines++;
                }
            }
            this.possLocalName = game.add.text(game.world.centerX - 125, game.world.centerY + 45, localNameString, style);
            this.possLocalName.align = "center";
            this.possLocalName.lineSpacing = -10;
        } else {
            this.possLocalName = game.add.text(game.world.centerX - 125, game.world.centerY + 45, localNameString, style);
        }
        this.possLocalName.anchor.setTo(0.5, 0.5);
        this.possLocalName.anchor.x = Math.round(this.possLocalName.width * 0.5) / this.possLocalName.width;


        this.possText = game.add.text(game.world.centerX, game.world.centerY + 45,
            ' ' + homePossession + '% - ' + visitorPossession + '% ',
            {font: '22px PopWarner', fill: game.global.colors.LIGHTBLUE});
        this.possText.anchor.setTo(0.5);
        this.possText.anchor.x = Math.round(this.possText.width * 0.5) / this.possText.width;

        // Check whether the visitor team's name fits in one line
        var visitorNameString = this.visitorTeam.name;
        if (visitorNameString.length > game.global.pauseStringLength) {
            splitString = visitorNameString.split(" ");
            count = splitString[0].length;
            lines = 0;
            visitorNameString = splitString[0];
            for (i = 1; i < splitString.length; i++) {
                if (splitString[i].length + count < game.global.pauseStringLength) {
                    visitorNameString += " " + splitString[i];
                    count += 1 + splitString[i].length;
                } else {
                    visitorNameString += "\n" + splitString[i];
                    count = splitString[i].length;
                    lines++;
                }
            }
            this.possVisitorName = game.add.text(game.world.centerX + 125, game.world.centerY + 45, visitorNameString, style);
            this.possVisitorName.align = "center";
            this.possVisitorName.lineSpacing = -10;
        } else {
            this.possVisitorName = game.add.text(game.world.centerX + 125, game.world.centerY + 45, visitorNameString, style);
        }
        this.possVisitorName.anchor.setTo(0.5, 0.5);
        this.possVisitorName.anchor.x = Math.round(this.possVisitorName.width * 0.5) / this.possVisitorName.width;

        this.promptText = game.add.text(game.world.centerX, game.world.centerY + 170, pausePrompt,
            {font: '16px PopWarner', fill: game.global.colors.ORANGE});
        this.promptText.anchor.setTo(0.5);
        this.promptText.anchor.x = Math.round(this.promptText.width * 0.5) / this.promptText.width;
    },

    /* *****************************************
     * pauseExit
     *
     * Resumes the match
     * *****************************************/
    pauseExit: function () {
        if (this.pauseMenuActive) {
            this.iniTime = this.iniTime + (game.time.time - this.pauseTime);
            this.titlePause.destroy();
            this.scorePause.destroy();
            this.homePause.destroy();
            this.visitorPause.destroy();
            this.possPause.destroy();
            this.possLocalName.destroy();
            this.possText.destroy();
            this.possVisitorName.destroy();
            this.promptText.destroy();
            this.pauseMenu.destroy();

            this.pauseMenuActive = false;

            // If the game has ended
            if (this.endGame) {
                this.homeTeam.players = new Array();
                this.visitorTeam.players = new Array();
                game.state.start('leagueMenu');
            } else {
                game.paused = false;
            }
        }
    }
};
