// We create our play state
var instructionsState = {
    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * *****************************************/
    preload: function () {
        this.gamepadInst = false;
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75, 1.75);

        // Add a background image
        var bgshadow = game.add.image(0, 0, 'bg_shadow');
        bgshadow.scale.setTo(2.5, 1.75);

        // Display the name of the game
        var titleImg = game.add.image(3 * game.world.centerX / 5 + 10, game.world.centerY / 2, 'title');
        titleImg.scale.setTo(0.65);
        titleImg.anchor.setTo(0.5, 0.5);

        var styleIntro = {font: '18px Courier', fill: '#ffffff'};
        var style = {font: '14px Courier', fill: '#ffffff'};

        // Display the instructions
        var instructions = game.add.text(40, game.world.centerY / 2 + 115, game.global.instructions.INTRO, styleIntro);
        instructions.anchor.setTo(0, 0.5);

        var arrowkeys = game.add.image(100, game.world.centerY + 110, 'arrowkeys');
        arrowkeys.scale.setTo(0.1);
        arrowkeys.anchor.setTo(0.5, 0.75);
        var arrowInfo = game.add.text(200, game.world.centerY + 90, game.global.instructions.ARROWKEYS, style);
        arrowInfo.anchor.setTo(0, 0.5);

        var zKey = game.add.image(game.world.centerX + 40, game.world.centerY + 25, 'zKey');
        zKey.scale.setTo(0.1);
        zKey.anchor.setTo(0.5);
        var xKey = game.add.image(game.world.centerX + 40, game.world.centerY + 85, 'xKey');
        xKey.scale.setTo(0.1);
        xKey.anchor.setTo(0.5);
        var cKey = game.add.image(game.world.centerX + 40, game.world.centerY + 145, 'cKey');
        cKey.scale.setTo(0.1);
        cKey.anchor.setTo(0.5);

        var zInfo = game.add.text(game.world.centerX + 80, game.world.centerY + 25, game.global.instructions.ZKEY, style);
        zInfo.anchor.setTo(0, 0.5);
        var xInfo = game.add.text(game.world.centerX + 80, game.world.centerY + 85, game.global.instructions.XKEY, style);
        xInfo.anchor.setTo(0, 0.5);
        var cInfo = game.add.text(game.world.centerX + 80, game.world.centerY + 145, game.global.instructions.CKEY, style);
        cInfo.anchor.setTo(0, 0.5);

        // Return to main menu
        var optionStyle = {
            font: '26pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 4
        };
        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        var txt = game.add.text(100, 470, game.global.optionsLabel.BACK, optionStyle);
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(function (target) {
            game.state.start('menu');
        });
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);

        var next = game.add.text(650, 470, '>', optionStyle);
        next.inputEnabled = true;
        next.input.useHandCursor = true;
        next.events.onInputUp.add(function (target) {
            if (!this.gamepadInst) {
                target.text = '<';
                this.gamepadInst = true;

                arrowkeys.loadTexture('arrowkeys_gamepad');
                zKey.loadTexture('zKey_gamepad');
                xKey.loadTexture('xKey_gamepad');
                cKey.loadTexture('cKey_gamepad');

                instructions.text = game.global.instructions.INTRO_GP;
                arrowInfo.text = game.global.instructions.ARROWKEYS_GP;
                zInfo.text = game.global.instructions.ZKEY_GP;
                xInfo.text = game.global.instructions.XKEY_GP;
                cInfo.text = game.global.instructions.CKEY_GP;
            } else {
                target.text = '>';
                this.gamepadInst = false;

                arrowkeys.loadTexture('arrowkeys');
                zKey.loadTexture('zKey');
                xKey.loadTexture('xKey');
                cKey.loadTexture('cKey');

                instructions.text = game.global.instructions.INTRO;
                arrowInfo.text = game.global.instructions.ARROWKEYS;
                zInfo.text = game.global.instructions.ZKEY;
                xInfo.text = game.global.instructions.XKEY;
                cInfo.text = game.global.instructions.CKEY;
            }
        });
        next.events.onInputOver.add(onOver);
        next.events.onInputOut.add(onOut);
    }
};