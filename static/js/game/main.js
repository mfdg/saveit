// Initialise Phaser
var game = new Phaser.Game(800, 580, Phaser.AUTO, 'gameDiv');

// Define our 'global' variable
game.global = {

    // Evaluated values
    evaluated: false,
    evaluation: 0,

    playerSpeed: 1,
    playerResistance: 1,
    playerStrength: 1,
    playerDefense: 1,
    playerTechnique: 1,

    // Last state visited
    lastState: 0,
    states: {
        MENU: 0,
        LEAGUE_MENU: 1,
        RANKING: 2
    },

    // Gamepad activity
    gamepadConnected: false,

    music: null,
    ringSound: null,

    // Match duration
    duration: 60000 * 1 + 1000 * 2,

    // Number of players per team
    numPlayers: 7,

    // Number of teams per league
    numTeamLeague: 12,

    // Maximum length for team name
    maxTeamLength: 10,

    // Maximum mean and deviation (+1) for goal generation function
    gaussianMean: 3,
    gaussianDev: 3,

    // Delay to improve visual appearance
    delay: 3000,
    delayStart: 1000,

    // Radio in which to search for support
    supportRadius: 200,
    // Minimum radio to search for support
    minSupportRadius: 110,

    // Distance to reach for support
    reachSupportDistance: 100,

    // Distance in which an opposite player present a threat
    threatDistance: 50,

    swipeDistance: 25,

    maxCatchVelocity: 160,

    maxNameLength: 30,
    maxStringLength: 16,
    pauseStringLength: 12,
    scoreStringLength: 16,
    scoreStringLengthMid: 22,

    // Array with the 8 directions as Phaser.Points
    directionPoints: new Array(new Phaser.Point(0, -1), new Phaser.Point(1, -1),
        new Phaser.Point(1, 0), new Phaser.Point(1, 1),
        new Phaser.Point(0, 1), new Phaser.Point(-1, 1),
        new Phaser.Point(-1, 0), new Phaser.Point(-1, -1)),

    // Color palette
    colors: {
        ORANGE: "#ddd",
        // ORANGE: "#ff6f00",
        RED: "#c91517",
        LIGHTBLUE: "#9292c1",
        DARKBLUE: "#6060b0",
        BGBLUE: "#3a3a88",
        LIGHTGRAY: "#606060",
        DARKGRAY: "#202020",
        WHITE: "#ffffff",
        GOLDEN: "#edbc1d"
    },

    // Enums for readability
    direction: {
        UP: 0,
        UPRIGHT: 1,
        RIGHT: 2,
        DOWNRIGHT: 3,
        DOWN: 4,
        DOWNLEFT: 5,
        LEFT: 6,
        UPLEFT: 7
    },
    action: {
        WAIT: 0,
        KICK: 1,
        PASS: 2,
        RUN: 3,
        SWIPE: 4
    },
    side: {
        RIGHT: 0,
        LEFT: 1
    },

    // Formation array
    formations: new Array('1-3-2', '1-4-1', '2-1-3', '2-2-2', '2-3-1',
        '3-1-2', '3-2-1', '3-3', '4-1-1'),
    selectedFormation: 0,
    rivalSelectedFormation: 0,

    // Shirt color array
    shirtColors: new Array("red", "yellow", "green", "white"),

    // Player array
    players: new Array(),

    // Animal array
    animals: new Array('fox', 'donkey'),

    // Control array
    controls: new Array('simulate', 'controller'),

    // Labels for menus
    menuLabel: {
        START: "New game",
        LOAD: "Load game",
        LOG: "Log in",
        INSTRUCTIONS: "Instructions",
        OPTIONS: "Options",
        CREDITS: "Credits"
    },

    optionsLabel: {
        ENGLISH: "English",
        SPANISH: "Español",
        MUTE: "Mute sounds",
        NOTMUTE: "Play sounds",
        BACK: "Back",
        OK: "Accept"
    },

    instructions: {
        INTRO: "If you choose to manage your players directly,\nthis are the keys to use:",
        ARROWKEYS: "During the match you\nwill manage the player\nclosest" +
        " to the ball,\nwhich is marked by a\nblack pin over his head.\n" +
        "Use the arrow keys to\nmove the selected player.",
        ZKEY: "You can pass the ball to another\nplayer with the Z key.",
        XKEY: "Use the X key to kick the ball\nto goal.",
        CKEY: "Press the C key to try to steal\nthe ball from an opponent.",

        INTRO_GP: "If you use a gamepad, these are the specific\ncontrols:",
        ARROWKEYS_GP: "During the match you\nwill manage the player\nclosest" +
        " to the ball,\nwhich is marked by a\nblack pin over his head.\n" +
        "Use the arrow keys or\nthe joystic to move the\nselected player.",
        ZKEY_GP: "You can pass the ball to another\nplayer with the blue botton.",
        XKEY_GP: "Use the red button to kick the\nball to goal.",
        CKEY_GP: "Press the pink button to try to\nsteal the ball from an opponent."
    },

    home: {
        HOME: "Home"
    },

    friendlyMenuText: {
        YOURTEAM: "Your team",
        RIVALTEAM: "Rival team"
    },

    leagueMenuText: {
        NEXTMATCH: "Next match:"
    },

    endLeagueText: {
        BADEND: "Too bad...",
        NEUTRALEND: "Not bad,",
        GOODEND: "Congratulations!",
        FINISH: "You ended ",
        NEWLEAGUE: "Choose a new league to continue,\nor back to return"
    },

    selectorLabel: {
        NAME: "Name",
        USER: "User",
        COLOR: "Color",
        YOURS: "AI",
        RIVALS: "AI",
        PLAYER: "Control"
    },

    matchLabel: {
        POSSESSION: "Possession",
        CLICKTORESUME: "Click to return to your game",
        CLICKTOFINISH: "Click to return to the menu",
        PAUSE: "Pause",
        END: "End",
        HALF: "2nd Half"
    },

    tableLegend: {
        PLAYED: "Games played",
        WON: "Games won",
        DRAWN: "Games drawn",
        LOST: "Games lost",
        FOR: "Goals for",
        AGAINST: "Goals against",
        POINTS: "Points"
    },

    credits: {
        BY: "Developed and designed by:"
    },

    image: {
        PAUSE: "pause",
        END: "end",
        HOME: "menuHome",
        LEAGUE: "menuLeague",
        FRIENDLY: "menuFriendly",
        SAVE: "menuSave",
        EXIT: "menuExit",
        PLAY: "playButton"
    },

    attributes: new Array("Speed", "Resistance", "Strength", "Defense", "Technique")
};

// Create first state
var mainState = {

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Load progress bar and boot state game file.
     * *****************************************/
    preload: function () {
        // Load the image
        game.load.image('background', '../../static/images/game/menuBG.png');
        game.load.image('bg_shadow', '../../static/images/game/shadow.png');
        game.load.image('progressBar', '../../static/images/game/progressBar_balls.png');
        game.load.script('boot', '../../static/js/game/states/boot.js');
    },

    /* *****************************************
     * create
     *
     * Add the boot state and switch to it, to
     * show splash screen.
     * *****************************************/
    create: function () {
        game.state.add('boot', bootState);
        game.state.start('boot');
    }

};

// Start state machine via mainState
game.state.add('Main', mainState);
game.state.start('Main');