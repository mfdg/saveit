// Class that evaluates actions passed by the Footballers
DonkeyEvaluator = function (playState) {

    // Set game state
    this.state = playState;

    this.counter = 0;


    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * evaluate
     *
     * Evaluates the possible actions the player
     * could make, and returns the best action.
     *
     * Params
     *    player: the player that called this
     *    function.
     * *****************************************/
    this.evaluate = function (player) {
        // Set initial values
        var actions = [new Action(null, game.global.action.WAIT, 0)];
        var auxArray = null;

        // If a goalkeeper has the ball
        if (this.state.ball.owner != null) {
            if (this.state.ball.owner == this.state.homeTeam.goalkeeper
                || this.state.ball.owner == this.state.visitorTeam.goalkeeper) {
                auxArray = evaluateDirectionMove(this.state, player, player.iniPos);
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
                return chooseAction(actions);
            }
        }

        // If a teammate has the ball
        if (player.team.hasBall) {
            // Get to the opponent's goal
            auxArray = evaluateDirectionMove(this.state, player, player.team.oppGoal);
            if (auxArray != null) {
                actions = actions.concat(auxArray);
            }
        }
        // If not
        else {
            // Try to steal the ball
            auxArray = evaluateDirectionMove(this.state, player, player.ball);
            if (auxArray != null) {
                actions = actions.concat(auxArray);
            }
        }
        return chooseAction(actions);
    };

    /* *****************************************
     * evaluateGK
     *
     * Evaluates the possible actions the goal-
     * keeper could make, and returns the best
     * action.
     *
     * Params
     *    player: the goalkeeper that called this
     *    function.
     * *****************************************/
    this.evaluateGK = function (player) {
        // Set initial values
        var actions = [new Action(null, game.global.action.WAIT, 0)];
        var auxArray = null;

        var goalBot = player.team.goal.getBotPost();
        var goalTop = player.team.goal.getTopPost();
        // If goalkeeper has the ball
        if (player.controlledBall != null) {
            if (this.counter > 50 * game.rnd.integerInRange(1, 5)) {
                // Try to pass a player after a delay
                auxArray = passSupportingPlayer(this.state, player);
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
                this.counter = 0;
            } else {
                this.counter++;
                return actions[0];
            }
        }
        // If not, stay aligned with the ball
        else if ((this.state.ball.y < goalBot.y) && (this.state.ball.y > goalTop.y)
            && (this.state.ball.y != player.y)) {
            auxArray = evaluateDirectionMove(this.state, player, new Phaser.Point(player.x, player.ball.y));
            if (auxArray != null) {
                actions = actions.concat(auxArray);
            }
        }

        return chooseAction(actions);
    };

    /* *****************************************
     * evaluateExitField
     *
     * Evaluates the actions to exit the field
     * when each match part finishes
     *
     * Params
     *    player: the player that has to exit
     * *****************************************/
    this.evaluateExitField = function (player) {
        // Slowly make the player disappear
        if (player.alpha != 0){
            player.alpha -= 0.007;
        }
        // Get the best action to get to the exit door
        return chooseAction(evaluateDirectionMove(this.state, player,
            new Phaser.Point(game.world.centerX, 70)));
    }
};

DonkeyEvaluator.prototype.constructor = DonkeyEvaluator;