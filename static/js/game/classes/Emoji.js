// Constants
// EMPTY

// Class that is used to handle the different emojis a player can send
Emoji = function (game, x, y, type, clickable) {

    // Inherit from Sprite
    Phaser.Sprite.call(this, game, x, y, 'emo_' + type);
    //game.add.sprite(x, y, 'emo_' + type);

    // Set emoji type
    this.type = type;

    this.anchor.setTo(0.5, 0.5);

    // Scale the emoji
    this.scale.setTo(0.1, 0.1);

    // Clickable
    if (clickable) {
        this.inputEnabled = true;
        this.events.onInputDown.add(function (target) {
            game.add.tween(this.scale).to({x: 0.16, y: 0.16}, 150).to({x: 0.1, y: 0.1}, 120).start();
        }, this);
        this.events.onInputUp.add(function (target) {
            Client.sendEmoji(this.type);
            playOnlineState.toggleEmojis();
        }, this);
    }

    game.add.existing(this);

/*
    this.sendEmoji = function(target) {
        console.log(this.type);
    };*/

};


// Set inheritance and constructor
Emoji.prototype = Object.create(Phaser.Sprite.prototype);
Emoji.prototype.constructor = Emoji;