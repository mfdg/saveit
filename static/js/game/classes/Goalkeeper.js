// Constants
var playerKick = 200;
var playerPass = 180;

// Class that represents the players
Goalkeeper = function (game, image, team, evaluator, ball, attributes) {

    // Inherit from Player
    Player.call(this, game, team.goal.getTopPost().x, game.world.centerY + 20, image, team, evaluator, ball, attributes);

    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * move
     *
     * Moves the player, using AI or input.
     * *****************************************/
    this.move = function () {
        this.aiMove();

        if (this.controlledBall != null) {
            this.moveBall();
        }
    };

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * aiMove
     *
     * Moves the player, using AI.
     * *****************************************/
    this.aiMove = function () {
        this.doAction(this.evaluator.evaluateGK(this));
    };
};

// Set inheritance and constructor
Goalkeeper.prototype = Object.create(Player.prototype);
Goalkeeper.prototype.constructor = Goalkeeper;