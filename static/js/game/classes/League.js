// Constants
// EMPTY

// Class that represents a league
League = function () {

    // Set initial match index
    this.matchIndex = 0;

    // Initialize team array
    this.teams = new Array();

    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * buildLeague
     *
     * Builds the league, creating the teams.
     * *****************************************/
    this.buildLeague = function (attributes) {
        // Create a color array without our team's color
        var colors = new Array(), i = 0;
        for (i = 0; i < game.global.shirtColors.length; i++) {
            if (game.global.shirtColors[i] != game.teamColor) {
                colors.push(game.global.shirtColors[i]);
            }
        }
        // Create team name array with nameGen.js
        var nameList = createNames();
        // For each team
        for (i = 0; i < game.global.numTeamLeague - 1; i++) {
            // Set color, name & formation
            var color = colors[Math.floor(Math.random() * colors.length)];
            var name = nameList[i];
            var formation = game.global.formations[Math.floor(Math.random() * game.global.formations.length)];
            var team = new Team(null, null, color, name, formation,
                Math.random() * game.global.gaussianMean, Math.random() * game.global.gaussianDev, attributes);
            // Add to team list
            this.teams.push(team);
        }
    };

    /* *****************************************
     * nextMatch
     *
     * Returns your team's next match.
     * *****************************************/
    this.nextMatch = function () {
        // Difference between playing away or at home
        if (game.league.matchIndex < game.global.numTeamLeague) {
            if (game.league.matchIndex % 2) {
                // Play at home
                game.playerMatch = new Array(this.teams[0], this.teams[game.global.numTeamLeague - 1]);
            } else {
                // Play as visitor
                game.playerMatch = new Array(this.teams[game.global.numTeamLeague - 1], this.teams[0]);
            }
        } else {
            if (game.league.matchIndex % 2) {
                // Play as visitor
                game.playerMatch = new Array(this.teams[game.global.numTeamLeague - 1], this.teams[0]);
            } else {
                // Play at home
                game.playerMatch = new Array(this.teams[0], this.teams[game.global.numTeamLeague - 1]);
            }
        }
    };

    /* *****************************************
     * afterMatch
     *
     * Saves the results of your match and
     * simulates the rest of the matches.
     *
     * Params
     *    homeScore: score of your match's home
     *               team
     *    visitorScore: score of your match's
     *                  visitor team
     * *****************************************/
    this.afterMatch = function (homeScore, visitorScore) {
        // Update match result
        var win = 0; // 0 Draw, 1 Home, 2 Visitor
        if (homeScore > visitorScore) {
            win = 1;
        } else if (homeScore < visitorScore) {
            win = 2;
        }

        var home, visitor;
        // Difference between playing away or at home
        if (game.league.matchIndex < game.global.numTeamLeague) {
            if (game.league.matchIndex % 2) {
                // Play at home
                home = this.teams[0];
                visitor = this.teams[game.global.numTeamLeague - 1];

            } else {
                // Play as visitor
                visitor = this.teams[0];
                home = this.teams[game.global.numTeamLeague - 1];
            }
        } else {
            if (game.league.matchIndex % 2) {
                // Play as visitor
                visitor = this.teams[0];
                home = this.teams[game.global.numTeamLeague - 1];
            } else {
                // Play at home
                home = this.teams[0];
                visitor = this.teams[game.global.numTeamLeague - 1];
            }
        }

        // Set points and wins/loses for both teams
        if (win == 1) {
            home.gamesWon++;
            home.points += 3;
            visitor.gamesLost++;
        } else if (win == 2) {
            home.gamesLost++;
            visitor.gamesWon++;
            visitor.points += 3;
        } else {
            home.gamesDrawn++;
            home.points++;
            visitor.gamesDrawn++;
            visitor.points++;
        }
        // Set other results
        home.goalsScored += homeScore;
        home.goalsAgainst += visitorScore;
        visitor.goalsScored += visitorScore;
        visitor.goalsAgainst += homeScore;


        // Match simulation
        var teamA, teamB, goalsA, goalsB;
        for (var i = 1; i < game.global.numTeamLeague / 2; i++) {
            teamA = this.teams[i];
            teamB = this.teams[game.global.numTeamLeague - i - 1];
            // Use gaussian goal generator to create result
            goalsA = Math.floor(teamA.goalGen());
            goalsB = Math.floor(teamB.goalGen());
            // Set points and wins/loses for each team
            if (goalsA > goalsB) {
                teamA.gamesWon++;
                teamA.points += 3;
                teamB.gamesLost++;
            } else if (goalsA < goalsB) {
                teamA.gamesLost++;
                teamB.gamesWon++;
                teamB.points += 3;
            } else {
                teamA.gamesDrawn++;
                teamA.points++;
                teamB.gamesDrawn++;
                teamB.points++;
            }
            // Set other results
            teamA.goalsScored += goalsA;
            teamA.goalsAgainst += goalsB;
            teamB.goalsScored += goalsB;
            teamB.goalsAgainst += goalsA;
        }

        // Update matchIndex
        game.matchIndex++;

        // League rotation
        this.rotate();
    };

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * rotate
     *
     * Rotates the team array, preparing it for
     * another round using a round robin
     * procedure.
     * *****************************************/
    this.rotate = function () {
        // Extract last team in the list
        var rotated = this.teams.pop();
        // Place it in the second position
        this.teams.splice(1, 0, rotated);
        // Update league match index
        this.matchIndex++;
    };
};

// Set constructor
League.prototype.constructor = League;