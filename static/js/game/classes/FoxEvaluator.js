// Class that evaluates actions passed by the Footballers
FoxEvaluator = function (playState) {

    // Set game state
    this.state = playState;

    this.counter = 0;


    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * evaluate
     *
     * Evaluates the possible actions the player
     * could make, and returns the best action.
     *
     * Params
     *    player: the player that called this
     *    function.
     * *****************************************/
    this.evaluate = function (player) {
        // Set initial values
        var actions = [new Action(null, game.global.action.WAIT, 0)];
        var auxArray = null;


        // If a goalkeeper has the ball
        if (this.state.ball.owner != null) {
            if (this.state.ball.owner == this.state.homeTeam.goalkeeper
                || this.state.ball.owner == this.state.visitorTeam.goalkeeper) {
                if (this.state.ball.owner == player.team.goalkeeper &&
                    playersCloseToBall(this.state, player.team) < 3) {
                    auxArray = evaluateDirectionMove(this.state, player,
                        new Phaser.Point((player.iniPos.x + player.team.goal.x) / 2, (player.iniPos.y + player.team.goal.y) / 2));
                } else {
                    auxArray = evaluateDirectionMove(this.state, player, player.iniPos);
                }
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
                return chooseAction(actions);
            }
        }

        // If the team is attacking
        if (player.team.hasBall) {
            // If this player has the ball
            if (player.controlledBall != null) {
                // Shoot if it has goal option
                var goalAction = hasGoalOption(this.state, player);
                if (goalAction != null) {
                    actions.push(goalAction);
                }
                // Pass if another player in position
                var passAction = passSupportingPlayer(this.state, player);
                if (passAction != null) {
                    actions = actions.concat(passAction);
                }
                // Evaluate movement and pass
                auxArray = evaluateDirectionMove(this.state, player,
                    new Phaser.Point(player.team.oppGoal.x + (player.team.oppGoal.width / 2), player.team.oppGoal.y + (player.team.oppGoal.height / 2)));
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }

            }
            // If this player doesn't have the ball
            else {
                // If player can support
                // (is close, can get to a goal position and there is not another support close to it)
                var supportAction = hasSupportOption(player);
                if (supportAction != null) {
                    actions = actions.concat(supportAction);
                } else {
                    // Evaluate movement
                    auxArray = evaluateMove(this.state, player);
                    if (auxArray != null) {
                        actions = actions.concat(auxArray);
                    }
                }
            }
        }
        // If the team is defending
        else if (this.state.ball.owner != null) {
            // If player is closest to ball
            if (isClosestToBall(this.state, player)) {
                // Evaluate movement
                auxArray = evaluateDirectionMove(this.state, player, this.state.ball.body);
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
            }
            // Evaluate movements
            else {
                // Evaluate movement
                auxArray = evaluateMove(this.state, player);
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
            }
        }
        // If the ball is loose
        else {
            // If player is closest to ball
            if (isClosestToBall(this.state, player)) {
                // Evaluate movement
                auxArray = evaluateDirectionMove(this.state, player, this.state.ball.body);
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
            }
            else {
                // Evaluate movement
                auxArray = evaluateMove(this.state, player);
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
            }
        }

        return chooseAction(actions);
    };

    /* *****************************************
     * evaluateGK
     *
     * Evaluates the possible actions the goal-
     * keeper could make, and returns the best
     * action.
     *
     * Params
     *    player: the goalkeeper that called this
     *    function.
     * *****************************************/
    this.evaluateGK = function (player) {
        // Set initial values
        var actions = [new Action(null, game.global.action.WAIT, 0)];
        var auxArray = null;

        var goalBot = player.team.goal.getBotPost();
        var goalTop = player.team.goal.getTopPost();
        // If goalkeeper has the ball
        if (player.controlledBall != null) {
            if (this.counter > 50 * game.rnd.integerInRange(1, 5)) {
                // Try to pass a player after a delay
                auxArray = passSupportingPlayer(this.state, player);
                if (auxArray != null) {
                    actions = actions.concat(auxArray);
                }
                this.counter = 0;
            } else {
                this.counter++;
                return actions[0];
            }
        }
        // If not, stay aligned with the ball
        else if ((this.state.ball.y < goalBot.y) && (this.state.ball.y > goalTop.y)
            && (this.state.ball.y != player.y)) {
            auxArray = evaluateDirectionMove(this.state, player, new Phaser.Point(player.x, player.ball.y));
            if (auxArray != null) {
                actions = actions.concat(auxArray);
            }
        }

        return chooseAction(actions);
    };

    /* *****************************************
     * evaluateExitField
     *
     * Evaluates the actions to exit the field
     * when each match part finishes
     *
     * Params
     *    player: the player that has to exit
     * *****************************************/
    this.evaluateExitField = function (player) {
        // Slowly make the player disappear
        if (player.alpha != 0) {
            player.alpha -= 0.007;
        }
        // Get the best action to get to the exit door
        return chooseAction(evaluateDirectionMove(this.state, player,
            new Phaser.Point(game.world.centerX, 70)));
    }

};

FoxEvaluator.prototype.constructor = FoxEvaluator;