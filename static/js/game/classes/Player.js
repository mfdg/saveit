// Constants
var playerVelocity = 80;
var playerVelocityDiag = playerVelocity * 0.66;
var playerFps = 7;

// Class that represents the players in general
Player = function (game, x, y, image, team, evaluator, ball, attributes) {
    this.counter = 0;
    this.lastAnimation = null;

    // Inherit from Sprite
    Phaser.Sprite.call(this, game, x, y, image);

    // ATTRIBUTES
    this.speed = 1 / 6 * attributes[0] + 1 / 3;  // Velocidad
    this.resistance = attributes[1];                // Aguantar el partido con los atributos
    this.strength = 1/9 * attributes[2] + 8/9;      // Tirar mas fuerte el balon
    this.defense = attributes[3];                   // Quitar el balon
    this.accuracy = attributes[4];                  // Tirar más preciso el balón

    // Set player's team
    this.team = team;

    // Set the evaluator function
    this.evaluator = evaluator;

    // Set initial position
    this.iniPos = new Phaser.Point(x, y);

    // Initialize direction
    this.direction = new Phaser.Point(0, 1);

    // Set if player has the ball
    this.controlledBall = null;

    // Set the ball reference
    this.ball = ball;

    // Last action done
    this.action = new Action(null, game.global.action.WAIT, 0);

    // Enable physics and resize collision box
    game.physics.p2.enable(this, false);
    this.body.setCircle((2 * this.height / 5) / 2);
    this.anchor.setTo(0.5, 0.8);
    this.body.fixedRotation = true;
    game.global.players.push(this);

    // Add animations to player: 8 directions
    this.animations.add('walkdown', [0, 1, 2, 3]);
    this.animations.add('walkdwlf', [4, 5, 6, 7]);
    this.animations.add('walkdwri', [8, 9, 10, 11]);
    this.animations.add('walkleft', [12, 13, 14, 15]);
    this.animations.add('walkright', [16, 17, 18, 19]);
    this.animations.add('walkup', [20, 21, 22, 23]);
    this.animations.add('walkuplf', [24, 25, 26, 27]);
    this.animations.add('walkupri', [28, 29, 30, 31]);


    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * affectResistance
     *
     * Modify the attributes depending on the
     * resistance of the player
     * *****************************************/
    this.affectResistance = function () {
        this.speed *= (this.resistance / 18 + 2 / 18);
        this.strength *= (this.resistance / 18 + 2 / 18);
        this.defense *= (this.resistance / 18 + 2 / 18);
        this.accuracy *= (this.resistance / 18 + 2 / 18);
    };

    /* *****************************************
     * setBallControl
     *
     * Sets the controlledBall parameter.
     *
     * Params
     *    ball: the ball or null
     * *****************************************/
    this.setBallControl = function (ball) {
        if (ball != null) {
            this.team.hasBall = true;
        } else {
            this.team.hasBall = false;
        }
        this.controlledBall = ball;
    };

    /* *****************************************
     * returnToIniPosition
     *
     * Moves the player to its original position.
     * *****************************************/
    this.returnToIniPosition = function () {
        this.controlledBall = null;
        this.body.x = this.iniPos.x;
        this.body.y = this.iniPos.y;
    };

    /* *****************************************
     * exitField
     *
     * Moves the player to the exit door
     * *****************************************/
    this.exitField = function () {
        var newAction = this.evaluator.evaluateExitField(this);

        if (newAction.action == game.global.action.RUN) {
            if (this.speed != null) {
                this.selectRun(newAction, this.speed);
            } else {
                this.selectRun(newAction, 1);
            }
        }
    };

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * moveTo
     *
     * Move the player in a direction.
     *
     * Params
     *    velocityX: velocity on x axis
     *    velocityY: velocity on y axis
     *    animation: name of the animation to display
     *    dirX: x factor of direction vector
     *    dirY: y factor of direction vector
     * *****************************************/
    this.moveTo = function (velocityX, velocityY, animation, dirX, dirY) {
        this.body.velocity.x = velocityX;
        this.body.velocity.y = velocityY;
        this.counter++;
        if (animation != null) {
            if (this.counter > 10 && animation != this.lastAnimation) {
                this.animations.play(animation, playerFps, true);
                this.direction = new Phaser.Point(dirX, dirY);
                this.lastAnimation = animation;
                this.counter = 0;
            } else {
                this.animations.play(this.lastAnimation, playerFps, true);
            }
        } else {
            this.animations.stop();
        }
    };

    /* *****************************************
     * doAction
     *
     * TODO
     * *****************************************/
    this.doAction = function (newAction, debug) {
        debug = typeof debug !== 'undefined' ? debug : false;
        if (debug == true) {
            console.log(newAction);
        }

        if (newAction.action == game.global.action.RUN) {
            this.selectRun(newAction, this.speed);
        } else if (newAction.action == game.global.action.WAIT) {
            // Stop the player
            this.moveTo(0, 0, null, null, null);
        } else if (newAction.action == game.global.action.KICK) {
            // Kick the ball
            this.direction = new Phaser.Point(game.global.directionPoints[newAction.direction].x,
                game.global.directionPoints[newAction.direction].y);
            if (this.moveBall() == 0) {
                this.controlledBall.kick(playerKick * this.strength, (randomBetween(-0.5, 0.5) / this.accuracy) + this.direction.x, (randomBetween(-0.5, 0.5) / this.accuracy) + this.direction.y);
            }

        } else if (newAction.action == game.global.action.PASS) {
            // Pass the ball
            this.direction = new Phaser.Point(game.global.directionPoints[newAction.direction].x,
                game.global.directionPoints[newAction.direction].y);
            if (this.moveBall() == 0) {
                this.controlledBall.kick(playerPass * this.strength, (randomBetween(-0.5, 0.5) / this.accuracy) + this.direction.x, (randomBetween(-0.5, 0.5) / this.accuracy) + this.direction.y);
            }

        } else if (newAction.action == game.global.action.SWIPE) {
            // Stop the player
            this.ball.push(this.defense);
        }

        // Set last action
        if (newAction !== null) {
            this.action = newAction;
        }
    };

    /* *****************************************
     * selectRun
     *
     * Select the direction in which to move
     *
     *  Params
     *    newAction: action RUN with the direction
     * *****************************************/
    this.selectRun = function (newAction, speed) {
        if (newAction.direction == game.global.direction.UP) {
            // Move the player upwards
            this.moveTo(0, -playerVelocity * speed, 'walkup', 0, -1);
        } else if (newAction.direction == game.global.direction.UPRIGHT) {
            // Move the player to the up right
            this.moveTo(playerVelocityDiag * speed, -playerVelocityDiag * speed, 'walkupri', 1, -1);
        } else if (newAction.direction == game.global.direction.RIGHT) {
            // Move the player to the right
            this.moveTo(playerVelocity * speed, 0, 'walkright', 1, 0);
        } else if (newAction.direction == game.global.direction.DOWNRIGHT) {
            // Move the player to the down right
            this.moveTo(playerVelocityDiag * speed, playerVelocityDiag * speed, 'walkdwri', 1, 1);
        } else if (newAction.direction == game.global.direction.DOWN) {
            // Move the player downwards
            this.moveTo(0, playerVelocity * speed, 'walkdown', 0, 1);
        } else if (newAction.direction == game.global.direction.DOWNLEFT) {
            // Move the player to the down left
            this.moveTo(-playerVelocityDiag * speed, playerVelocityDiag * speed, 'walkdwlf', -1, 1);
        } else if (newAction.direction == game.global.direction.LEFT) {
            // Move the player to the left
            this.moveTo(-playerVelocity * speed, 0, 'walkleft', -1, 0);
        } else if (newAction.direction == game.global.direction.UPLEFT) {
            // Move the player to the up left
            this.moveTo(-playerVelocityDiag * speed, -playerVelocityDiag * speed, 'walkuplf', -1, -1);
        }
    };

    /* *****************************************
     * moveBall
     *
     * Move the ball along with the player.
     * *****************************************/
    this.moveBall = function () {
        this.controlledBall.body.x = this.body.x + this.width / 2 * this.direction.x;
        this.controlledBall.body.y = this.body.y + this.height / 4 * this.direction.y;
        return 0;
    };
};

// Set inheritance and constructor
Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;