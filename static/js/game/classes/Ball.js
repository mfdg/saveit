// Constants
// EMPTY

// Class that represents the ball
Ball = function (game, x, y) {

    // Inherit from Sprite
    Phaser.Sprite.call(this, game, x, y, 'ball');

    // Set initial owner as null
    this.owner = null;

    // Enable physics
    game.physics.p2.enable(this, false);
    this.body.setCircle(5);
    this.anchor.setTo(0.5, 0.5);
    this.body.damping = 0.5;

    // Scale the ball
    this.scale.setTo(0.068, 0.068);


    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * kick
     *
     * Accelerates the ball as if kicked, and
     * sets the owner to null.
     *
     * Params:
     *    force: the force with which to kick
     *    directX: direction in x axis
     *    directY: direction in Y axis
     * *****************************************/
    this.kick = function (force, directX, directY) {
        var direction = new Phaser.Point(directX, directY);
        direction.normalize();

        this.body.velocity.x = direction.x * force;
        this.body.velocity.y = direction.y * force;
        this.body.damping = 0.5;
        this.owner.setBallControl(null);
        this.owner = null;

        // Client.moveBall({
        //     velX: this.body.velocity.x,
        //     velY: this.body.velocity.y
        // })
    };

    /* *****************************************
     * push
     *
     * Accelerates the ball as if pushed, and
     * sets the owner to null. Can be used even
     * if you are not the owner.
     * *****************************************/
    this.push = function (defenderVal) {
        var direction = new Phaser.Point(game.rnd.integerInRange(0, 2) - 1, game.rnd.integerInRange(0, 2) - 1);
        direction.normalize();

        this.body.velocity.x += direction.x * 40;
        this.body.velocity.y += direction.y * 40;
        this.body.damping = 0.5;
        if (this.owner != null) {
            this.owner.setBallControl(null);
            this.owner = null;
        }
        // Client.moveBall({
        //     velX: this.body.velocity.x,
        //     velY: this.body.velocity.y
        // });
    };

    /* *****************************************
     * updateOwner
     *
     * Updates the owner of the ball.
     *
     * Params
     *    players: array of players
     * *****************************************/
    this.updateOwner = function (players) {
        if ((Math.sqrt((this.body.velocity.x * this.body.velocity.x ) +
                (this.body.velocity.y * this.body.velocity.y)) <= game.global.maxCatchVelocity)) {
            for (var i = 0; i < players.length; i++) {
                if (this.owner == null
                    && Phaser.Math.distance(players[i].x, players[i].y, this.x, this.y) <= 15) {
                    // if (this.owner != null) {
                    //     this.owner.setBallControl(null);
                    // }
                    this.setOwner(players[i]);
                    break;
                }
            }
        }
    };

    /* *****************************************
     * setOwner
     *
     * Sets the owner of the ball.
     *
     * Params:
     *    owner: new owner
     * *****************************************/
    this.setOwner = function (owner) {
        if (owner != null) {
            this.owner = owner;
            this.owner.setBallControl(this);
            this.body.damping = 0.7;
        }
    };

    /* *****************************************
     * returnToIniPosition
     *
     * Moves the ball to its original position.
     * *****************************************/
    this.returnToIniPosition = function () {
        this.owner = null;
        this.body.x = game.world.centerX;
        this.body.y = game.world.centerY + 20;
    };
};

// Set inheritance and constructor
Ball.prototype = Object.create(Phaser.Sprite.prototype);
Ball.prototype.constructor = Ball;