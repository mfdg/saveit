// Constants
// EMPTY

// Class that represents the goals
Goal = function (game, side) {

    // Set side
    this.side = side;

    // Inherit from Rectangle
    if (side == game.global.side.LEFT) {
        Phaser.Rectangle.call(this, game.world.centerX - 370, game.world.centerY - 49, 16, 150);
    } else {
        Phaser.Rectangle.call(this, game.world.centerX + 355, game.world.centerY - 49, 16, 150);
    }


    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * getCenter
     *
     * Returns the center of the goal.
     * *****************************************/
    this.getCenter = function () {
        return new Phaser.Point(this.x + this.width, this.y + (this.height/2));
    };

    /* *****************************************
     * getTopPost
     *
     * Returns the top post coordinates.
     * *****************************************/
    this.getTopPost = function () {
        if (this.side == game.global.side.LEFT) {
            return new Phaser.Point(this.x + this.width, this.y);
        } else {
            return new Phaser.Point(this.x, this.y);
        }
    };

    /* *****************************************
     * getBotPost
     *
     * Returns the bot post coordinates.
     * *****************************************/
    this.getBotPost = function () {
        if (this.side == game.global.side.LEFT) {
            return new Phaser.Point(this.x + this.width, this.y + this.height);
        } else {
            return new Phaser.Point(this.x, this.y + this.height);
        }
    };

    /* *****************************************
     * isGoal
     *
     * Checks if the ball is inside the goal.
     *
     * Params
     *    ball: ball
     * *****************************************/
    this.isGoal = function (ball) {
        if (this.side == game.global.side.LEFT) {
            return this.contains(ball.x + (ball.width / 2), ball.y + (ball.height / 2));
        } else {
            return this.contains(ball.x - (ball.width / 2), ball.y + (ball.height / 2));
        }
    };
};

// Set inheritance and constructor
Goal.prototype = Object.create(Phaser.Rectangle.prototype);
Goal.prototype.constructor = Goal;