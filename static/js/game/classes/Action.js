// Constants
// EMPTY

// Class that represents the actions the Footballers can do
Action = function (direction, action, score) {

    // Set direction of the action
    this.direction = direction;

    // Set action type
    this.action = action;

    // Set score of this action
    this.score = score;
};

// Set constructor
Action.prototype.constructor = Action;