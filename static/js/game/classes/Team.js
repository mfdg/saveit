// Constants

// X displacements
var defX = -150;
var midX = -20;
var strX = 80;

// Y displacements
// generaL
var adj = 20;

// - 4 in a row
var wing4Y = 190;
var mid4Y = 70;

// - 3 in a row
var wing3Y = 150;

// - 2 in a row
var mid2Y = 100;

// Create the formations with the players in position
var formations = {
    "4-1-1": new Array(new Phaser.Point(defX, -wing4Y + adj), new Phaser.Point(defX, -mid4Y + adj),
        new Phaser.Point(defX, mid4Y + adj), new Phaser.Point(defX, wing4Y + adj),
        new Phaser.Point(midX, adj),
        new Phaser.Point(strX, adj)),
    "3-3": new Array(new Phaser.Point(defX, -wing3Y + adj), new Phaser.Point(defX, adj), new Phaser.Point(defX, wing3Y + adj),
        new Phaser.Point(midX, -wing3Y + adj), new Phaser.Point(midX, adj), new Phaser.Point(midX, wing3Y + adj)),
    "3-2-1": new Array(new Phaser.Point(defX, -wing3Y + adj), new Phaser.Point(defX, adj), new Phaser.Point(defX, wing3Y + adj),
        new Phaser.Point(midX, -mid2Y + adj), new Phaser.Point(midX, mid2Y + adj),
        new Phaser.Point(strX, adj)),
    "3-1-2": new Array(new Phaser.Point(defX, -wing3Y + adj), new Phaser.Point(defX, adj), new Phaser.Point(defX, wing3Y + adj),
        new Phaser.Point(midX, adj),
        new Phaser.Point(strX, -mid2Y + adj), new Phaser.Point(strX, mid2Y + adj)),
    "2-3-1": new Array(new Phaser.Point(defX, -mid2Y + adj), new Phaser.Point(defX, mid2Y + adj),
        new Phaser.Point(midX, -wing3Y + adj), new Phaser.Point(midX, adj), new Phaser.Point(midX, wing3Y + adj),
        new Phaser.Point(strX, adj)),
    "2-2-2": new Array(new Phaser.Point(defX, -mid2Y + adj), new Phaser.Point(defX, mid2Y + adj),
        new Phaser.Point(midX, -mid2Y + adj), new Phaser.Point(midX, mid2Y + adj),
        new Phaser.Point(strX, -mid2Y + adj), new Phaser.Point(strX, mid2Y + adj)),
    "2-1-3": new Array(new Phaser.Point(defX, -mid2Y + adj), new Phaser.Point(defX, mid2Y + adj),
        new Phaser.Point(midX, adj),
        new Phaser.Point(strX, -wing3Y + adj), new Phaser.Point(strX, adj), new Phaser.Point(strX, wing3Y + adj)),
    "1-4-1": new Array(new Phaser.Point(defX, adj),
        new Phaser.Point(midX, -wing4Y + adj), new Phaser.Point(midX, -mid4Y + adj),
        new Phaser.Point(midX, mid4Y + adj), new Phaser.Point(midX, wing4Y + adj),
        new Phaser.Point(strX, adj)),
    "1-3-2": new Array(new Phaser.Point(defX, adj),
        new Phaser.Point(midX, -wing3Y + adj), new Phaser.Point(midX, adj), new Phaser.Point(midX, wing3Y + adj),
        new Phaser.Point(strX, -mid2Y + adj), new Phaser.Point(strX, mid2Y + adj)),

};

// Class that represents the players
Team = function (goal, oppGoal, color, name, formation, mean, stdev) {

    // Reference to the team's goal
    this.goal = goal;

    // Reference to the opponent's goal
    this.oppGoal = oppGoal;

    // Set the team's color
    this.color = color;

    // Set the team's name
    this.name = name;

    // Set the team's formation
    this.formation = formation;

    // Set mean and stdev for saving purposes
    this.mean = mean;
    this.stdev = stdev;

    // Set the team's goal coefficient
    this.goalGen = gaussian(mean, stdev);

    // Whether the team has the ball
    this.hasBall = false;

    // Player array
    this.players = new Array();
    this.goalkeeper = null;

    // Table figures
    this.gamesWon = 0;
    this.gamesDrawn = 0;
    this.gamesLost = 0;
    this.goalsScored = 0;
    this.goalsAgainst = 0;
    this.points = 0;

    // Initial positions array
    this.iniPositions = formations[formation];

    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * buildTeam
     *
     * Builds the team, creating the players: the
     * footballers and the goalkeeper.
     *
     * Params:
     *    evaluator: evaluator to be used by the
     *               players
     *    ball: the ball
     *    renderQueue: the queue that is used to
     *                 correctly render the
     *                 players and ball
     * *****************************************/
    this.buildTeam = function (evaluator, ball, attributes, renderQueue) {
        // Create the goalkeeper
        this.addGoalkeeper(evaluator, ball, attributes, renderQueue);

        var appearanceCode, appearanceName;
        // Create footballers until you fill the number of players specified in the global variable
        for (var i = 1; i < game.global.numPlayers; i++) {
            appearanceCode = game.rnd.integerInRange(0, 3);
            appearanceName = null;
            // Decide on the base appearance of your player
            switch (appearanceCode) {
                case 0:
                    appearanceName = "cocoa_";
                    break;
                case 1:
                    appearanceName = "coffee_";
                    break;
                case 2:
                    appearanceName = "milk_";
                    break;
                default:
                    appearanceName = "vanilla_";
                    break;
            }
            // Check the correct number of iniPositions is filled
            if (this.iniPositions.length > i - 1) {
                // Check if the player is being placed correctly
                if (this.goal.side == game.global.side.LEFT) {
                    this.addPlayer(game.world.centerX + this.iniPositions[i - 1].x,
                        game.world.centerY + this.iniPositions[i - 1].y,
                        appearanceName + this.color, evaluator, ball, attributes, renderQueue);
                } else {
                    this.addPlayer(game.world.centerX - this.iniPositions[i - 1].x,
                        game.world.centerY + this.iniPositions[i - 1].y,
                        appearanceName + this.color, evaluator, ball, attributes, renderQueue);
                }
            }
        }
    };

    /* *****************************************
     * setGoals
     *
     * Sets the team and the opponent's goal
     *
     * Params:
     *    goal: the team's goal
     *    oppGoal: the opponent's goal
     * *****************************************/
    this.setGoals = function (goal, oppGoal) {
        this.goal = goal;
        this.oppGoal = oppGoal;
    };

    /* *****************************************
     * setFormation
     *
     * Sets the team formation
     *
     * Params:
     *    formation: the team's formation
     * *****************************************/
    this.setFormation = function (formation) {
        // Translate formation into iniPositions
        this.iniPositions = formations[formation];
    };

    /* *****************************************
     * updatePlayers
     *
     * Updates the players.
     * *****************************************/
    this.updatePlayers = function () {
        // For every player, move
        for (var i = 0; i < this.players.length; i++) {
            this.players[i].move();
        }
    };

    /* *****************************************
     * updatePlayersExit
     *
     * Updates the players so that they exit the
     * field.
     * *****************************************/
    this.updatePlayersExit = function () {
        for (var i = 0; i < this.players.length; i++) {
            this.players[i].exitField();
        }
    };

    /* *****************************************
     * returnPlayers
     *
     * Return the players to their initial posi-
     * tions.
     * *****************************************/
    this.returnPlayers = function () {
        this.hasBall = false;
        for (var i = 0; i < this.players.length; i++) {
            if (this.players[i].alpha != 1) {
                this.players[i].alpha = 1;
            }
            this.players[i].returnToIniPosition();
        }
    };

    /* *****************************************
     * switchSides
     *
     * Switches the sides, changing the goals and
     * initial positions of its players.
     * *****************************************/
    this.switchSides = function () {
        var i;
        var aux = this.goal;
        this.goal = this.oppGoal;
        this.oppGoal = aux;

        // Change goalkeeper side
        this.goalkeeper.iniPos.x = this.goal.getTopPost().x;
        this.goalkeeper.body.velocity.x = 0;
        this.goalkeeper.body.velocity.y = 0;
        this.goalkeeper.animations.stop();

        // Change each footballer
        if (this.goal.side == game.global.side.LEFT) {
            for (i = 1; i < game.global.numPlayers; i++) {
                this.players[i].iniPos.x = game.world.centerX + this.iniPositions[i - 1].x;
                this.players[i].body.velocity.x = 0;
                this.players[i].body.velocity.y = 0;
                this.players[i].animations.stop();
            }
        } else {
            for (i = 1; i < game.global.numPlayers; i++) {
                this.players[i].iniPos.x = game.world.centerX - this.iniPositions[i - 1].x;
                this.players[i].body.velocity.x = 0;
                this.players[i].body.velocity.y = 0;
                this.players[i].animations.stop();
            }
        }
    };

    this.getPlayerClosestToBall = function (state) {
        var i;
        var player = this.players[1];
        if (this.hasBall && state.ball.owner != this.goalkeeper) {
            for (i = 1; i < this.players.length; i++) {
                if (this.players[i].controlledBall != null) {
                    player = this.players[i];
                }
            }
        } else {
            var distance = 999;
            for (i = 1; i < this.players.length; i++) {
                var auxDist = Phaser.Math.distance(this.players[i].body.x, this.players[i].body.y,
                    state.ball.body.x, state.ball.body.y);
                if (auxDist < distance) {
                    distance = auxDist;
                    player = this.players[i];
                }
            }
        }
        return player;
    };


    /* *****************************************
     * affectResistanceToPlayers
     *
     * affectResistance to every player
     * *****************************************/
    this.affectResistanceToPlayers = function() {
        for (var i = 0; i < this.players.length; i++) {
            this.players[i].affectResistance();
        }
    };

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * addPlayer
     *
     * Adds a player to the team
     *
     * Params:
     *    posX: initial x position
     *    posY: initial y position
     *    sprites: name of the spritesheet
     *    evaluator: evaluator to be used for AI
     *    ball: the ball
     *    renderQueue: queue used to render cor-
     *                 rectly
     * *****************************************/
    this.addPlayer = function (posX, posY, sprites, evaluator, ball, attributes, renderQueue) {
        // New footballer
        var p1 = new Footballer(game, posX, posY, sprites,
            false, this, evaluator, ball, attributes);
        // Add to render queue and initial setups
        p1 = renderQueue.add(p1);
        p1.body.setCollisionGroup = game.movableObj;
        p1.body.collides = [game.movableObj, game.unmovObj];
        this.players.push(p1);
    };

    /* *****************************************
     * addGoalkeeper
     *
     * Adds the goalkeeper to the team
     *
     * Params:
     *    evaluator: evaluator to be used for AI
     *    ball: the ball
     *    renderQueue: queue used to render cor-
     *                 rectly
     * *****************************************/
    this.addGoalkeeper = function (evaluator, ball, attributes, renderQueue) {
        var appearanceCode = game.rnd.integerInRange(0, 3);
        var appearanceName = null;
        // Decide on the base appearance of your player
        switch (appearanceCode) {
            case 0:
                appearanceName = "cocoa_";
                break;
            case 1:
                appearanceName = "coffee_";
                break;
            case 2:
                appearanceName = "milk_";
                break;
            default:
                appearanceName = "vanilla_";
                break;
        }
        // New goalkeeper
        this.goalkeeper = new Goalkeeper(game, appearanceName + this.color, this, evaluator, ball, attributes);
        // Add to render queue and initial setups
        this.goalkeeper = renderQueue.add(this.goalkeeper);
        this.goalkeeper.body.setCollisionGroup = game.movableObj;
        this.goalkeeper.body.collides = [game.movableObj, game.unmovObj];
        // Add to player array
        this.players.push(this.goalkeeper);
    };
};

// Set constructor
Team.prototype.constructor = Team;