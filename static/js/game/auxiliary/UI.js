/*******************************************
 *              UI FUNCTIONS               *
 *******************************************/

/* *****************************************
 * createHeader
 *
 * Create the header common to several states
 * *****************************************/
function createHeader () {
    var menuBar = game.add.sprite(0, 0, 'menuBar');
    // menuBar.scale.setTo(0.46, 0.35);
    // Display the name of the game
    var titleImg = game.add.image(15, 14, 'title');
    titleImg.scale.setTo(0.28);
}

/* *****************************************
 * addMenuOption
 *
 * Factory for the easy creation of menu
 * options.
 *
 * Params
 *    img: name of the image to be shown
 *    callback: function to be executed when
 *              clicked
 *    state: current state
 * *****************************************/
function addMenuOption(img, callback, state) {
    var icon = game.add.image(0, (state.optionCount * 85), img);
    icon.scale.setTo(state.menuOptionScale);

    icon.x = icon.x + icon.width/2;
    icon.y = icon.y + icon.height/2;
    icon.anchor.setTo(0.5);

    var onOver = function (target) {
        target.loadTexture(img + "_hover");
    };
    var onOut = function (target) {
        target.loadTexture(img);
    };
    icon.inputEnabled = true;
    icon.input.useHandCursor = true;
    icon.events.onInputUp.add(callback);
    icon.events.onInputOver.add(onOver);
    icon.events.onInputOut.add(onOut);
    state.optionCount ++;
}

/* *****************************************
 * createLateralMenu
 *
 * Creates the lateral menu common to
 * several states
 *
 * Params
 *    game: the game
 *    state: current state
 * *****************************************/
function createLateralMenu (game, state) {
    addMenuOption(game.global.image.HOME, function (target) {
        game.state.start('homeMenu');
    }, state);
    addMenuOption(game.global.image.LEAGUE, function (target) {
        game.state.start('leagueMenu');
    }, state);
    addMenuOption(game.global.image.FRIENDLY, function (target) {
        game.state.start('friendlyMenu');
    }, state);
    addMenuOption(game.global.image.SAVE, function (target) {
        alertSave(target);
        save();
    }, state);
    addMenuOption(game.global.image.EXIT, function (target) {
        game.global.currentMusic.stop();
        game.global.currentMusic = game.global.music['menu'];
        game.global.currentMusic.play();

        game.state.start('menu');
    }, state);

    // Black drop for kewl looks
    var rect6 = new Phaser.Rectangle(0, state.optionCount * 85, state.menuOptionWidth * state.menuOptionScale, 400);
    game.debug.geom(rect6, '#202020');
}

/* *****************************************
 * alertSave
 *
 * Animate save button
 *
 * Params
 *    image: the image for save button
 * *****************************************/
function alertSave (image) {
    game.global.saveSound.play();
    tween = game.add.tween(image).to( { angle: -3 }, 100)
        .to({angle: 3}, 200).to( { angle: -3 }, 200)
        .to({angle: 3}, 200).to( { angle: 0 }, 100).start();
}