/*******************************************
 *             MISC FUNCTIONS              *
 *******************************************/

/* *****************************************
 * gaussian
 *
 * Returns a gaussian random function with
 * the given mean and stdev.
 *
 * Params:
 *    mean: the mean
 *    stdev: the standard deviation
 * *****************************************/
function gaussian(mean, stdev) {
    var y2;
    var use_last = false;
    return function () {
        var y1;
        if (use_last) {
            y1 = y2;
            use_last = false;
        }
        else {
            var x1, x2, w;
            do {
                x1 = 2.0 * Math.random() - 1.0;
                x2 = 2.0 * Math.random() - 1.0;
                w = x1 * x1 + x2 * x2;
            } while (w >= 1.0);
            w = Math.sqrt((-2.0 * Math.log(w)) / w);
            y1 = x1 * w;
            y2 = x2 * w;
            use_last = true;
        }

        var retval = mean + stdev * y1;
        if (retval > 0)
            return retval;
        return -retval;
    }
}

/* *****************************************
 * compare
 *
 * Team comparison.
 *
 * Params:
 *    a: team a
 *    b: team b
 * *****************************************/
function compare(a, b) {
    if (a.points < b.points) {
        return 1;
    } else if (a.points > b.points) {
        return -1;
    } else {
        if ((a.goalsScored - a.goalsAgainst) < (b.goalsScored - b.goalsAgainst)) {
            return 1;
        } else if ((a.goalsScored - a.goalsAgainst) > (b.goalsScored - b.goalsAgainst)) {
            return -1;
        } else {
            if (a.goalsScored < b.goalsScored) {
                return 1;
            } else if (a.goalsScored > b.goalsScored) {
                return -1;
            }
        }
    }
    return 0;
}


/* *****************************************
 * randomBetween
 *
 * Returns a random number in an interval.
 *
 * Params:
 *    min: minimun number of the interval
 *    max: maximun number of the interval
 * *****************************************/
function randomBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}