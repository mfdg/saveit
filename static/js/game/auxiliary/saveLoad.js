/*******************************************
 *         SAVE AND LOAD FUNCTIONS         *
 *******************************************/

/* *****************************************
 * save
 *
 * Saves the data of your league into local
 * storage.
 * *****************************************/
function save() {
    localStorage.setItem("teamColor", game.teamColor);
    localStorage.setItem("teamName", game.teamName);
    localStorage.setItem("worldLeague", game.worldLeague);

    localStorage.setItem("matchIndex", game.league.matchIndex);
    for (var i = 0; i < game.global.numTeamLeague; i++) {
        localStorage.setItem("teamColor" + i, game.league.teams[i].color);
        localStorage.setItem("teamName" + i, game.league.teams[i].name);
        localStorage.setItem("teamFormation" + i, game.league.teams[i].formation);
        localStorage.setItem("teamMean" + i, game.league.teams[i].mean);
        localStorage.setItem("teamStdev" + i, game.league.teams[i].stdev);
        localStorage.setItem("gamesWon" + i, game.league.teams[i].gamesWon);
        localStorage.setItem("gamesDrawn" + i, game.league.teams[i].gamesDrawn);
        localStorage.setItem("gamesLost" + i, game.league.teams[i].gamesLost);
        localStorage.setItem("goalsScored" + i, game.league.teams[i].goalsScored);
        localStorage.setItem("goalsAgainst" + i, game.league.teams[i].goalsAgainst);
        localStorage.setItem("points" + i, game.league.teams[i].points);
    }
}

/* *****************************************
 * load
 *
 * Loads the data currently available in your
 * local storage.
 * *****************************************/
function load() {
    if (localStorage.getItem("teamName") === null)
        return -1;
    game.worldLeague = localStorage.getItem("worldLeague");
    game.teamColor = localStorage.getItem("teamColor");
    game.teamName = localStorage.getItem("teamName");

    game.league = new League();
    game.league.matchIndex = localStorage.getItem("matchIndex");
    game.league.teams = new Array();
    var team;
    for (var i = 0; i < game.global.numTeamLeague; i++) {
        team = new Team(null, null, localStorage.getItem("teamColor" + i),
            localStorage.getItem("teamName" + i), localStorage.getItem("teamFormation" + i),
            Number(localStorage.getItem("teamMean" + i)), Number(localStorage.getItem("teamStdev" + i)));

        if (i == 0) {
            game.playerTeam = team;
        }

        team.gamesWon = Number(localStorage.getItem("gamesWon" + i));
        team.gamesDrawn = Number(localStorage.getItem("gamesDrawn" + i));
        team.gamesLost = Number(localStorage.getItem("gamesLost" + i));
        team.goalsScored = Number(localStorage.getItem("goalsScored" + i));
        team.goalsAgainst = Number(localStorage.getItem("goalsAgainst" + i));
        team.points = Number(localStorage.getItem("points" + i));
        game.league.teams.push(team);
    }

    return 0;
}