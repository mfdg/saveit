/*******************************************
 *           NAMEGEN FUNCTIONS             *
 *******************************************/

// Azeroth names
var azeroth = ["Alterac", "Bahía del Botín", "Cima del Trueno", "Corona de Hielo", "Dalaran", "Darnassus", "Dun Modr",
    "Exodar", "Forjaz", "Fuego Infernal", "Gadgetzan", "Gilneas", "Gnomeregan", "Gundrak", "Kul Tiras", "Lordaeron",
    "Lunargenta", "Menethil", "Minahonda", "Nazjatar", "Nethergarde", "Orgrimmar", "Pico Nidal", "Quel'Danas", "Rocal",
    "Shattrath", "Sen'Jin", "Sentencia", "Skettis", "Stromgarde", "Suramar", "Thaurissan", "Thrallmar", "Trinquete",
    "Uldaman", "Ventormenta", "Villa Oscura", "Vistaeterna", "Zul'Gurub", "Zul'Aman", "Zul'Farrak", "Zuldazar"];

// Star Wars names
var starWars = ["Alderaan", "Anoat", "Bespin", "Cato Nemoidia", "Cholganna", "Corellia", "Coruscant", "Dagobah",
    "Dathomir", "Eadu", "Endor", "Felucia", "Geonosis", "Hosnian Prime", "Hoth", "Ilum", "Iridonia", "Jakku", "Jedha",
    "Kamino", "Kashyyyk", "Kessel", "Malachor", "Malastare", "Mandalore", "Moraband", "Mustafar", "Naboo", "Nal Hutta",
    "Polis Massa", "Rishi", "Rodia", "Ruusan", "Ryloth", "Scarif", "Shili", "Takodana", "Tatooine", "Wobani", "Umbara",
    "Utapau", "Vendaxa", "Yavin", "Ylesia"];

// Harry Potter names
var harryPotter = ["Azkaban", "Beauxbatons", "Borgin & Burkes", "Hog's Head", "Caldero Chorreante", "Castelobruxo",
    "Diagon", "Durmstrang", "Flourish & Blotts", "Fortescue", "Grimmauld Place", "Gringotts", "Hogsmeade", "Hogwarts",
    "Honeydukes", "Ilvermorny", "Knockturn", "Koldovstoretz", "La Madriguera", "Las Tres Escobas", "Little Whinging",
    "Mahuotokoro", "Malkin", "Nurmengard", "Ollivanders", "Hangelton", "San Mungo", "Scrivenshaft", "Uagadou",
    "Wiseacre", "Zonko"];

// The Elder Scrolls names
var nirn = ["Ald Lambasi", "Ald Velothi", "Alinor", "Almalexia", "Anvil", "Arenthia", "Arnesia", "Bravil", "Bruma",
    "Caer Suvio", "Carrera Blanca", "Cheydinhal", "Delodiil", "Gideon", "Falkreath", "Falinesti", "Helstrom", "Hibernalia",
    "Imperial", "Kvatch", "Leyawiin", "Lilandril", "Lilmoth", "Lucero del Alba", "Marbruk", "Markarth", "Morthal", "Riften",
    "Roca Cuervo", "Rosanegra", "Sancre Tor", "Shimmerene", "Silvenar", "Skingrad", "Soledad", "Stros M'kai",
    "Ventalia", "Vivec"];

// Names
var names = ["Azkaban", "Beauxbatons", "Borgin & Burkes", "Hog's Head", "Caldero Chorreante", "Castelobruxo",
    "Diagon", "Durmstrang", "Flourish & Blotts", "Fortescue", "Grimmauld Place", "Gringotts", "Hogsmeade", "Hogwarts",
    "Honeydukes", "Ilvermorny", "Knockturn", "Koldovstoretz", "La Madriguera", "Las Tres Escobas", "Little Whinging",
    "Mahuotokoro", "Malkin", "Nurmengard", "Ollivanders", "Hangelton", "San Mungo", "Scrivenshaft", "Uagadou",
    "Wiseacre", "Zonko"];

// Pre- and suffixes
var prefix = ["Atlético de", "Athletic de", "Deportivo de", "Real", "Sporting de", "Racing de", "Club Deportivo", "CD",
    "Club de Fútbol", "CDF", "Inter"];
var suffix = ["United", "City", "CD", "FC"];

/* *****************************************
 * randomEl
 *
 * Chooses a random element from a list.
 * *****************************************/
function randomEl(list) {
    var i = Math.floor(Math.random() * list.length);
    return list[i];
}

/* *****************************************
 * createNames
 *
 * Creates an array of team names using
 * global variables and making sure some
 * restrictions are met.
 * *****************************************/
function createNames() {
    var place;
    switch (game.worldLeague) {
        case 'azeroth':
            place = azeroth;
            break;
        case 'starWars':
            place = starWars;
            break;
        case 'harryPotter':
            place = harryPotter;
            break;
        case 'nirn':
            place = nirn;
            break;
    }

    var namelist = new Array(), dupflag = 0, i = 0;
    // Create initial name list
    for (i = 0; i < game.global.numTeamLeague; i++) {
        dupflag = 0;
        // Random place name
        var auxName = randomEl(place);
        // We allow only two teams from the same place
        var ind = namelist.indexOf(auxName);
        if (ind != -1) {
            ind = namelist.indexOf(auxName, ind + 1);
            if (ind != -1) {
                i--;
                dupflag = 1;
            }
        }
        if (!dupflag) {
            namelist.push(auxName);
        }
    }

    var finishedList = new Array();
    // Once we have place list, add decoration
    for (i = 0; i < namelist.length; i++) {
        var aux;
        if (Math.round(Math.random())) {
            aux = randomEl(prefix) + ' ' + namelist[i];
        } else {
            aux = namelist[i] + ' ' + randomEl(suffix);
        }
        // There can't be two teams with the same name
        if (finishedList.indexOf(aux) != -1 || aux.length > game.global.maxNameLength) {
            i--;
        } else {
            finishedList.push(aux);
        }
    }

    return finishedList;
}

/* *****************************************
 * createName
 *
 * Creates a team name
 * *****************************************/
function createName() {
    var name = randomEl(names);
    var aux;

    if (Math.round(Math.random())) {
        aux = randomEl(prefix) + ' ' + name;
    } else {
        aux = name + ' ' + randomEl(suffix);
    }

    return aux;
}