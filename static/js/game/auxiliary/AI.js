/*******************************************
 *              AI FUNCTIONS               *
 *******************************************/

// Constants
var admittedDistance = 100;

/* *****************************************
 * hasGoalOption
 *
 * Checks if the player has the option to
 * score a goal.
 *
 * Params
 *    player: the player who might have goal
 *    option
 * *****************************************/
function hasGoalOption(state, player) {
    var goalCenter = player.team.oppGoal.getCenter();
    var temp = null;

    if (Phaser.Math.distance(player.body.x, player.body.y, goalCenter.x, goalCenter.y) < admittedDistance) {

        var topPost = player.team.oppGoal.getTopPost();
        var botPost = player.team.oppGoal.getBotPost();
        var oppGoalkeeper = null;
        if (state.visitorTeam == player.team) {
            oppGoalkeeper = state.homeTeam.goalkeeper;
        } else {
            oppGoalkeeper = state.visitorTeam.goalkeeper;
        }

        if (player.body.x > player.team.oppGoal.x) {
            if ((player.body.y > topPost.y) && (player.body.y < botPost.y)
                && !((player.y < oppGoalkeeper.y + 3) && (player.y > oppGoalkeeper.y - 3))) {
                return new Action(game.global.direction.LEFT, game.global.action.KICK, 9999);
            } else {
                temp = player.body.x + player.body.y - topPost.x;
                if ((temp > topPost.y) && (temp < botPost.y)) {
                    return new Action(game.global.direction.DOWNLEFT, game.global.action.KICK, 9999);
                }
                temp = topPost.x - player.body.x + player.body.y;
                if ((temp > topPost.y) && (temp < botPost.y)) {
                    return new Action(game.global.direction.UPLEFT, game.global.action.KICK, 9999);
                }
            }
        } else {
            if ((player.body.y > topPost.y) && (player.body.y < botPost.y)
                && !((player.y < oppGoalkeeper.y + 3) && (player.y > oppGoalkeeper.y - 3))) {
                return new Action(game.global.direction.RIGHT, game.global.action.KICK, 9999);
            } else {
                temp = player.body.x + player.body.y - topPost.x;
                if ((temp > topPost.y) && (temp < botPost.y)) {
                    return new Action(game.global.direction.UPRIGHT, game.global.action.KICK, 9999);
                }
                temp = topPost.x - player.body.x + player.body.y;
                if ((temp > topPost.y) && (temp < botPost.y)) {
                    return new Action(game.global.direction.DOWNRIGHT, game.global.action.KICK, 9999);
                }
            }
        }
    }
}

/* *****************************************
 * passSupportingPlayer
 *
 * Checks if the player has a supporting
 * player in position.
 *
 * Params
 *    player: the player who looks for
 *    support
 * *****************************************/
function passSupportingPlayer(state, player) {
    var actions = new Array();
    var distance;
    for (var i = 0; i < game.global.numPlayers; i++) {
        if (player.team.players[i] != player) {
            distance = Phaser.Math.distance(player.x, player.y, player.team.players[i].x, player.team.players[i].y);
            if (distance < game.global.supportRadius) {
                var dir = closestDirection(player, player.team.players[i]);
                var score = 600 + (6000 / Phaser.Math.distance(player.team.players[i].x, player.team.players[i].y,
                        player.team.oppGoal.x, player.team.oppGoal.y));
                if (isOppPlayerNear(state, player.team.players[i])) {
                    score -= 50;
                }
                if (isOppPlayerObstacle(state, player, dir)) {
                    score /= 4;
                }
                if (player.team.players[i] == player.team.goalkeeper) {
                    score /= 3;
                }
                actions.push(new Action(dir, game.global.action.PASS,
                    score));

            }
        }
    }
    return actions;
}

/* *****************************************
 * hasSupportOption
 *
 * Checks if the player can reach a support-
 * ing position.
 *
 * Params
 *    player: the player who might reach
 *    supporting position
 * *****************************************/
function hasSupportOption(player) {
    var actions = new Array();
    var distance;
    for (var i = 0; i < game.global.numPlayers; i++) {
        // Search for the teammate with the ball
        if (player.team.players[i].controlledBall != null) {
            for (var j = 0; j < game.global.directionPoints.length; j++) {
                distance = Phaser.Math.distance(player.x + game.global.reachSupportDistance * game.global.directionPoints[j].x,
                    player.y + game.global.reachSupportDistance * game.global.directionPoints[j].y,
                    player.team.players[i].x, player.team.players[i].y);
                if ((distance < game.global.supportRadius) && (distance > game.global.minSupportRadius)) {
                    actions.push(new Action(j, game.global.action.RUN,
                        600 - (0.75 * distance) + (1000 / Math.abs(player.team.players[i].x - player.x))
                            + (400 * Math.abs(player.team.players[i].y - player.y))
                        + (1000 / Phaser.Math.distance(player.x, player.y,
                            player.team.oppGoal.x, player.team.oppGoal.y))));
                }
            }
            break;
        }
    }
    return actions;
}

/* *****************************************
 * isClosestToBall
 *
 * Checks if the player is the closest to the
 * ball
 *
 * Params
 *    player: the player
 * *****************************************/
function isClosestToBall(state, player) {
    var distance = Phaser.Math.distance(player.body.x, player.body.y, state.ball.body.x, state.ball.body.y);
    for (var i = 0; i < game.global.numPlayers; i++) {
        var auxPlayer = player.team.players[i];
        if (distance > Phaser.Math.distance(auxPlayer.body.x, auxPlayer.body.y, state.ball.body.x, state.ball.body.y)
            && auxPlayer != player) {
            return false;
        }
    }
    return true;
}

/* *****************************************
 * isFarthestToBall
 *
 * Checks if the player is the farthest to the
 * ball
 *
 * Params
 *    player: the player
 * *****************************************/
function isFarthestToBall(state, player) {
    var distance = Phaser.Math.distance(player.body.x, player.body.y, state.ball.body.x, state.ball.body.y);
    for (var i = 0; i < game.global.numPlayers; i++) {
        var auxPlayer = player.team.players[i];
        if (distance < Phaser.Math.distance(auxPlayer.body.x, auxPlayer.body.y, state.ball.body.x, state.ball.body.y)
            && auxPlayer != player) {
            return false;
        }
    }
    return true;
}

/* *****************************************
 * evaluateMove
 *
 * Evaluates every movement.
 *
 * Params
 *    player: the player that is evaluating
 * *****************************************/
function evaluateMove(state, player) {
    var point = null;
    var weHaventBall = false;
    if (state.ball.owner != null)
        if (state.ball.owner.team != player.team)
            weHaventBall = true;

    if ((state.ball.owner == null && (Phaser.Math.distance(player.x, player.y, state.ball.x, state.ball.y) < 80) &&
        playersCloseToBall(state, player.team) < 3) ||
        (weHaventBall && Phaser.Math.distance(player.team.goal.x, player.team.goal.y, state.ball.x, state.ball.y) < 300)) {
        point = new Phaser.Point(state.ball.x, state.ball.y);
    } else {
        if (isFarthestToBall(state, player)) {
            point = new Phaser.Point(player.iniPos.x, player.iniPos.y);
            // point = new Phaser.Point((player.iniPos.x * 3 + state.ball.x) / 4,
            //     (player.iniPos.y * 3 + state.ball.y) / 4);
        } else {
            point = new Phaser.Point((player.iniPos.x*3 + state.ball.x*2) / 5,
                (player.iniPos.y*3 + state.ball.y*2) / 5);
        }
    }
    return evaluateDirectionMove(state, player, point);
}

/* *****************************************
 * evaluateDirectionMove
 *
 * Evaluates every movement in respect to
 * an objective.
 *
 * Params
 *    player: the player that is evaluating
 *    objective: coordinates of the objective
 * *****************************************/
function evaluateDirectionMove(state, player, objective) {
    var distance = Phaser.Math.distance(player.body.x, player.body.y, objective.x, objective.y);
    var actions = new Array();
    var score = 0;
    var leftTop = state.goalL.getTopPost();
    var leftBot = state.goalL.getBotPost();
    var rightTop = state.goalR.getTopPost();
    var rightBot = state.goalR.getBotPost();

    // game.global.direction.UP
    var auxDist = Phaser.Math.distance(player.body.x, player.body.y - 4, objective.x, objective.y);
    if (auxDist < distance) {
        if ((player.body.x > leftBot.x && player.body.x < rightBot.x)
            || (!(player.body.x < leftBot.x && player.body.y > leftBot.y)
            && !(player.body.x > rightBot.x && player.body.y > rightBot.y))) {
            score = 1000 + 1000 / auxDist;
            if (isOppPlayerNear(state, player)) {
                score -= 100;
            }
            if (isOppPlayerObstacle(state, player, game.global.direction.UP)) {
                score /= 3;
            }
        } else {
            score = 200 / auxDist;
        }
    } else {
        score = 300 / auxDist;
    }
    actions.push(new Action(game.global.direction.UP, game.global.action.RUN, score));

    // game.global.direction.UPRIGHT
    auxDist = Phaser.Math.distance(player.body.x + 3, player.body.y - 3, objective.x, objective.y);
    if (auxDist < distance) {
        if ((player.body.x > leftBot.x && player.body.x < rightBot.x)
            || (!(player.body.x < leftBot.x && player.body.y > leftBot.y)
            && !(player.body.x > rightBot.x && player.body.y > rightBot.y))) {
            score = 1000 + 1000 / auxDist;
            if (isOppPlayerNear(state, player)) {
                score -= 100;
            }
            if (isOppPlayerObstacle(state, player, game.global.direction.UPRIGHT)) {
                score /= 3;
            }
        } else {
            score = 100 / auxDist;
        }
    } else {
        score = 200 / auxDist;
    }
    actions.push(new Action(game.global.direction.UPRIGHT, game.global.action.RUN, score));

    // game.global.direction.RIGHT
    auxDist = Phaser.Math.distance(player.body.x + 4, player.body.y, objective.x, objective.y);
    if ((player.body.x + (player.width / 2) > rightBot.x) && (player.controlledBall != null)) {
        score = 50 / auxDist;
    } else if (player.body.x - (player.width / 2) < leftBot.x) {
        score = 1000 + 1200 / auxDist;
    } else if (auxDist < distance) {
        score = 1000 + 1000 / auxDist;
        if (isOppPlayerNear(state, player)) {
            score -= 100;
        }
        if (isOppPlayerObstacle(state, player, game.global.direction.RIGHT)) {
            score /= 3;
        }
    } else {
        score = 200 / auxDist;
    }
    actions.push(new Action(game.global.direction.RIGHT, game.global.action.RUN, score));

    // game.global.direction.DOWNRIGHT
    auxDist = Phaser.Math.distance(player.body.x + 3, player.body.y + 3, objective.x, objective.y);
    if (auxDist < distance) {
        if ((player.body.x > leftTop.x && player.body.x < rightTop.x)
            || (!(player.body.x < leftTop.x && player.body.y < leftTop.y)
            && !(player.body.x > rightTop.x && player.body.y < rightTop.y))) {
            score = 1000 + 1000 / auxDist;
            if (isOppPlayerNear(state, player)) {
                score -= 100;
            }
            if (isOppPlayerObstacle(state, player, game.global.direction.DOWNRIGHT)) {
                score /= 3;
            }
        } else {
            score = 100 / auxDist;
        }
    } else {
        score = 200 / auxDist;
    }
    actions.push(new Action(game.global.direction.DOWNRIGHT, game.global.action.RUN, score));

    // game.global.direction.DOWN
    auxDist = Phaser.Math.distance(player.body.x, player.body.y + 4, objective.x, objective.y);
    if (auxDist < distance) {
        if ((player.body.x > leftTop.x && player.body.x < rightTop.x)
            || (!(player.body.x < leftTop.x && player.body.y < leftTop.y)
            && !(player.body.x > rightTop.x && player.body.y < rightTop.y))) {
            score = 1000 + 1000 / auxDist;
            if (isOppPlayerNear(state, player)) {
                score -= 100;
            }
            if (isOppPlayerObstacle(state, player, game.global.direction.DOWN)) {
                score /= 3;
            }
        } else {
            score = 100 / auxDist;
        }
    } else {
        score = 200 / auxDist;
    }
    actions.push(new Action(game.global.direction.DOWN, game.global.action.RUN, score));

    // game.global.direction.DOWNLEFT
    auxDist = Phaser.Math.distance(player.body.x - 3, player.body.y + 3, objective.x, objective.y);
    if (auxDist < distance) {
        if ((player.body.x > leftTop.x && player.body.x < rightTop.x)
            || (!(player.body.x < leftTop.x && player.body.y < leftTop.y)
            && !(player.body.x > rightTop.x && player.body.y < rightTop.y))) {
            score = 1000 + 1000 / auxDist;
            if (isOppPlayerNear(state, player)) {
                score -= 100;
            }
            if (isOppPlayerObstacle(state, player, game.global.direction.DOWNLEFT)) {
                score /= 3;
            }
        } else {
            score = 100 / auxDist;
        }
    } else {
        score = 200 / auxDist;
    }
    actions.push(new Action(game.global.direction.DOWNLEFT, game.global.action.RUN, score));

    // game.global.direction.LEFT
    auxDist = Phaser.Math.distance(player.body.x - 4, player.body.y, objective.x, objective.y);
    if ((player.body.x - (player.width / 2) < leftBot.x) && (player.controlledBall != null)) {
        score = 50 / auxDist;
    } else if (player.body.x + (player.width / 2) > rightBot.x) {
        score = 1000 + 1200 / auxDist;
    } else if (auxDist < distance) {
        score = 1000 + 1000 / auxDist;
        if (isOppPlayerNear(state, player)) {
            score -= 100;
        }
        if (isOppPlayerObstacle(state, player, game.global.direction.LEFT)) {
            score /= 3;
        }
    } else {
        score = 200 / auxDist;
    }
    actions.push(new Action(game.global.direction.LEFT, game.global.action.RUN, score));

    // game.global.direction.UPLEFT
    auxDist = Phaser.Math.distance(player.body.x - 3, player.body.y - 3, objective.x, objective.y);
    if (auxDist < distance) {
        if ((player.body.x > leftBot.x && player.body.x < rightBot.x)
            || (!(player.body.x < leftBot.x && player.body.y > leftBot.y)
            && !(player.body.x > rightBot.x && player.body.y > rightBot.y))) {
            score = 1000 + 1000 / auxDist;
            if (isOppPlayerNear(state, player)) {
                score -= 100;
            }
            if (isOppPlayerObstacle(state, player, game.global.direction.UPLEFT)) {
                score /= 3;
            }
        } else {
            score = 100 / auxDist;
        }
    } else {
        score = 200 / auxDist;
    }
    actions.push(new Action(game.global.direction.UPLEFT, game.global.action.RUN, score));

    // Check whether to swipe if the objective is the ball
    if (objective == state.ball.body && state.ball.owner != null
        && state.ball.owner.team != player.team
        && Phaser.Math.distance(player.body.x, player.body.y, objective.x, objective.y) <= game.global.swipeDistance) {
        actions.push(new Action(null, game.global.action.SWIPE, 1000));
    }

    return actions;
}

/* *****************************************
 * closestDirection
 *
 * Calculates which of the 8 directions is
 * closer to the vector between origin and
 * destination.
 *
 * Params
 *    origin: start point
 *    destination: end point
 * *****************************************/
function closestDirection(origin, destination) {
    var vector = new Phaser.Point(destination.x - origin.x, destination.y - origin.y);
    var directions = game.global.directionPoints;

    var diff = 999, auxDiff;
    var ret = null;

    for (var i = 0; i < game.global.directionPoints.length; i++) {
        directions[i].normalize();
        auxDiff = Phaser.Math.distance(vector.x, vector.y, directions[i].x, directions[i].y);
        if (auxDiff < diff) {
            diff = auxDiff;
            ret = i;
        }
    }
    return ret;
}

/* *****************************************
 * isOppPlayerNear
 *
 * Checks if there is any opposite player
 * near.
 *
 * Params
 *    player: the player that is evaluating
 * *****************************************/
function isOppPlayerNear(state, player) {
    var team;
    if (state.homeTeam == player.team)
        team = state.visitorTeam;
    else
        team = state.homeTeam;

    for (var i = 0; i < game.global.numPlayers; i++) {
        var auxPlayer = team.players[i];
        if (Phaser.Math.distance(player.body.x, player.body.y, auxPlayer.body.x, auxPlayer.body.y) < game.global.threatDistance) {
            return true;
        }
    }
    return false;
}

/* *****************************************
 * isOppPlayerObstacle
 *
 * Checks if there is any opposite player in
 * this direction.
 *
 * Params
 *    state: the current state
 *    player: the player that is evaluating
 *    dir: dir
 * *****************************************/
function isOppPlayerObstacle(state, player, dir) {
    var team;
    var direction = game.global.directionPoints[dir];
    if (state.homeTeam == player.team)
        team = state.visitorTeam;
    else
        team = state.homeTeam;

    var obstacle = false;
    for (var i = 0; i < game.global.numPlayers; i++) {
        var auxPlayer = team.players[i];
        obstacle = false;
        if (Phaser.Math.distance(player.body.x, player.body.y, auxPlayer.body.x, auxPlayer.body.y) < game.global.threatDistance) {
            // Checks if is in our direction
            if (direction.x == 1) {
                if (auxPlayer.x > player.x + 5) {
                    obstacle = true;
                }
            } else if (direction.x == -1) {
                if (auxPlayer.x < player.x - 5) {
                    obstacle = true;
                }
            } else {
                obstacle = true;
            }

            if (obstacle) {
                if (direction.y == 1) {
                    if (auxPlayer.y > player.y + 5) {
                        return true;
                    }
                } else if (direction.y == -1) {
                    if (auxPlayer.y < player.y - 5) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
    }
    return false;
}

/*******************************************
 *          AUXILIARY FUNCTIONS            *
 *******************************************/

/* *****************************************
 * playersCloseToBall
 *
 * Returns the number of players of a team
 * close to the ball.
 *
 * Params
 *    state: the current state
 *    team: the team of the players to count
 * *****************************************/
function playersCloseToBall(state, team) {
    var counter = 0;

    for (var i = 0; i < team.players.length; i++) {
        var auxPlayer = team.players[i];
        if (Phaser.Math.distance(auxPlayer.x, auxPlayer.y, state.ball.x, state.ball.y) < 80) {
            counter++;
        }
    }
    return counter;
}

/* *****************************************
 * chooseAction
 *
 * Returns the best action from the list.
 *
 * Params
 *    actions: array of actions
 * *****************************************/
function chooseAction(actions) {
    // Choose best Action from list
    var bestAction = actions[0];
    for (var i = 1; i < actions.length; i++) {
        if (actions[i].score > bestAction.score) {
            bestAction = actions[i];
        }
    }
    return bestAction;
}