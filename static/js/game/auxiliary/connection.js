/*******************************************
 *       AJAX CONNECTION FUNCTIONS         *
 *******************************************/

// Constants
// empty

/* *****************************************
 * loginCoachdata
 *
 * Sends the user to log in to coachdata app
 * and receives data from the server
 *
 * Params
 *    user: the user string
 * *****************************************/
function loginCoachdata(user) {
    $.ajax({
        url: '/coachdata/gamelogin',
        data: {
            'user': user
        },
        dataType: 'json',
        success: function (data) {
            if (data['response'] == "OK") {
                game.global.user = user;

                // TODO check value to assign
                if (data['evaluated']) {
                    game.global.evaluated = true;

                    for (var i = 0; i < Object.keys(data['evaluations']).length; i++) {
                        game.global.evaluation += data['evaluations'][i];
                    }

                    game.global.evaluation /= (Object.keys(data['evaluations']).length * data['max_mark']);

                } else {
                    game.global.evaluated = false;
                }

                menuState.loginResponse(true);
            } else {
                menuState.loginResponse(false);
            }
        },
        error: function (data) {
            menuState.loginResponse(false);
        }
    });
}

/* *****************************************
 * sendMatchResults
 *
 * Sends data related to a finished match to
 * database
 *
 * Params
 *    userTeam: the team of the user to be
 *    updated
 *    iaTeam: the team against it played
 * *****************************************/
function sendMatchResults(userScore, iaScore) {
    var win = 0; // 0 Draw, 1 Home, -1 Visitor
    if (userScore > iaScore) {
        win = 1;
    } else if (userScore < iaScore) {
        win = -1;
    }
    $.ajax({
        url: '/coachdata/gamesaveresult',
        data: {
            'user': game.user,
            'result': win,
            'goalsFor': userScore,
            'goalsAgainst': iaScore
        },
        dataType: 'json',
        success: function (data) {
            if (data['response'] == "OK") {
                console.log("SUCCESS");
            } else {
                console.log("ERROR");
            }
        },
        error: function (data) {
            console.log("FAILURE");
        }
    });
}

/* *****************************************
 * getMatchResults
 *
 * Get results and match info of a user
 *
 * Params
 *    user: the user string
 * *****************************************/
function getMatchResults(user) {
    $.ajax({
        url: '/coachdata/gamegetranking',
        data: {
            'user': user
        },
        dataType: 'json',
        success: function (data) {
            if (data['response'] == "OK") {
                rankingState.drawTables(data);
            } else {
                rankingState.drawTables(null);
            }
        },
        error: function (data) {
            console.log("FAILURE");
        }
    });
}